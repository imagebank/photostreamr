//
//  ChooseServerController.m
//  PhotoStreamr
//
//  Created by yoshi on 5/31/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "AppDelegate.h"
#import "ChooseServerController.h"
#import "Server.h"
#import "Utilities.h"
#import "Constants.h"
#import "PrivatePasswordDialog.h"
#import "PrivateFolderPasswordChecker.h"
#import "JSON.h"
#import "HTMLHelpController.h"
#import "HelpPopOverController.h"
#import "IASKAppSettingsViewController.h"
#import "OfflineFoldersController.h"
#import "HTMLHelpController.h"
#import <netinet/in.h>
#import "FolderContentsController.h"
#import "ServerBrowserViewController.h"

#include <arpa/inet.h>

#ifdef FREE_UPGRADABLE_VERSION
#import "MKStoreManager.h"
#import "BuyUpgradeController.h"
#endif

#define kCouldNotConnectToServerAlertTag 11
#define kNaggingPopUpAlertTag 22
#define kUpgradePopUpAlertTag 33
#define kSecondsBetweenShowingNagPopUp 21600


@implementation ChooseServerController

@synthesize servers, managedObjContext, serversTableView, selectedServer, connectingView, connectingIndicator, currentRequest, mainView, webView, availableBonjourServers, choosableBonjourServers, serviceBrowser;

- (id)initWithNavigatorURL:(NSURL*)URL query:(NSDictionary*)query {	
	if ( self = [self initWithNibName:
				 (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) ? @"ChooseServerController_NO_iAD-iPad" : @"ChooseServerController_NO_iAD"
							   bundle:nil] ) {
		self.managedObjContext = [query objectForKey:@"managedObjectContext"];
//		self.title = @"ImageBank";
        self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"titlebar"]];
		if ( self.managedObjContext == nil ) {
			[self setManagedObjContext:[(AppDelegate *)[[UIApplication sharedApplication] delegate] managedObjectContext]];
		}
        self.choosableBonjourServers = [[NSMutableArray alloc] init];
        self.availableBonjourServers = [[NSMutableArray alloc] init];
        self.serviceBrowser = [[NSNetServiceBrowser alloc] init];
	}
	return self;
}

/*
- (id)initWithStyle:(UITableViewStyle)style {
    // Override initWithStyle: if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
    if (self = [super initWithStyle:style]) {
    }
    return self;
}
*/

#pragma mark
#pragma mark UIViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [self.serviceBrowser setDelegate:self];
	[self retain];
    
	UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh
																			   target:self action:@selector(refresh:)];
	self.navigationItem.leftBarButtonItem = self.editButtonItem;
    
	self.navigationItem.rightBarButtonItem = addButton;
	self.navigationItem.rightBarButtonItem.enabled = YES;
    
    self.connectingIndicator.color = [Utilities globalAppTintColor];
	
	#ifdef FREE_UPGRADABLE_VERSION
	[self showAlertOnFirstLoad];
	#endif
	
    [self.serversTableView setTag:100];
    
    [self.serversTableView setBackgroundColor:[UIColor colorWithRed:211/255.0f green:211/255.0f blue:211/255.0f alpha:1.0f]];
//    UIImage *image = [UIImage imageNamed:(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) ? @"background-gradient-ipad.png" : @"background-gradient.png"];
//    UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
//    imageView.contentMode = UIViewContentModeScaleToFill;
//    [self.serversTableView setBackgroundView:imageView];
//    [imageView release];
    [self.serversTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
	//self.clearsSelectionOnViewWillAppear = NO;
    
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ) {
        [self.connectingView.layer setCornerRadius:30.0f];
    }
    
    self.navigationItem.backBarButtonItem.title = @"";
    self.title = @"";
    
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self refresh:nil];
    
    self.serversTableView.alpha = 1.0f;
	// load the data
	NSFetchRequest *request = [[NSFetchRequest alloc] init];
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"Server" inManagedObjectContext:self.managedObjContext];
	[request setEntity:entity];
	
	NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES selector:@selector(caseInsensitiveCompare:)];
	NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
	[request setSortDescriptors:sortDescriptors];
	[sortDescriptors release];
	[sortDescriptor release];
	
	NSError *error;
	NSMutableArray *mutableFetchResults = [[self.managedObjContext executeFetchRequest:request error:&error] mutableCopy];
	if (mutableFetchResults == nil) {
		// Handle the error.
	}
	[self setServers:mutableFetchResults];
	[mutableFetchResults release];
	[request release];
	
    self.webView.hidden = YES;
	[self.serversTableView reloadData];

	self.navigationController.navigationBar.hidden = NO;
    [self.navigationController setToolbarHidden:NO];
    
    self.editButtonItem.enabled = YES;
    if ( [servers count] < 1 ) {
        self.editButtonItem.enabled = NO;
    }
    self.revealSideViewController.panInteractionsWhenClosed = PPRevealSideInteractionNavigationBar | PPRevealSideInteractionContentView;

    [self.revealSideViewController setDelegate:self];
    self.navigationItem.leftBarButtonItem.enabled = YES;
    
    UIBarButtonItem *spacer = [[[UIBarButtonItem alloc]
                                initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                target:nil action:nil] autorelease];
    UIBarButtonItem *helpButton = [[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Help",@"")
                                                                    style:UIBarButtonItemStyleBordered target:self action:@selector(showHelp:)] autorelease];
    UIBarButtonItem *sideMenuButton =
    [[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ButtonMenu.png"]
                                      style:UIBarButtonItemStyleBordered
                                     target:self
                                     action:@selector(revealSideMenu:)] autorelease];
    
    NSMutableArray *items = [NSMutableArray arrayWithObjects:sideMenuButton, spacer,nil];
    
#ifdef FREE_UPGRADABLE_VERSION
    if ( ![MKStoreManager fullVersionUpgradePurchased] ) {
        
        UIBarButtonItem *upgradeButton = [[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Upgrade",@"Upgrade to full version button")
                                                                           style:UIBarButtonItemStyleBordered
                                                                          target:self
                                                                          action:@selector(showUpgradeView:)] autorelease];
        
        [items addObject:upgradeButton];
        [items addObject:spacer];
    }
#endif
    
    [items addObject:helpButton];
    
    [self setToolbarItems:items];
    [self.navigationController setToolbarHidden:NO];

}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    HelpPopOverController *contentViewController = [[HelpPopOverController alloc] initWithStyle:UITableViewStylePlain];
    [contentViewController setDelegate:self];
    [self.revealSideViewController preloadViewController:contentViewController
                                                 forSide:PPRevealSideDirectionLeft
                                              withOffset:[UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad ? 470.0 : 70.0];
    [contentViewController release];
}

- (void)viewWillDisappear:(BOOL)animated {
	[super viewWillDisappear:animated];
}
/*
- (void)viewDidDisappear:(BOOL)animated {
	[super viewDidDisappear:animated];
}
*/

-(NSUInteger)supportedInterfaceOrientations {
	if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
		return UIInterfaceOrientationMaskAll;
	} else {
		return UIInterfaceOrientationMaskPortrait;
	}
}

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
	if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        return YES;
    }
    return UIInterfaceOrientationIsPortrait(toInterfaceOrientation);
}

-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
	[super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
}


- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
	[super viewDidUnload];
}

#pragma mark
#pragma mark Data Model Loading
- (void) loadBonjourServers {
	[self.choosableBonjourServers removeAllObjects];
    for (id data in self.availableBonjourServers) {
		NSNetService *server = (NSNetService *)data;
		if ( [[server addresses] count] > 0 ) {

			// choose the first ip address
			NSData *address = [[server addresses] objectAtIndex:0];
			struct sockaddr_in *socketAddress = nil;
			NSString *ipString = nil;
			int port;
			socketAddress = (struct sockaddr_in *) [address bytes];
			ipString = [NSString stringWithFormat: @"%s",inet_ntoa(socketAddress->sin_addr)];
			port = ntohs(socketAddress->sin_port);
			
			// get server key from txt record
			NSDictionary *txtRecord = [NSNetService dictionaryFromTXTRecordData:[server TXTRecordData]];
			NSData *hashedPasswordData = [txtRecord objectForKey:@"k"];
			NSString *hashedPassword = [[[NSString alloc] initWithData:hashedPasswordData encoding:NSUTF8StringEncoding] autorelease];
            
			[self.choosableBonjourServers addObject:[NSDictionary 
                                                     dictionaryWithObjectsAndKeys:[server name],@"name"
                                                        ,[NSString stringWithFormat:@"%@:%i",ipString,port],@"url"
                                                     ,hashedPassword,@"password",nil]];
            
            // don't save it
            [self.managedObjContext rollback];
		}
	}
    [self.serversTableView reloadData];
}

-(void)refresh:(id)sender {
	if ( self.serviceBrowser ) {
		[serviceBrowser stop];
	}
	[self.serviceBrowser searchForServicesOfType:@"_imagebank._tcp." inDomain:@""];
}

#pragma mark -
#pragma mark NSNetServiceBrowser Delegate Method Implementations

// New service was found
- (void)netServiceBrowser:(NSNetServiceBrowser *)netServiceBrowser didFindService:(NSNetService *)netService moreComing:(BOOL)moreServicesComing {
	// Make sure that we don't have such service already (why would this happen? not sure)
	if ( [self.availableBonjourServers containsObject:netService] ) {
		return;
	}
	
	// If more entries are coming, no need to update UI just yet
    //	if ( moreServicesComing ) {
    //		return;
    //	}
	
	[netService setDelegate:self];
	[netService retain];
	[netService resolveWithTimeout:20];
}

- (void)netService:(NSNetService *)netService
     didNotResolve:(NSDictionary *)errorDict {
	TTDINFO(@"An error occurred with service %@.%@.%@, error code = %@",		  
            [netService name], [netService type], [netService domain], [errorDict objectForKey:NSNetServicesErrorCode]);
}
- (void)netServiceDidResolveAddress:(NSNetService *)netService {
	[self.availableBonjourServers addObject:netService];
	[self loadBonjourServers];
}


#pragma mark
#pragma mark Table view methods


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}


// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if ( section == 0 ) {
        return 1;
    } else if ( section == 1 ) {
        return [self.choosableBonjourServers count];
    } else {
        return [servers count] + 1;
    }
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
    }
    
    UIImage *image = [UIImage imageNamed:@"cell-gradient.png"];
    UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
    imageView.contentMode = UIViewContentModeScaleToFill;
//    cell.backgroundView = imageView;
    cell.backgroundColor = [UIColor colorWithPatternImage:image];
    cell.textLabel.backgroundColor = [UIColor clearColor];

    // Set up the cell...
    if ( indexPath.section == 0 ) {
        cell.textLabel.text = NSLocalizedString(@"Saved Folders", @"");
        cell.imageView.image = [UIImage imageNamed:@"tableitem-iphoto.png"];
        cell.detailTextLabel.text = NSLocalizedString(@"View your saved photos offline",@"");
        cell.detailTextLabel.backgroundColor = [UIColor clearColor];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.editing = NO;
    } else if ( indexPath.section == 1  ) {
        NSDictionary *serverInfo = (NSDictionary*) [self.choosableBonjourServers objectAtIndex:indexPath.row];
        cell.imageView.image = [UIImage imageNamed:@"30x-Widescreen.png"];
        cell.textLabel.text = [serverInfo objectForKey:@"name"];
        cell.detailTextLabel.text = [serverInfo objectForKey:@"url"];
        cell.detailTextLabel.backgroundColor = [UIColor clearColor];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    } else if ( indexPath.section == 2 ) {
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        if ( indexPath.row >= [servers count] ) {
            cell.textLabel.text = NSLocalizedString(@"Add a Server",@"");
            cell.detailTextLabel.text = NSLocalizedString(@"For viewing photos remotely",@"");
            cell.detailTextLabel.backgroundColor = [UIColor clearColor];
            cell.imageView.image = nil;
        } else {
            Server *server = (Server *)[servers objectAtIndex:indexPath.row];
            cell.imageView.image = [UIImage imageNamed:@"30x-Widescreen.png"];
            cell.textLabel.text = [server name];
            cell.detailTextLabel.text = ([server pin] && [[server pin] length] > 0) ? 
            [NSString stringWithFormat:@"%@: %@", NSLocalizedString(@"Remote Server ID", @"Remote Server ID"), [server pin]] : [server url];
            cell.detailTextLabel.backgroundColor = [UIColor clearColor];
            [imageView release];
        }
    }
    return cell;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    if ( indexPath.section == 0 || indexPath.section == 1 ) {
        return UITableViewCellEditingStyleNone;
    } else if ( indexPath.section == 2 && indexPath.row >= [servers count] ) {
        return UITableViewCellEditingStyleNone;
    }
    return UITableViewCellEditingStyleDelete;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ( indexPath.section == 0 ) {

#ifdef FREE_UPGRADABLE_VERSION
        if ( ![MKStoreManager fullVersionUpgradePurchased] ) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Requires Full Version"
                                                                                      ,@"Requires Full Version")
                                                            message:NSLocalizedString(@"Upgrade for Saved Folders",@"Upgrade for Saved Folders")
                                                           delegate:self cancelButtonTitle:NSLocalizedString(@"Not Now", @"Not Now") otherButtonTitles:NSLocalizedString(@"Upgrade",@"Upgrade to full version button"),nil];
            alert.tag = kUpgradePopUpAlertTag;
            [alert show];
            [alert release];
            return;
        }
#endif
        
        
        OfflineFoldersController *offlineController = [[[OfflineFoldersController alloc] 
                                                        initWithNibName:(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) ? @"OfflineFoldersController-iPad" :@"OfflineFoldersController" bundle:nil] autorelease];
        [offlineController setManagedObjContext:self.managedObjContext];
        [self.navigationController pushViewController:offlineController animated:YES];
    } else if ( indexPath.section == 1 ) {
        NSDictionary *serverInfo = [self.choosableBonjourServers objectAtIndex:indexPath.row];
        NSString *password = @"";
        if ( [[NSUserDefaults standardUserDefaults] boolForKey:@"pref_save_server_passwords"] ) {
            NSString *savedPassword = [Utilities savedPasswordForServerUrl:[serverInfo objectForKey:@"url"]];         
            if ( [savedPassword length] > 0 ) {
                password = savedPassword;
            }
        }        
        [self startCheckingPasswordForServer:[serverInfo objectForKey:@"url"] password:password];
        [self.serversTableView deselectRowAtIndexPath:indexPath animated:YES];
    } else if ( indexPath.section == 2 ) {
        
        if ( indexPath.row >= [servers count] ) {
            
            ServerBrowserViewController *browseController = [[ServerBrowserViewController alloc] initWithNavigatorURL:nil query:[NSDictionary dictionaryWithObjectsAndKeys:self.managedObjContext,@"managedObjectContext",nil]];
            [self.navigationController pushViewController:browseController animated:YES];
            [browseController release];

        } else {            
            Server *server = (Server *)[servers objectAtIndex:indexPath.row];
            //	NSError *error = nil;
            
            if ( [server pin] && [[server pin] length] > 0 ) {
                [self startLookupAddressByPIN:[NSString stringWithFormat:@"http://imagebanklookup.appspot.com/lookup?pin=%@&",[server pin]]];
            } else {            
                [self startCheckingPasswordForServer:[server url] password:@""];
            }
            [self.serversTableView deselectRowAtIndexPath:indexPath animated:YES];
        }

    } 
}

-(void)tableView:(UITableView*)tableView willBeginEditingRowAtIndexPath:(NSIndexPath *)indexPath
{
}

- (void)setEditing:(BOOL)editing animated:(BOOL)animated {
    [super setEditing:editing animated:animated];
    [self.serversTableView setEditing:editing animated:YES];
    if ( !editing && [servers count] < 1 ) {
        self.editButtonItem.enabled = NO;
    }
}


-(void)startCheckingPasswordForServer:(NSString*)serverUrl password:(NSString*)password {
    self.serversTableView.alpha = 0.5f;
	self.connectingView.hidden = NO;
	[self.mainView bringSubviewToFront:connectingView];
	[self.connectingIndicator startAnimating];
	[self setSelectedServer:serverUrl];
	NSString *url = [NSString stringWithFormat:@"http://%@/files/getListAtRootDirectory?chk=YES&s=%@",serverUrl,password];
	TTURLRequest *request = [TTURLRequest requestWithURL:url delegate:self];
    [request setCachePolicy:TTURLRequestCachePolicyNoCache];
    [request setUserInfo:[TTUserInfo topic:@"check_password"]];
	request.response = self;
	request.httpMethod = @"GET";
	[self setCurrentRequest:request];
	[request send];
}	

-(void)startLookupAddressByPIN:(NSString*)serverUrl {
    self.serversTableView.alpha = 0.5f;
	self.connectingView.hidden = NO;
	[self.mainView bringSubviewToFront:connectingView];
	[self.connectingIndicator startAnimating];
    [self setSelectedServer:serverUrl];
	TTURLRequest *request = [TTURLRequest requestWithURL:serverUrl delegate:self];
    [request setCachePolicy:TTURLRequestCachePolicyNoCache];
    [request setUserInfo:[TTUserInfo topic:@"lookup_pin"]];
	request.response = self;
	request.httpMethod = @"GET";
	[self setCurrentRequest:request];
	[request send];    
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/


- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ( indexPath.section == 0 ) {
        
    } else if ( indexPath.section == 2  ) {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
		Server *serverToDelete = (Server *)[servers objectAtIndex:indexPath.row];
		[self.managedObjContext deleteObject:serverToDelete];
		NSError *error;
		if (![self.managedObjContext save:&error]) {
			// Handle the error.
			NSLog(@"Unresolved error when deleting server object: %@, %@, %@", error, [error userInfo],[error localizedDescription]);
		}
		[servers removeObjectAtIndex:indexPath.row];
		[tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:YES];
		if ( [servers count] == 0 ) {
            [tableView reloadData];
            if ( !self.isEditing ) {
                self.editButtonItem.enabled = NO;
            }
		}
    }   
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if ( section == 0 ) {
        return [NSString stringWithFormat:NSLocalizedString(@"On Your Device", @""),[UIDevice currentDevice].model];
    } else if ( section == 1 ) {
        return NSLocalizedString(@"On Your Local Network",@"");
    } else {
        return NSLocalizedString(@"Saved Remote Servers",@"");
    } 
}

#pragma mark
#pragma mark UI Actions

- (void) openFileListControllerWithServer: (NSString *) serverUrl  {
	NSDictionary *query = [NSDictionary dictionaryWithObjectsAndKeys:serverUrl,@"server_ip",
						   self.managedObjContext,@"managedObjectContext",nil];
    
    FolderContentsController *fc = [[FolderContentsController alloc] initWithNavigatorURL:nil query:query];
    [self.navigationController pushViewController:fc animated:YES];
    [fc release];
}

-(void)showPasswordAlert {
	UIAlertView *passwordAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Enter Server Password",@"") message:@""
														   delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel",nil) otherButtonTitles:NSLocalizedString(@"OK",nil), nil];
    [passwordAlert setAlertViewStyle:UIAlertViewStyleSecureTextInput];
    [[passwordAlert textFieldAtIndex:0] becomeFirstResponder];
	passwordAlert.tag = 120;	
	[passwordAlert show];
	[passwordAlert release];
}

-(void)showServerUnreachableAlert {
	UIAlertView *serverUnreachableAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Connection Error",@"Connection Error")
																	 message:NSLocalizedString(@"Sorry, we cannot connect to the server.  Please make sure you are connected to a network."
																							   ,@"Connection Error Description")
																	delegate:self 
														   cancelButtonTitle:NSLocalizedString(@"Cancel",nil)
														   otherButtonTitles:NSLocalizedString(@"Try again",@"Re-try connecting to server"),nil];
	[serverUnreachableAlert show];
	[serverUnreachableAlert release];
    self.serversTableView.alpha = 1.0f;
}

-(IBAction)cancelConnectToServer:(id)sender {
	self.connectingView.hidden = YES;
    self.serversTableView.alpha = 1.0f;
	if ( currentRequest != nil )
		[currentRequest cancel];
}

-(void)showHelp:(id)sender {
    HTMLHelpController *helpController = [[[HTMLHelpController alloc] init] autorelease];
    [helpController setHtmlFilenamePrefix:@"help"];
    [helpController setPresentedAsModal:YES];
    UINavigationController *navwrap = [[[UINavigationController alloc] initWithRootViewController:helpController] autorelease];
    [self presentModalViewController:navwrap
                            animated:YES];
}

-(void)revealSideMenu:(id)sender {
    [self.revealSideViewController pushOldViewControllerOnDirection:PPRevealSideDirectionLeft withOffset:[UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad ? 470.0 : 70.0 animated:YES];
}

#ifdef FREE_UPGRADABLE_VERSION
-(void) showAlertOnFirstLoad {
	NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
	NSDate *lastShownPopUpDate = [userDefaults objectForKey:@"lastShownPopUpDate"];
	NSLog(@"last shown pop up date = %@",lastShownPopUpDate);
//	long timeInterval = lastShownPopUpDate != nil ? (long) [[NSDate date] timeIntervalSinceDate:lastShownPopUpDate] : kSecondsBetweenShowingNagPopUp;
	// disable time-enabled pop up - only show once
	long timeInterval = lastShownPopUpDate != nil ? kSecondsBetweenShowingNagPopUp - 100 : kSecondsBetweenShowingNagPopUp;
	NSLog(@"time interval = %i",timeInterval);
	
	// Don't show pop up if it's been shown recently, as defined by kSecondsBetweenShowingNagPopUp, but show it for the very first time
	if ( timeInterval < kSecondsBetweenShowingNagPopUp || [MKStoreManager fullVersionUpgradePurchased] )
		return;
	
	[userDefaults setObject:[NSDate date] forKey:@"lastShownPopUpDate"];
	NSString *messageToBeShown = [NSString stringWithFormat:NSLocalizedString(@"You are using the free version of PhotoStreamr. You can view up to %i images in a folder and shuffle up to %i images.  To remove this restriction and the ads, please upgrade to the full version by tapping the Upgrade button.",
																			  @"Message that tells the user the free version has restrictions"),kMaxFolderImagesDisplayForFreeVersion,kMaxShuffleImagesDisplayForFreeVersion];
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Welcome to PhotoStreamr!"
																			  ,@"Pop up title that shows up when the app loads for the very first time")
													message:messageToBeShown
												   delegate:self cancelButtonTitle:NSLocalizedString(@"Not Now", @"Not Now") otherButtonTitles:NSLocalizedString(@"Upgrade",@"Upgrade to full version button"),nil];
	alert.tag = kNaggingPopUpAlertTag;
	[alert show];
	[alert release];
}

-(void)showUpgradeView:(id)sender {
	if ([SKPaymentQueue canMakePayments]) {
		BuyUpgradeController *upgradeController = [[BuyUpgradeController alloc] init];
		[upgradeController setManagedObjContext:self.managedObjContext];
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
            upgradeController.modalPresentationStyle = UIModalPresentationFormSheet;
		[self.navigationController presentModalViewController:upgradeController animated:YES];
//		[upgradeController release];
	} else {
		// TODO: show alert view
		NSLog(@"Cannot make payments!");
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Cannot purchase upgrade",
																				  @"Alert title when tapping Upgrade button but not able to make payments")
														message:NSLocalizedString(@"Sorry, you are not authorized to purchase from the App Store",
																				  @"Sorry, you are not authorized to purchase from the App Store")
													   delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];
		[alert release];
	}
}


#endif

#pragma mark -
#pragma mark PopOverContentDelegate implementation

- (void)showUpgradeViewController {
#ifdef FREE_UPGRADABLE_VERSION
    [self showUpgradeView:nil];
#endif
}

-(void)showSettingsController {
	IASKAppSettingsViewController *appSettingsViewController = [[IASKAppSettingsViewController alloc] initWithNibName:@"IASKAppSettingsView" bundle:nil];
	appSettingsViewController.delegate = self;
	UINavigationController *aNavController = [[UINavigationController alloc] initWithRootViewController:appSettingsViewController];
    [appSettingsViewController setShowCreditsFooter:NO];
    appSettingsViewController.showDoneButton = YES;
	aNavController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
	if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
		aNavController.modalPresentationStyle = UIModalPresentationFormSheet;
    [self presentModalViewController:aNavController animated:YES];
	[appSettingsViewController release];
    [aNavController release];
}

-(void)popToRootController {
    // already at root
}

#pragma mark
#pragma mark UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
	[textField resignFirstResponder];
	UIAlertView *alertView = (UIAlertView *) [self.view viewWithTag:120];
	[alertView dismissWithClickedButtonIndex:1 animated:YES];
	return NO;
}

#pragma mark
#pragma mark UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
	if ( buttonIndex == 0 ) {
		[alertView dismissWithClickedButtonIndex:0 animated:YES];
#ifdef FREE_UPGRADABLE_VERSION
	} else if ( alertView.tag == kNaggingPopUpAlertTag || alertView.tag == kUpgradePopUpAlertTag ) {
		[alertView dismissWithClickedButtonIndex:0 animated:YES];
		[self showUpgradeView:alertView];
#endif
	} else {
		// check password field, if password ok push view controller
        NSString *hashedPassword = @"";
        if ( alertView.tag == 120 ) {
            // alert with password field
            UITextField *textField = [alertView textFieldAtIndex:0];
            hashedPassword = [Utilities hashedSecret:textField.text];
        }
        if ( [selectedServer hasPrefix:@"http://imagebanklookup.appspot.com"] ) {
            [self startLookupAddressByPIN:selectedServer];
        } else {
            if ( [[NSUserDefaults standardUserDefaults] boolForKey:@"pref_save_server_passwords"] ) {
                // save password
                TTDINFO(@"Saving password");
                [Utilities storePassword:hashedPassword forServerUrl:selectedServer];
            }
            [self startCheckingPasswordForServer:selectedServer password:hashedPassword];
        }
	}
    self.serversTableView.alpha = 1.0f;
}

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/


/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark
#pragma mark TTURLResponse
- (NSError*)request:(TTURLRequest*)request processResponse:(NSHTTPURLResponse*)response data:(id)data {
    TTUserInfo *info = [request userInfo];
    if ( [[info topic] isEqualToString:@"lookup_pin"] ) {
        return [self processResponseForLookupAddressByPIN:data];
    } else {
        return [self processResponseForCheckPassword:data];
    }
}

-(NSError*)processResponseForCheckPassword:(id)data {
	[self.connectingIndicator stopAnimating];
	self.connectingView.hidden = YES;
	[self.mainView sendSubviewToBack:connectingView];
	NSString *responseBody = [[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] autorelease];
	NSString *responseText = [responseBody stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
	if ( [responseText hasPrefix:@"OK"] ) {
		// check server version
		NSArray *responseTextParts = [responseText componentsSeparatedByString:@"-"];
		if ( [responseTextParts count] != 3 || ![(NSString*)[responseTextParts objectAtIndex:1] isEqualToString:@"2.0"] ) {
			TTAlert(NSLocalizedString(@"This version of PhotoStreamr requires a newer version of PhotoStreamr Home.  For the best experience, please download and reinstall PhotoStreamr Home on your PC or Mac from http://imagebank4ios.weebly.com",@"PhotoStreamr Home outdated message"));
			return nil;
		}
		// store the hashed PF password of the server
		[[PrivateFolderPasswordChecker sharedInstance] setHashedPFPassword:(NSString*)[responseTextParts objectAtIndex:2]];
        // save the server password
        
		[self openFileListControllerWithServer:self.selectedServer];
		return nil;
	}
	[self showPasswordAlert];
	return nil;    
}

-(NSError*)processResponseForLookupAddressByPIN:(id)data {
	NSString *responseBody = [[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] autorelease];
    SBJSON *parser = [[SBJSON alloc] init];
	NSDictionary *fileData = [parser objectWithString:responseBody];
	TTDINFO(@"json parsing error: %@",[parser errorTrace]);
	[parser release];
    NSString *ipAddress = (NSString*)[fileData objectForKey:@"ip_address"];
    NSString *port = (NSString*)[fileData objectForKey:@"port"];
    TTDINFO(@"parsed json for lookup by PIN: ip: %@ , port = %@",ipAddress,port);
    self.selectedServer = [NSString stringWithFormat:@"%@:%@",ipAddress,port];
    NSString *password = @"";
    if ( [[NSUserDefaults standardUserDefaults] boolForKey:@"pref_save_server_passwords"] ) {
        NSString *savedPassword = [Utilities savedPasswordForServerUrl:self.selectedServer];         
        if ( [savedPassword length] > 0 ) {
            password = savedPassword;
        }
    }        
    [self startCheckingPasswordForServer:self.selectedServer password:password];
    return nil;
}


#pragma mark
#pragma mark TTURLRequestDelegate
- (void)request:(TTURLRequest*)request didFailLoadWithError:(NSError*)error {
	self.connectingView.hidden = YES;
	[self.mainView sendSubviewToBack:connectingView];
	[self showServerUnreachableAlert];
}

- (void)dealloc {
	[servers release];
    [choosableBonjourServers release];
	[managedObjContext release];
	[serversTableView release];
	[selectedServer release];
	[currentRequest release];
    self.serviceBrowser = nil;
	[serviceBrowser release];

    [super dealloc];
}

#pragma mark
#pragma mark IAskSettingsDelegate
- (void)settingsViewControllerDidEnd:(IASKAppSettingsViewController*)sender {
    [self dismissModalViewControllerAnimated:YES];
    if ( ![[NSUserDefaults standardUserDefaults] boolForKey:@"pref_save_server_passwords"] ) {
        [Utilities destroySavedPasswords];
    }
}

#pragma mark -
#pragma mark PPRevealSideViewControllerDelegate

-(void) pprevealSideViewController:(PPRevealSideViewController *)controller didPopToController:(UIViewController *)centerController {
    self.serversTableView.userInteractionEnabled = YES;
    self.navigationItem.leftBarButtonItem.enabled = YES;
}

- (void) pprevealSideViewController:(PPRevealSideViewController *)controller didPushController:(UIViewController *)pushedController {
    self.serversTableView.userInteractionEnabled = NO;
        self.navigationItem.leftBarButtonItem.enabled = NO;
}


@end

