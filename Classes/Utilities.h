//
//  Utilities.h
//  ImageBank
//
//  Created by yoshi on 6/6/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CommonCrypto/CommonDigest.h>
#import "File.h"

#define UIColorFromRGB(rgbValue) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

@interface Utilities : NSObject {
	
}

CGFloat DegreesToRadians(CGFloat degrees);

//generates md5 hash from a string
+ (NSString *) returnMD5Hash:(NSString*)concat;
+(NSString *) hashedSecret:(NSString*)secret;

+(BOOL) isUpgradedVersion;
+(NSString*) encodeToPercentEscapeString:(NSString *)string;
+(NSString*)getLocalizedHtmlFilenameUsingPrefix :(NSString*)filePrefix;

+(BOOL)isOfflineFolder:(NSString*)path;
+(BOOL)isOfflinePrivateFoldersPasswordSetup;
+(void)setOfflinePrivateFoldersPassword:(NSString*)newPw;
+(NSString*)getOfflinePrivateFoldersPassword;
+(BOOL)isOfflinePrivateFolder:(NSString*)path;
+(void)enableOfflinePrivateFolder:(NSString *)fullPath;
+(void)disableOfflinePrivateFolder:(NSString*) fullPath;
+(NSMutableArray*) offlineFolders;
+(NSString*)savedPasswordForServerUrl:(NSString*) serverUrl;
+(void) storePassword:(NSString*)password forServerUrl:(NSString*)serverUrl;
+(void) destroySavedPasswords;
+(DirectoryFile*) lastSavedFolder;
+(void) storeLastSavedFolder:(NSString *)folderName isPrivate:(BOOL)isPrivate;
+(void) storeFolderSortOptionsWithField:(NSString*)field isAscending:(BOOL)isAsc;
+(NSDictionary*) folderSortOptions;
+(CGFloat) thumbnailWidthForScreenWidth:(CGFloat)width;
+(NSUInteger) thumbnailCountForScreenWidth:(CGFloat)width;
+(CGFloat) thumbnailSpacingForScreenWidth:(CGFloat)width;
+ (NSString *)contentTypeForImageData:(NSData *)data;
+(UIColor*) globalAppTintColor;

@end