//
//  OfflineFileListModel.h
//  ImageBankFree
//
//  Created by Yoshi Sugawara on 5/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FileListModel.h"

@interface OfflineFileListModel : FileListModel {
    NSString *directoryOfImages;
    NSMutableArray *imageFiles;
    BOOL isModelLoaded;
    BOOL isModelLoading;
}

@property (nonatomic,readwrite,retain) NSString *directoryOfImages;
@property (nonatomic,readwrite,retain)     NSMutableArray *imageFiles;

-(id) initWithDocumentsDirectory:(NSString*)docDir;

@end
