//
//  FileUploadNotifier.h
//  ImageBankFree
//
//  Created by yoshi on 4/12/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "File.h"
#import "UploadOperation.h"
#import "FolderContentsController.h"

@interface FileUploadNotifier : NSObject<TTURLRequestDelegate> {
	BOOL running;
	NSMutableArray* queue;
	FolderContentsController *callingController;
}

+(id)sharedInstance;
-(void)notifyComplete:(UploadOperation*)operation;
-(void)scheduleProcessQueue;
-(void)addUploadOperation:(NSData*)imageData 
			 forDirectory:(DirectoryFile*)dir 
		   withController:(FolderContentsController*)controller
			  orientation:(UIInterfaceOrientation)orientation;

@property(nonatomic,readwrite) BOOL running;
@property(nonatomic,readwrite,retain) NSMutableArray* queue;	
@property(nonatomic,readwrite,assign) FolderContentsController *callingController;

@end
