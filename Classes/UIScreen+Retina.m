//
//  UIScreen+Retina.m
//  ImageBankFree
//
//  Created by Yoshi Sugawara on 4/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UIScreen+Retina.h"

@implementation UIScreen (Retina)

- (BOOL) isRetina {
    return [self respondsToSelector:@selector(displayLinkWithTarget:selector:)] && (self.scale == 2.0);
}

-(BOOL) isIPadRetina {
    return [UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad &&
        [self isRetina];
}

@end
