//
//  SharedQueue.m
//  ImageBankFree
//
//  Created by Yoshi Sugawara on 7/29/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "NSOperationQueue+SharedQueue.h"


@implementation NSOperationQueue (SharedQueue)

+(NSOperationQueue*)sharedOperationQueue;
{
    static NSOperationQueue* sharedQueue = nil;
    if (sharedQueue == nil) {
        sharedQueue = [[NSOperationQueue alloc] init];
    }
    return sharedQueue;
}

@end
