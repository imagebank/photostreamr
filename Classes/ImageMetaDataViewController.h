//
//  ImageMetaDataViewControllerViewController.h
//  ImageBankFree
//
//  Created by Yoshi Sugawara on 4/26/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MovableViewWithInsetView.h"

@interface ImageMetaDataViewController : UIViewController<TTURLResponse,TTURLRequestDelegate,UITableViewDelegate, UITableViewDataSource> {
    IBOutlet UIView *view;
    IBOutlet UITableView *tableView;
    IBOutlet UIActivityIndicatorView *activityIndicator;
    IBOutlet UILabel *loadingLabel;
    IBOutlet UITableViewCell *customCell;
    
    NSString *metadataUrl;
    NSDictionary *metadataDataSource;
}

-(void) loadMetadataContent;

@property (nonatomic,retain) IBOutlet UITableView *tableView;
@property (nonatomic,retain) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (nonatomic,retain) IBOutlet UILabel *loadingLabel;
@property (nonatomic,retain) NSString *metadataUrl;
@property (nonatomic,assign) IBOutlet UITableViewCell *customCell;
@property (nonatomic,retain) NSDictionary *metadataDataSource;

@end
