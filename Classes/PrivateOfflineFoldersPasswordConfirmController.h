//
//  PrivateOfflineFoldersPasswordConfirmController.h
//  ImageBankFree
//
//  Created by Yoshi Sugawara on 5/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PrivateOfflineFoldersPasswordConfirmController : UIViewController<UITextFieldDelegate> {
    NSString *password;
    IBOutlet UITextField *passwordConfirmField;
    IBOutlet UIButton *finishButton;
    IBOutlet UIScrollView *scrollView;
}

@property (nonatomic,readwrite,retain) NSString *password;

-(IBAction)verifyAndSavePassword:(id)sender;

@end
