//
//  DownloadURLOperation.h
//  ImageBankFree
//
//  Created by Yoshi Sugawara on 12/8/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PopUpNotificationView.h"

@interface DownloadURLOperation : NSOperation {
    // In concurrent operations, we have to manage the operation's state
    BOOL executing_;
    BOOL finished_;
    
    // The actual NSURLConnection management
    NSURL*    connectionURL_;
    NSURLConnection*  connection_;
    NSMutableData*    data_;
    
    // growl-like notification support
    PopUpNotificationView *popUpView;
    UIInterfaceOrientation orientation;
}

@property (nonatomic,readonly) NSError* error;
@property (nonatomic,readonly) NSMutableData *data;
@property (nonatomic,readonly) NSURL *connectionURL;
@property (nonatomic,readwrite,retain) 	PopUpNotificationView *popUpView;
@property (nonatomic,readwrite,assign)	UIInterfaceOrientation orientation;

- (id)initWithURL:(NSURL*)url withOrientation:(UIInterfaceOrientation)orientation;
-(void)slideInPopUpView;
-(void) hideView;
-(void)updateStatusMessageSuccess;
-(void)updateStatusMessageFail;

@end
