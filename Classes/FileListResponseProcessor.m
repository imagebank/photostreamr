//
//  FileListResponseProcessor.m
//  ImageBank
//
//  Created by yoshi on 5/13/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "FileListResponseProcessor.h"
#import "File.h"
#import "JSON.h"


@implementation FileListResponseProcessor

@synthesize files, dirs, totalNumberOfPhotos;

-(id)init {
	if ( self = [super init] ) {
		self.files = [[NSMutableArray alloc] init];
        self.dirs = [[NSMutableArray alloc] init];
	}
	return self;
}

-(void)dealloc {
	[files release];
    [dirs release];
	[super dealloc];
}

//////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark TTURLResponse

- (NSError*)request:(TTURLRequest*)request processResponse:(NSHTTPURLResponse*)response data:(id)data
{
	TTDINFO(@"Start processing response");
	NSString *responseBody = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    TTDINFO(@"response body = %@",responseBody);
	SBJSON *parser = [[SBJSON alloc] init];
	NSArray *fileData = [parser objectWithString:responseBody];
	TTDINFO(@"json parsing error: %@",[parser errorTrace]);
	[responseBody release];
	[parser release];

    [self setTotalNumberOfPhotos:[(NSString*)[fileData objectAtIndex:0] intValue]];
    NSArray *directoryFiles = [fileData objectAtIndex:1];
    NSArray *imageFiles = [fileData objectAtIndex:2];
    
	for (id data in directoryFiles) {
		TTDINFO(@"looping thru directory data");
		// TODO: use a static method in File to create the file maybe
		File *file = nil;
        TTDINFO(@"file url = %@",(NSString *)[(NSDictionary *)data objectForKey:@"url"]);
			file = [[DirectoryFile alloc] initWithName:(NSString *)[(NSDictionary *)data objectForKey:@"name"]
												   url:(NSString *)[(NSDictionary *)data objectForKey:@"url"]
											imageCount:0
											  dirCount:0];
			BOOL isPrivate = [(NSNumber*)[(NSDictionary*)data objectForKey:@"is_private"] intValue] == 1;
			[(DirectoryFile*) file setIsPrivate:isPrivate];
        
            BOOL isiPhoto = [(NSDictionary*)data objectForKey:@"is_iphoto"] != nil ? 
                [(NSNumber*)[(NSDictionary*)data objectForKey:@"is_iphoto"] intValue] == 1 :
                NO;
            [(DirectoryFile*) file setIsiPhoto:isiPhoto];
			
			// special case: make this a RandomDirectoryFile if the name indicates it's a random directory
			if ( [file.name isEqualToString:@"__imagebank_random_images"] ) {
				File *newFile = [[[RandomDirectoryFile alloc] initWithName:file.name
															 url:file.url
													  imageCount:0
														dirCount:0] autorelease];
				[(RandomDirectoryFile*) newFile setIsPrivate:isPrivate];
				[file release];
				file = [newFile retain];
			}				
			
		[self.dirs addObject:file];
		[file release];
	}

    TTDINFO(@"looping thru image file data");
    for (id data in imageFiles) {
        TTDINFO(@"image filename = %@",(NSString *)[(NSDictionary *)data objectForKey:@"name"]);
        File *file = nil;
        CGSize isize = CGSizeMake([(NSString *)[(NSDictionary *)data objectForKey:@"size_x"] floatValue],
                                  [(NSString *)[(NSDictionary *)data objectForKey:@"size_y"] floatValue]);
        file = [[ImageFile alloc] initWithName:(NSString *)[(NSDictionary *)data objectForKey:@"name"]
                                           url:(NSString *)[(NSDictionary *)data objectForKey:@"url"]
                                          size:isize
                                  thumbnailUrl:(NSString *)[(NSDictionary *)data objectForKey:@"thumbnail_url"]
                             smallThumbnailUrl:(NSString *)[(NSDictionary*)data objectForKey:@"small_thumbnail_url"]
                                   metadataUrl:(NSString*) [(NSDictionary*)data objectForKey:@"metadata_url"]];
        [self.files addObject:file];
        [file release];
    }
	
	return nil;
}

- (NSString *)format
{
	return @"json";
}

@end
