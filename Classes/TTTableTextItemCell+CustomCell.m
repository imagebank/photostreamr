//
//  TTTableTextItemCell+CustomCell.m
//  ImageBankFree
//
//  Created by Yoshi Sugawara on 12/7/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "TTTableTextItemCell+CustomCell.h"

static const UILineBreakMode kLineBreakMode = UILineBreakModeWordWrap;

@implementation TTTableTextItemCell (CustomCell)

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString*)identifier {
    if (self = [super initWithStyle:style reuseIdentifier:identifier]) {
        self.textLabel.highlightedTextColor = TTSTYLEVAR(highlightedTextColor);
        self.textLabel.lineBreakMode = kLineBreakMode;
        self.textLabel.numberOfLines = 0;
        UIImage *image = [UIImage imageNamed:@"cell-gradient.png"];
        self.backgroundColor = [UIColor colorWithPatternImage:image];
        self.textLabel.backgroundColor = [UIColor clearColor];
    }
    
    return self;
}

@end
