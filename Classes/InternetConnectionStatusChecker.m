//
//  InternetConnectionStatusChecker.m
//  ImageBankFree
//
//  Created by Yoshi Sugawara on 8/20/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "InternetConnectionStatusChecker.h"

@implementation InternetConnectionStatusChecker

static InternetConnectionStatusChecker *sharedInstance = nil;

@synthesize wifiAvailable;

#pragma mark
#pragma mark Singleton methods

// Get the shared instance and create it if necessary.
+ (InternetConnectionStatusChecker*)sharedInstance {
    if (sharedInstance == nil) {
        sharedInstance = [[super allocWithZone:NULL] init];
    }	
    return sharedInstance;
}


// We don't want to allocate a new instance, so return the current one.
+ (id)allocWithZone:(NSZone*)zone {
    return [[self sharedInstance] retain];
}

// Equally, we don't want to generate multiple copies of the singleton.
- (id)copyWithZone:(NSZone *)zone {
    return self;
}

// Once again - do nothing, as we don't have a retain counter for this object.
- (id)retain {
    return self;
}

// Replace the retain counter so we can never release this object.
- (NSUInteger)retainCount {
    return NSUIntegerMax;
}

// This function is empty, as we don't want to let the user release this object.
- (oneway void)release {
	
}

//Do nothing, other than return the shared instance - as this is expected from autorelease.
- (id)autorelease {
    return self;
}

- (id)init
{
    self = [super init];
    if (self) {
        TTDINFO("Initializing connections status checker");
        // check for internet connection
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkNetworkStatus:) name:kReachabilityChangedNotification object:nil];
        
        internetReachable = [[Reachability reachabilityForInternetConnection] retain];
        [internetReachable startNotifier];
        
        // check if a pathway to a random host exists
        hostReachable = [[Reachability reachabilityWithHostName:
                          @"www.google.ca"] retain];
        [hostReachable startNotifier];

        // now patiently wait for the notification
    }
    
    return self;
}

- (void) checkNetworkStatus:(NSNotification *)notice
{
    // called after network status changes
    
    NetworkStatus currentNetworkStatus = [internetReachable currentReachabilityStatus];
    switch (currentNetworkStatus)
    
    {
        case NotReachable:
        {
            TTDINFO(@"The internet is down.");
            wifiAvailable = NO;
            break;
            
        }
        case ReachableViaWiFi:
        {
            TTDINFO(@"The internet is working via WIFI.");
            wifiAvailable = YES;
            break;
            
        }
        case ReachableViaWWAN:
        {
            TTDINFO(@"The internet is working via WWAN.");
            wifiAvailable = NO;
            break;
            
        }
    }    
    
    NetworkStatus hostStatus = [hostReachable currentReachabilityStatus];
    switch (hostStatus)
    
    {
        case NotReachable:
        {
            TTDINFO(@"A gateway to the host server is down.");            
            break;
            
        }
        case ReachableViaWiFi:
        {
            TTDINFO(@"A gateway to the host server is working via WIFI.");
            break;
            
        }
        case ReachableViaWWAN:
        {
            TTDINFO(@"A gateway to the host server is working via WWAN.");
            break;
            
        }
    }

}

-(BOOL) useLowQualityImages {
    BOOL enableLowQuality = (BOOL) [[NSUserDefaults standardUserDefaults] boolForKey:@"pref_low_quality_3g"];
    return enableLowQuality && !self.wifiAvailable;
}

@end
