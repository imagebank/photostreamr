//
//  OfflineFileListModel.m
//  ImageBankFree
//
//  Created by Yoshi Sugawara on 5/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "OfflineFileListModel.h"
#import "JSON.h"
#import "File.h"

@implementation OfflineFileListModel

@synthesize directoryOfImages, imageFiles;

-(id) initWithDocumentsDirectory:(NSString*)docDir {
    if ( (self = [super init])) {
        [self setDirectoryOfImages:docDir];
        [self setImageFiles:[[NSMutableArray alloc] init]];
    }
    return self;
}

-(NSArray*) files {
    return [self imageFiles];
}

-(NSArray*) dirs {
    return [[[NSArray alloc] init] autorelease];
}

-(NSInteger)imageCount {
    return [imageFiles count];
}

-(BOOL) isLoaded {
    return isModelLoaded;
}

-(BOOL) isLoading {
    return isModelLoading;
}

- (void)load:(TTURLRequestCachePolicy)cachePolicy more:(BOOL)more {
    isModelLoading = YES;
    // read json in directory
    NSError *error = nil;
    NSString *modelJson = [NSString stringWithContentsOfFile:[directoryOfImages stringByAppendingPathComponent:@"model.json"] encoding:NSUTF8StringEncoding error:&error];
   	SBJSON *parser = [[SBJSON alloc] init];
    NSArray *fileData = [parser objectWithString:modelJson];
    TTDINFO(@"looping thru offline image file data");
    for (id data in fileData) {
        TTDINFO(@"offline image filename = %@",(NSString *)[(NSDictionary *)data objectForKey:@"name"]);
        File *file = nil;
        CGSize isize = CGSizeMake([(NSString *)[(NSDictionary *)data objectForKey:@"size_x"] floatValue],
                                  [(NSString *)[(NSDictionary *)data objectForKey:@"size_y"] floatValue]);
        file = [[ImageFile alloc] initWithName:(NSString *)[(NSDictionary *)data objectForKey:@"name"]
                                           url:(NSString *)[(NSDictionary *)data objectForKey:@"url"]
                                          size:isize
                                  thumbnailUrl:(NSString *)[(NSDictionary *)data objectForKey:@"thumbnail_url"]
                             smallThumbnailUrl:@""
                                   metadataUrl:(NSString*) [(NSDictionary*)data objectForKey:@"metadata_url"]];
        NSDictionary* fileAttribs = [[NSFileManager defaultManager] attributesOfItemAtPath:[directoryOfImages stringByAppendingPathComponent:[file name]] error:nil];
        if ( fileAttribs != nil ) {
            [file setModifiedDate:[fileAttribs fileModificationDate]];
        }
        [self.imageFiles addObject:file];
        [file release];
    }
    
    
    // sort
    [self.imageFiles sortUsingComparator:^NSComparisonResult(id first, id second) {
        ImageFile *firstFile = (ImageFile*) first;
        ImageFile *secondFile = (ImageFile*) second;
        if ( [self.sortByField isEqualToString:@"date"]) {
            return sortAscending ?
                [[firstFile modifiedDate] compare:[secondFile modifiedDate]] :
                [[secondFile modifiedDate] compare:[firstFile modifiedDate]];
        }
        return sortAscending ?
            [[[firstFile name] uppercaseString] compare:[[secondFile name] uppercaseString]] :
            [[[secondFile name] uppercaseString] compare:[[firstFile name] uppercaseString]];
    }];
    
    isModelLoading = NO;
    isModelLoaded = YES;
    [self didFinishLoad];
}

@end
