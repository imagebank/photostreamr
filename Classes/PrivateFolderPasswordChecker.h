//
//  PrivateFolderPasswordChecker.h
//  ImageBankFree
//
//  Created by yoshi on 2/23/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface PrivateFolderPasswordChecker : NSObject {
	NSString *hashedPFPassword;
	BOOL allowAccess;
    BOOL allowOfflineAccess;
    NSDate *dateWentIntoBackground;
}

+(id)sharedInstance;
-(BOOL)canAccessWithHashedPassword:(NSString*)hashedPassword;
-(BOOL)canAccessOfflineWithPassword:(NSString*)password;
- (BOOL)showButton;
- (BOOL) shouldCheckAccess;

@property(nonatomic,readwrite,retain) NSString *hashedPFPassword;
@property(nonatomic,assign) BOOL allowAccess;
@property(nonatomic,assign) BOOL allowOfflineAccess;
@property(nonatomic,retain) NSDate *dateWentIntoBackground;

@end
