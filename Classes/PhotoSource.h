//
//  PhotoSource.h
//  ImageBank
//
//  Created by yoshi on 5/14/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Three20/Three20.h>
#import "File.h"
#import "FileListModel.h"

@class Photo;

@interface PhotoSource : TTURLRequestModel <TTPhotoSource,TTURLResponse> {
	NSString* _title;
	DirectoryFile* _directory;
	ImageFile* _image;
	NSMutableArray* _photos;
	NSInteger _totalNumberOfPhotos;
	BOOL _forceLoadingFromNetwork;
    
    NSString *sortByField;
    BOOL sortAscending;

}

@property(nonatomic,assign) NSInteger totalNumberOfPhotos;
@property (nonatomic,retain) NSString *sortByField;
@property (nonatomic,assign) BOOL sortAscending;


- (id)initWithTitle:(NSString*)titles
		  directory:(DirectoryFile*)dir
			 photos:(NSArray*)photos;

- (id)initWithTitle:(NSString*)titles
		  directory:(DirectoryFile*)dir
totalNumberOfPhotos:(NSInteger)totalNumberOfPhotos
forceReloadFromNetwork:(BOOL)reload;

- (id) initWithPhoto:(Photo*)photo;
-(void)setPhotosFromFileListModel:(FileListModel*)flm;

@end

/////////////////

@interface Photo : NSObject <TTPhoto> {
	id<TTPhotoSource> _photoSource;
	NSString* _thumbURL;
	NSString* _smallURL;
	NSString* _URL;
	CGSize _size;
	NSInteger _index;
	NSString* _caption;
    NSString* _metadataUrl;
    NSString* _filename;
    BOOL _isSelected;
}

- (id)initWithURL:(NSString*)URL smallURL:(NSString*)smallURL size:(CGSize)size;

- (id)initWithURL:(NSString*)URL smallURL:(NSString*)smallURL size:(CGSize)size
		  caption:(NSString*)caption metadataUrl:(NSString*)metadataUrl;
-(NSString*) getMetadataUrl;
-(NSString*) getFilename;
@property (nonatomic,copy) NSString* metadataUrl;
@property (nonatomic,copy) NSString* filename;
@property (nonatomic,assign) BOOL isSelected;
@end
