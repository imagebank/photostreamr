//
//  AddServerController.h
//  PhotoStreamr
//
//  Created by yoshi on 6/13/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Three20/Three20.h>

@interface AddServerController : TTViewController<UITextFieldDelegate
> {
	IBOutlet UITextField *nameField;
	IBOutlet UITextField *addressField;
	IBOutlet UITextField *portField;
    
    IBOutlet UITextField *pinField;
    BOOL addByPIN;
	NSManagedObjectContext *managedObjectContext;

}

- (id)initWithNavigatorURL:(NSURL*)URL query:(NSDictionary*)query;
-(void)showAlertWithTitle:(NSString*)title msg:(NSString*)msg;
-(IBAction)createServer:(id)sender;

@property (nonatomic,retain) IBOutlet UITextField *nameField;
@property (nonatomic,retain) IBOutlet UITextField *addressField;
@property (nonatomic,retain) IBOutlet UITextField *portField;
@property (nonatomic,retain) IBOutlet UITextField *pinField;
@property (readwrite,nonatomic,retain) 	NSManagedObjectContext *managedObjectContext;


@end
