//
//  FolderContentsSearchResultsController.h
//  ImageBank
//
//  Created by yoshi on 11/3/12.
//
//

#import <Three20/Three20.h>
#import <Foundation/Foundation.h>

@class FolderContentsController;

@interface FolderContentsSearchResultsController : NSObject<UISearchDisplayDelegate,UITableViewDataSource,TTURLResponse,UISearchBarDelegate,UITableViewDelegate> {
    FolderContentsController *parentController;
    UISearchDisplayController *searchDisplayController;
    NSInteger resultCount;
    NSInteger filenameResultCount;
    NSInteger metadataResultCount;
    BOOL isSearchEnabled;
    BOOL isSearchIndexRunning;
    BOOL isSearchIndexBuildingFromScratch;
    BOOL isSearchNotAvailable;
}

@property (nonatomic,retain) FolderContentsController *parentController;
@property (nonatomic,retain) UISearchDisplayController *searchDisplayController;

@end
