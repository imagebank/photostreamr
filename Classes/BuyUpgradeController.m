//
//  BuyUpgradeController.m
//  PhotoStreamr
//
//  Created by yoshi on 8/4/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "BuyUpgradeController.h"
#import "MKStoreManager.h"


@implementation BuyUpgradeController

@synthesize upgradeNowLabel,managedObjContext, fullVersionUpgradeProduct, purchasingNowLabel;

/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        // Custom initialization
    }
    return self;
}
*/

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	purchaseButton.enabled = NO;
    restoreButton.enabled = NO;
	[self requestProductData];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    if (_productRequest != nil ) {
        _productRequest = nil;
        [_productRequest release];
    }
}

-(NSUInteger)supportedInterfaceOrientations {
	if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
		return UIInterfaceOrientationMaskAll;
	} else {
		return UIInterfaceOrientationMaskPortrait;
	}
}

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
	if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        return YES;
    }
    return UIInterfaceOrientationIsPortrait(toInterfaceOrientation);
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (IBAction)cancel:(id)sender {
	[self dismissModalViewControllerAnimated:YES];
}

- (void) requestProductData {
	NSLog(@"Making request to Store..");
	activityIndicator.hidden = NO;
	[activityIndicator startAnimating];
	_productRequest = [[SKProductsRequest alloc] initWithProductIdentifiers: [NSSet setWithObject:@"full_version_upgrade"]];
	_productRequest.delegate = self;
	[_productRequest start];
}

- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response {
	NSLog(@"Got response from Store..");
	[activityIndicator stopAnimating];
    NSArray *products = response.products;
	NSLog(@"Products = %@",products);
    // populate UI
	[self setFullVersionUpgradeProduct:[products objectAtIndex:0]];
	NSLog(@"Product Name = %@",[self.fullVersionUpgradeProduct localizedTitle]);
	NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
	[numberFormatter setFormatterBehavior:NSNumberFormatterBehavior10_4];	
	[numberFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
	[numberFormatter setLocale:self.fullVersionUpgradeProduct.priceLocale];
	NSString *formattedString = [numberFormatter stringFromNumber:self.fullVersionUpgradeProduct.price];
	NSLog(@"Product price = %@",formattedString);
    
	[numberFormatter release];
	[upgradeNowLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:14]];
	[upgradeNowLabel setText:[NSString stringWithFormat:NSLocalizedString(@"Upgrade Now for %@!",
																		  @"Upgrade now label with price"),formattedString]];
	purchaseButton.enabled = YES;
    restoreButton.enabled = YES;
    [request autorelease];
}

- (IBAction) buyUpgrade:(id)sender {
	purchaseInProgressView.hidden = NO;
	[purchasingActivityIndicator startAnimating];
    purchasingNowLabel.text = NSLocalizedString(@"Purchasing Full Version",@"");
	[self.view bringSubviewToFront:purchaseInProgressView];
	[MKStoreManager setDelegate:self];
	[[MKStoreManager sharedManager] buyFullVersionUpgrade:self.fullVersionUpgradeProduct];
}

-(IBAction) restoreUpgrade:(id)sender {
	purchaseInProgressView.hidden = NO;
	[purchasingActivityIndicator startAnimating];
	[self.view bringSubviewToFront:purchaseInProgressView];
    purchasingNowLabel.text = NSLocalizedString(@"Restoring Full Version",@"");
	[MKStoreManager setDelegate:self];
    [[MKStoreManager sharedManager] restoreFullVersionUpgrade];
}

#pragma mark
#pragma mark SKRequestDelegate
- (void)request:(SKRequest *)request didFailWithError:(NSError *)error {
	[self dismissModalViewControllerAnimated:YES];
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Unable to connect to the App Store",
																			  @"Alert pop-up title when cannot connect to App Store")
													message:NSLocalizedString(@"Cannot connect to the App Store.  Please check your network connection.",
																			  @"Error message when trying to purchase content but there is no internet connection")
												   delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
	[alert show];
	[alert release];
}

#pragma mark
#pragma mark MKStoreKitDelegate
- (void)productPurchased:(NSString *)productId {
	[self dismissModalViewControllerAnimated:YES];
    [self.navigationController popToRootViewControllerAnimated:YES];
//	TTNavigator *navigator = [TTNavigator navigator];
//	[navigator removeAllViewControllers];
//	TTURLAction *action = [TTURLAction actionWithURLPath:@"tt://chooseserver?"];
//	[action applyQuery:[NSDictionary dictionaryWithObjectsAndKeys:self.managedObjContext,@"managedObjectContext",
//						nil]];
//	[navigator openURLAction:action];
}

- (void)productPurchaseCancelled:(NSString *)productId {
	[purchasingActivityIndicator stopAnimating];
	purchaseInProgressView.hidden = YES;
}

- (void)productPurchaseFailed:(SKPaymentTransaction *)transaction {
	[purchasingActivityIndicator stopAnimating];
	purchaseInProgressView.hidden = YES;	
	NSString *messageToBeShown = nil;
	if ( [transaction.error localizedFailureReason] == nil ) {
		messageToBeShown = NSLocalizedString(@"Cannot connect to the App Store.  Please check your network connection.",
											 @"Error message when trying to purchase content but there is no internet connection");
	} else {
		messageToBeShown = 
			[NSString stringWithFormat:NSLocalizedString(@"Reason: %@, You can try: %@"
													  ,@"Payment failure reason"),
			 						   [transaction.error localizedFailureReason], [transaction.error localizedRecoverySuggestion]];
	}
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Unable to complete your purchase",
																			  @"Alert pop-up title when payment failed")
													message:messageToBeShown
												   delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
	[alert show];
	[alert release];
}

-(void)productRestoreFailed {
    [purchasingActivityIndicator stopAnimating];
	purchaseInProgressView.hidden = YES;
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Unable to restore your purchase",
																			  @"Alert pop-up title when payment failed")
													message:@""
												   delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
	[alert show];
	[alert release];    
}

- (void)dealloc {
	[managedObjContext release];
    [fullVersionUpgradeProduct release];
    [purchasingNowLabel release];
    [upgradeNowLabel release];
    [super dealloc];
}


@end
