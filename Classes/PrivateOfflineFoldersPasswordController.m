//
//  PrivateOfflineFoldersPasswordController.m
//  ImageBankFree
//
//  Created by Yoshi Sugawara on 5/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "PrivateOfflineFoldersPasswordController.h"
#import "PrivateOfflineFoldersPasswordConfirmController.h"

@interface PrivateOfflineFoldersPasswordController ()

@end

@implementation PrivateOfflineFoldersPasswordController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    UIBarButtonItem *cancelButton = [[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelAction:)] autorelease];

    self.navigationItem.rightBarButtonItem = cancelButton;
    self.title = NSLocalizedString(@"Private Folders",@"");
    
    [self.view setBackgroundColor:[UIColor colorWithRed:211/255.0f green:211/255.0f blue:211/255.0f alpha:1.0f]];

    if ( scrollView ) {
        scrollView.contentSize = CGSizeMake(self.view.bounds.size.width + 200, (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) ? self.view.bounds.size.height + 200.0 : self.view.bounds.size.height);
    }
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [passwordField becomeFirstResponder];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    if ( scrollView && !(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)) { 
        scrollView.contentSize = CGSizeMake(self.view.bounds.size.height,self.view.bounds.size.width);
    }
}

#pragma mark
#pragma mark UITextFieldDelegate 
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self confirmPassword:textField];
    return NO;
}

-(IBAction)confirmPassword:(id)sender {
    NSString *pw = passwordField.text;
    if ( [pw length] == 0 ) {
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Missing Required Information", @"")
                                                            message:NSLocalizedString(@"Please enter a password",@"")
                                                           delegate:self
                                                  cancelButtonTitle:@"OK" 
                                                  otherButtonTitles:nil];
        [alertView show];
        [alertView release];
        [passwordField becomeFirstResponder];
        return;
    }
    [passwordField resignFirstResponder];
    PrivateOfflineFoldersPasswordConfirmController *nextController = [[[PrivateOfflineFoldersPasswordConfirmController alloc] init] autorelease];
    [nextController setPassword:pw];
    [self.navigationController pushViewController:nextController animated:YES];
}

-(void)cancelAction:(id)sender {
    [self dismissModalViewControllerAnimated:YES];
}

@end
