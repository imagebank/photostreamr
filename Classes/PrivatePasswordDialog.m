//
//  PrivatePasswordDialog.m
//  ImageBankFree
//
//  Created by yoshi on 2/23/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "PrivatePasswordDialog.h"
#import "PrivateFolderPasswordChecker.h"
#import "Utilities.h"
#import "Constants.h"
#import "FolderContentsController.h"

@implementation PrivatePasswordDialog

@synthesize delegate, isResuming, isForOfflineFolders;

-(id)initWithVC:(id)vc {
	self = [self init];
	[self setDelegate:vc];
	CGRect frame = [[self.delegate getView] frame];
	blackWall = [[UIView alloc] initWithFrame:frame];
	blackWall.backgroundColor = [UIColor blackColor];
	blackWall.opaque = YES;	
	return self;
}	

-(void) showPrivatePasswordDialog {	
	UIWindow *window = [[UIApplication sharedApplication] keyWindow];
	
	// add a black opaque view on top of the whole window
    [blackWall setFrame:[[[UIApplication sharedApplication] keyWindow] frame]];
	[window addSubview:blackWall];
	
    NSString *title = NSLocalizedString(@"Password Required",@"");
    
	passwordAlert = [[UIAlertView alloc] initWithTitle:title
                                               message:@""
                                              delegate:self 
                                     cancelButtonTitle:NSLocalizedString(@"Cancel",nil) 
                                     otherButtonTitles:NSLocalizedString(@"OK",nil), nil];
    passwordAlert.alertViewStyle = UIAlertViewStyleSecureTextInput;
    passwordAlert.tag = 1120;
	[passwordAlert show];
    isShown = YES;
}

-(void) dismissDialog {
	if ( passwordAlert ) {
		[passwordAlert dismissWithClickedButtonIndex:3 animated:NO];
	}
    if ( blackWall ) {
        [blackWall removeFromSuperview];
    }
    isShown = NO;
}

-(BOOL) isAlertShown {
    return passwordAlert && isShown;
}

#pragma mark
#pragma mark UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    isShown = NO;
	if ( buttonIndex == 3 ) {
		// just dismising an existing alert view
		return;
	}
		
	if ( buttonIndex == 1 ) {
		// Hit OK button
		UITextField *textField = [passwordAlert textFieldAtIndex:0];
        
        if ( (!isForOfflineFolders && [[PrivateFolderPasswordChecker sharedInstance] canAccessWithHashedPassword:[Utilities hashedSecret:textField.text]]) ||
            (isForOfflineFolders && [[PrivateFolderPasswordChecker sharedInstance] canAccessOfflineWithPassword:textField.text])
            )  {
			[blackWall removeFromSuperview];
			[self.delegate doAfterPrivateAccessAllowed];
            //			[alertView dismissWithClickedButtonIndex:1 animated:YES];
			return;            
        }
	}
	if ( [self.delegate respondsToSelector:@selector(doAfterPrivateAccessDenied)] )  {
		[self.delegate doAfterPrivateAccessDenied];
	}

	if ( isResuming && [self.delegate respondsToSelector:@selector(doAfterPrivateAccessDeniedOnResume)] ) {
		[self.delegate doAfterPrivateAccessDeniedOnResume];
	}		
	[blackWall removeFromSuperview];
//	[alertView dismissWithClickedButtonIndex:0 animated:YES];
}

#pragma mark
#pragma mark UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
	[textField resignFirstResponder];
	UIAlertView *alertView = (UIAlertView *) [[delegate getView] viewWithTag:1120];
	[alertView dismissWithClickedButtonIndex:1 animated:YES];
	return NO;
}

- (BOOL)alertViewShouldEnableFirstOtherButton:(UIAlertView *)thisAlertView
{
    UITextField *textField = [thisAlertView textFieldAtIndex:0];
    if ([textField.text length] == 0)
    {
        return NO;
    }
    return YES;
}

-(void)dealloc {
	delegate = nil;
	[blackWall release];
	if ( passwordAlert ) {
		[passwordAlert release];
	}
	[super dealloc];
}
@end
