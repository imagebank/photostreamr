//
//  FileListResponseProcessor.h
//  ImageBank
//
//  Created by yoshi on 5/13/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "Three20/Three20.h"


@interface FileListResponseProcessor : NSObject<TTURLResponse> {
    NSMutableArray *dirs;
	NSMutableArray *files;
    NSInteger totalNumberOfPhotos;
}

@property (nonatomic,retain) NSMutableArray *files; // read-only for clients, read-write for this class
@property (nonatomic,retain) NSMutableArray *dirs;
@property (nonatomic,readwrite) NSInteger totalNumberOfPhotos;

@end
