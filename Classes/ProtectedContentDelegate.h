//
//  ProtectedContentDelegate.h
//  ImageBankFree
//
//  Created by yoshi on 2/25/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol ProtectedContentDelegate

-(void) doAfterPrivateAccessAllowed;
-(UIView*) getView;

@optional
-(void) doAfterPrivateAccessDenied;
-(void) doAfterPrivateAccessDeniedOnResume;

@end
