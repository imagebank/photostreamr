//
//  FilesListModel.h
//  ImageBank
//
//  Created by yoshi on 5/13/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "Three20/Three20.h"
#import "FileListResponseProcessor.h"
#import "File.h"


@interface FileListModel : TTURLRequestModel<NSCopying> {
	DirectoryFile *directory;
	NSString *urlForExternalDirectoryContents;
	FileListResponseProcessor *responseProcessor;
	BOOL forceLoadingFromNetwork;
    NSString *sortByField;
    BOOL sortAscending;
}
-(id)initWithDirectory:(DirectoryFile*)dir;

-(NSArray*) files;
-(NSArray*) dirs;
-(NSInteger)imageCount;
-(void)loadSynchronously:(TTURLRequestCachePolicy)cachePolicy;

@property (nonatomic,readwrite) BOOL forceLoadingFromNetwork;
@property (nonatomic,readwrite,retain) DirectoryFile *directory;
@property (nonatomic,retain) NSString *sortByField;
@property (nonatomic,assign) BOOL sortAscending;

@end
