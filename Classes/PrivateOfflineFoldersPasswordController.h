//
//  PrivateOfflineFoldersPasswordController.h
//  ImageBankFree
//
//  Created by Yoshi Sugawara on 5/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PrivateOfflineFoldersPasswordController : UIViewController<UITextFieldDelegate> {
    IBOutlet UITextField *passwordField;
    IBOutlet UIButton *nextButton;
    IBOutlet UIScrollView *scrollView;
    
}

-(IBAction)confirmPassword:(id)sender;

@end
