//
//  FolderContentsController.m
//  ImageBank
//
//  Created by yoshi on 6/13/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "FolderContentsController.h"
#import "File.h"
#import "PhotoSource.h"
#import "FileListModel.h"
#import "FileListDataSource.h"
#import "PhotoController.h"
#import "Constants.h"
#import "PrivatePasswordDialog.h"
#import "PrivateFolderPasswordChecker.h"
#import "Utilities.h"
#import "FileUploadNotifier.h"
#import "FolderContentsDataSource.h"
#import "FolderContentsModel.h"
#import "HelpPopOverController.h"
#import "WEPopoverContentViewController.h"
#import "IASKAppSettingsViewController.h"
#import "NSOperationQueue+SharedQueue.h"
#import "CreateOfflineFilesOperation.h"
#import "OfflineFolderSaveController.h"
#import "UIDeviceHardware.h"

#ifdef FREE_UPGRADABLE_VERSION
#import "BuyUpgradeController.h"
#import "MKStoreManager.h"
#endif

@implementation FolderContentsController

@synthesize fileListModel,currentDirectory, parentDirectory, displayMode, isAtRootDirectory, managedObjContext, privateFoldersAccessButton, offlineButton, downloadSelectedButton, sortByField, folderSearchBar, searchResultsController, folderSearchDisplayController, indexPathAtBeginDragging;

-(id)initWithNavigatorURL:(NSURL *)URL query:(NSDictionary *)query {
	if ( self = [super init] ) {
		doReload = NO;
		NSString *url;
		NSString *server = [query objectForKey:@"server_ip"];
		DirectoryFile *file = [query objectForKey:@"file"];
		if ( server ) {
			// Top level directory
			url =  [NSString stringWithFormat:@"http://%@/files/getListAtRootDirectory?a=b",server];
			file = [[[DirectoryFile alloc] initWithName:@"" url:url imageCount:0 dirCount:0] autorelease];
			[self setIsAtRootDirectory:YES];
		} else {
			[self setIsAtRootDirectory:NO];
		}
		
		[self setCurrentDirectory:file];
        
        DirectoryFile *parentDir = [query objectForKey:@"parent_directory"];
        if ( parentDir ) {
            [self setParentDirectory:parentDir];
        }
        TTDINFO(@"parent directory = %@",[self.parentDirectory name]);
        
		NSManagedObjectContext *context = [query objectForKey:@"managedObjectContext"];
		[self setManagedObjContext:context];
		
		if ( [self.currentDirectory isKindOfClass:[RandomDirectoryFile class]] ) {
			titleForView = [NSLocalizedString(@"Shuffle",@"Title for random images") copy];
		} else if ( [self.currentDirectory isKindOfClass:[SearchResultsDirectoryFile class]] ) {
            titleForView = [NSLocalizedString(@"Search Results",@"") copy];
        } else {
			titleForView = [self.currentDirectory.name copy];
		}
		[self setTitle:@""];
        self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"titlebar"]];
        self.navigationBarTintColor = [Utilities globalAppTintColor];
        
        NSString *isOfflineFolderParam = (NSString *) [query objectForKey:@"isOfflineFolder"];
        if ( isOfflineFolderParam ) {
            isOfflineFolder = YES;
        }
	}

	BOOL allowPrivateFolderAccess = [[PrivateFolderPasswordChecker sharedInstance] allowAccess];
	self.privateFoldersAccessButton = [[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:allowPrivateFolderAccess ? @"icon_unlock.png" : @"icon_lock.png"]
																		style:UIBarButtonItemStyleBordered
																	   target:self
																	   action:@selector(showPrivatePasswordAccess:)] autorelease];
	privateFoldersAccessButton.tag = kPrivateFoldersAccessToolbarButton;
	
    self.offlineButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon_folder_down.png"] style:UIBarButtonItemStyleBordered target:self action:@selector(startSelectingPhotos:)];
    self.downloadSelectedButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Download Selected",@"") style:UIBarButtonItemStyleBordered target:self action:@selector(saveOfflineImages:)];
    [downloadSelectedButton setTag:kDownloadSelectedButtonTag];
    [downloadSelectedButton setEnabled:NO];

    NSDictionary *sortOptions = [Utilities folderSortOptions];
    NSString *sortByFieldName = [sortOptions objectForKey:@"sort_by_field"];
    NSString *isAscValue = [sortOptions objectForKey:@"is_asc"];
    [self setSortByField:sortByFieldName];
    sortAscending = [isAscValue isEqualToString:@"yes"];
    
	return self;
}

#pragma mark
#pragma mark TTModelViewController

-(void)createModel {
	TTDINFO(@"Creating model...");
    FolderContentsModel *multiModel = [[FolderContentsModel alloc] initWithTitle:self.title directory:self.currentDirectory isOffline:isOfflineFolder];
    [multiModel setIsRootDirectory:self.isAtRootDirectory];
    FolderContentsDataSource *ds = [[FolderContentsDataSource alloc] init];
    [ds setCurrentDirectory:self.currentDirectory];
    [ds setDelegate:self];
	ds.model = multiModel;
    [[multiModel delegates] addObject:ds];
    [[multiModel fileListModel] setForceLoadingFromNetwork:doReload];
    [[multiModel fileListModel] setSortByField:self.sortByField];
    [[multiModel fileListModel] setSortAscending:sortAscending];
    [[multiModel photosModel] setSortByField:self.sortByField];
    [[multiModel photosModel] setSortAscending:sortAscending];
    doReload = NO;
    // this causes the model to load - TTTableViewController.setDataSource -> TTModelViewController.setModel
	self.dataSource = ds;
}

- (void)modelDidStartLoad:(id <TTModel>)model {
    if ( !isSelectingPhotosForDownload ) {
        [self setToolbarItems:[self toolbarItems:NO]];
    }
    if ( [model isLoading] ) {
        TTDINFO(@"Model is loading!");
    } else {
        TTDINFO(@"Model is not loading :(");
    }
	[self updateView];
}

/*
 * The central model for this controller is the FileListModel
 After it is finished loading, the PhotoSource is set if we're going to display the thumbnail view
 */
- (void)modelDidFinishLoad:(id<TTModel>)model {
	TTDINFO(@"model did finish load called! model: %@",[model class]);
    [super modelDidFinishLoad:model];
//    if ( [model isKindOfClass:[PhotoSource class]] ) {
//        [ds setPhotoSource:[self photoSource]];
//    }
    

//	if ( [model isKindOfClass:[FileListModel class]] ) {
//		int imgCount = 0;
//		int dirCount = 0;
//		for (File* file in [self.fileListModel files]) {
//			if ( [file isKindOfClass:[ImageFile class]] ) {
//				imgCount++;
//			} else if ([file isKindOfClass:[DirectoryFile class]] && ![file isKindOfClass:[RandomDirectoryFile class]]) {
//				dirCount++;
//			}
//		}
//		if ( imgCount > 0 && dirCount == 0 ) {
//			[self setDisplayMode:DisplayModeImages];
//		} else {
//            [self updateTableDelegateWithVarHeight];
//        }
//		
//		[self setupTableView];
//		if ( displayMode == DisplayModeImages ) {
//			[self setPhotoSourceAccordingToDirectoryWithReloadSetting:doReload];
//		}
//	}
    [self updateTableDelegateWithVarHeight];
    [self setupTableView];
	[self.tableView reloadData];
    if ( !isSelectingPhotosForDownload ) {
        [self setToolbarItems:[self toolbarItems:NO]];
    }
	doReload = NO;
    
    if ( [model isKindOfClass:[FolderContentsModel class]] ) {
        // preload next thumbnails upon load completion of the final model
        [self didEndDragging];
    }
}



-(void)setPhotoSourceAccordingToDirectoryWithReloadSetting:(BOOL)forceReload {
	if ( [self.currentDirectory isKindOfClass:[RandomDirectoryFile class]] ) {
		// for random file directory just pass the existing file list data source
		[self setPhotoSource:[[[PhotoSource alloc] initWithTitle:self.title
													   directory:self.currentDirectory
														  photos:[self.fileListModel files]] autorelease]];
	} else {
		[self setPhotoSource:[[[PhotoSource alloc] initWithTitle:self.title
													   directory:self.currentDirectory
											 totalNumberOfPhotos:[self.fileListModel imageCount]
										  forceReloadFromNetwork:forceReload] autorelease]];
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)setPhotoSource:(id<TTPhotoSource>)photoSource {
    if (photoSource != _photoSource) {
        [_photoSource release];
        _photoSource = [photoSource retain];        
    }
}

- (void)updateTableDelegateWithVarHeight {
        [_tableDelegate release];
        _tableDelegate = [[[[TTTableViewVarHeightDelegate alloc] initWithController:self] autorelease] retain];
        
        // You need to set it to nil before changing it or it won't have any effect
        _tableView.delegate = nil;
        _tableView.delegate = _tableDelegate;
}

- (void)updateTableDelegateWithFixedHeight {
    [_tableDelegate release];
    _tableDelegate = [[[[TTTableViewDelegate alloc] initWithController:self] autorelease] retain];
    
    // You need to set it to nil before changing it or it won't have any effect
    _tableView.delegate = nil;
    _tableView.delegate = _tableDelegate;
}

-(BOOL)isSavingForOfflineExecuting {
    BOOL retval = NO;
    for (NSOperation *op in [[NSOperationQueue sharedOperationQueue] operations]) {
        TTDINFO(@"operation: %@ , isCancelled = %@, is finished = %@, is executing = %@",[[op class] description],[op isCancelled] ? @"YES" : @"NO",[op isFinished] ? @"YES" : @"NO",[op isExecuting] ? @"YES" : @"NO");

        if ( [op isExecuting] ) {
            return YES;
        }
    }
    return retval;
}

-(void) adjustContentInsets {
    [self.tableView setContentInset:UIEdgeInsetsMake(0.0f, 0.0f, 0.0f, 0.0f)];
}

- (void)updateTableLayout {
    self.tableView.scrollIndicatorInsets = UIEdgeInsetsMake(TTBarsHeight(), 0, 0, 0);
}


-(void) preFetchNextImagesFromIndex:(NSInteger)index numberOfImages:(NSInteger)count {
    PhotoSource *ps = (PhotoSource*) [(FolderContentsDataSource*) self.dataSource photoSource];
    for (int i = 1; i<=count; i++) {
        NSInteger nextIndex = index + i;
        if ( nextIndex >= [ps numberOfPhotos] ) {
            // we're at the end, stop
            return;
        }
        [self preFetchImageAtIndex:nextIndex];
    }
}

-(void) preFetchImageAtIndex:(NSInteger)index {
    PhotoSource *ps = (PhotoSource*) [(FolderContentsDataSource*) self.dataSource photoSource];
    id<TTPhoto> nextPhoto = [ps photoAtIndex:index];
    TTDINFO(@"prefetching thumbnail at index: %d file: %@",index,[nextPhoto URLForVersion:TTPhotoVersionThumbnail]);
    TTURLRequest *request = [TTURLRequest requestWithURL:[nextPhoto URLForVersion:TTPhotoVersionThumbnail] delegate:self];
    request.response = [[[TTURLImageResponse alloc] init] autorelease];
    [request send];
}

#pragma mark
#pragma mark TTTableViewController
-(void)didBeginDragging {
    [self setIndexPathAtBeginDragging:[[self.tableView indexPathsForVisibleRows] objectAtIndex:0]];
    TTDINFO(@"Row at begin dragging: %i",indexPathAtBeginDragging.row);
}

-(void)didEndDragging {
    if ( [UIDeviceHardware isSlowerDevice] ) {
        return;
    }
    NSArray *visibleRows = [self.tableView indexPathsForVisibleRows];
    if ( visibleRows == nil || visibleRows.count == 0 ) {
        return;
    }
    NSIndexPath *currentIndexPath = [visibleRows objectAtIndex:0];
    TTDINFO(@"End dragging: current row: %i , row at begin drag: %i",currentIndexPath.row,indexPathAtBeginDragging.row);
    if ( indexPathAtBeginDragging == nil || currentIndexPath.row > indexPathAtBeginDragging.row) {
        // user scrolled downwards
        TTDINFO(@"Downward scroll direction detected! Preloading thumbnails....");
        // preload images that are not visible, only at the last visible row
        NSIndexPath *lastIndexPath = [visibleRows lastObject];
        // prefetch the next rows
        FolderContentsDataSource *dataSource = (FolderContentsDataSource*) self.dataSource;
        NSInteger columnCount = [dataSource columnCountForView:self.tableView];
        NSInteger prefetchStartIndex = ((lastIndexPath.row + 1) * columnCount) + 1;
        NSInteger numPhotosToPrefetch = columnCount * 5;
        TTDINFO("Prefetching thumbs from index %i for %i photos",prefetchStartIndex,numPhotosToPrefetch);
        [self preFetchNextImagesFromIndex:prefetchStartIndex numberOfImages:numPhotosToPrefetch];
    }
}

#ifdef FREE_UPGRADABLE_VERSION
-(void)didSelectObject:(id)object atIndexPath:(NSIndexPath *)indexPath {
    TTTableTextItem *item = (TTTableTextItem*) object;
    BOOL isRandomDirectory = (item.userInfo != nil && [[item.userInfo objectForKey:@"file"] isKindOfClass:[RandomDirectoryFile class]] );
    if ( isRandomDirectory && ![MKStoreManager fullVersionUpgradePurchased] ) {
        [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Requires Full Version"
                                                                                  ,@"Requires Full Version")
                                                        message:NSLocalizedString(@"Upgrade for Shuffle",@"Upgrade for Shuffle")
                                                       delegate:self cancelButtonTitle:NSLocalizedString(@"Not Now", @"Not Now") otherButtonTitles:NSLocalizedString(@"Upgrade",@"Upgrade to full version button"),nil];
        [alert show];
        [alert release];
    }
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
	if ( buttonIndex == 0 ) {
		[alertView dismissWithClickedButtonIndex:0 animated:YES];
	} else {
		[alertView dismissWithClickedButtonIndex:0 animated:YES];
		[self showUpgradeView:alertView];
    }
}
#endif

#pragma mark
#pragma mark UIViewController

-(void)loadView {
	[super loadView];
//	self.navigationController.toolbar.tintColor = [UIColor whiteColor];
	[self.navigationController setToolbarHidden:NO];
    [self.revealSideViewController setWantsFullScreenLayout:NO];
}

-(void)viewDidLoad {
	[super viewDidLoad];
        
	privatePasswordDialog = [[PrivatePasswordDialog alloc] initWithVC:self];
    [privatePasswordDialog setIsForOfflineFolders:isOfflineFolder];
    [self.tableView setBackgroundColor:[UIColor colorWithRed:211/255.0f green:211/255.0f blue:211/255.0f alpha:1.0f]];
    
    UIBarButtonItem *refreshButton = [[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(refreshAction:)] autorelease];
    [refreshButton setStyle:UIBarButtonItemStyleBordered];

	self.navigationItem.rightBarButtonItem = refreshButton;
	self.navigationItem.rightBarButtonItem.enabled = isOfflineFolder ? NO : YES;
    
    if ( !isOfflineFolder && ![self.currentDirectory isKindOfClass:[SearchResultsDirectoryFile class]] && !self.isAtRootDirectory ) {
        
        searchResultsController = [[FolderContentsSearchResultsController alloc] init];
        [searchResultsController setParentController:self];
        
        folderSearchBar = [[UISearchBar alloc] init];
        folderSearchBar.placeholder = NSLocalizedString(@"Search folder",@"Search folder");
        [folderSearchBar sizeToFit];
        folderSearchBar.tintColor = [UIColor grayColor];
        [folderSearchBar setAutocapitalizationType:UITextAutocapitalizationTypeNone];

        folderSearchDisplayController = [[UISearchDisplayController alloc] initWithSearchBar:folderSearchBar contentsController:self];
        folderSearchDisplayController.searchResultsTableView.tableFooterView = [[UIView new] autorelease];
        [searchResultsController setSearchDisplayController:folderSearchDisplayController];
        folderSearchDisplayController.delegate = searchResultsController;
        folderSearchDisplayController.searchResultsDataSource = searchResultsController;
        folderSearchDisplayController.searchResultsDelegate = searchResultsController;
//        [folderSearchDisplayController setActive:YES animated:YES];
        
        
        self.tableView.tableHeaderView = folderSearchBar;
        
    }
    
    self.title = @"";
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
	[super viewDidUnload];
}

-(void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];

    self.revealSideViewController.panInteractionsWhenClosed = PPRevealSideInteractionNavigationBar | PPRevealSideInteractionContentView;
    
	[self.navigationController setToolbarHidden:NO];
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(appGoingIntoBackground:)
												 name:UIApplicationWillResignActiveNotification object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(appGoingIntoForeground:)
												 name:UIApplicationDidBecomeActiveNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didFinishSaveForOffline) 
                                                 name:@"SaveForOfflineDidFinish"
                                               object:nil];
    
	self.navigationController.navigationBar.hidden = NO;
    self.navigationController.toolbar.hidden = NO;
	
	// fix the private password button based on the current state
	self.privateFoldersAccessButton.image = [UIImage imageNamed:[[PrivateFolderPasswordChecker sharedInstance] allowAccess] ? @"icon_unlock.png" : @"icon_lock.png"];
    
    if ( [self isSavingForOfflineExecuting] ) {
        [self changeOfflineButtonToInProgress];
    }
    
    if ( isSelectingPhotosForDownload ) {
        [self setToolbarItems:[self toolbarItemsForSelectingPhotos]];
        [self updatePhotosSelectedStatus];
    } else {
        [self setToolbarItems:[self toolbarItems:NO]];
    }
    
    [self.revealSideViewController setDelegate:self];
    HelpPopOverController *sideViewController = (HelpPopOverController*) [self.revealSideViewController  controllerForSide:PPRevealSideDirectionLeft];
    [sideViewController setDelegate:self];
    
    
    if ( folderSearchBar ) {
        [folderSearchBar resignFirstResponder];
    }    

}

-(void)viewWillDisappear:(BOOL)animated {
	[super viewWillDisappear:animated];
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
//    TTDINFO(@"View debug: %@",[self.tableView recursiveDescription]);
//    for (UIView* v in self.tableView.subviews) {
//        CGFloat red = (arc4random()%256)/256.0;
//        CGFloat green = (arc4random()%256)/256.0;
//        CGFloat blue = (arc4random()%256)/256.0;
//        v.layer.borderColor = [UIColor colorWithRed:red green:green blue:blue alpha:1.0].CGColor;
//        v.layer.borderWidth = 3.0;
//    }
    TTDINFO(@"Checking to scroll table...");
    if ( lastCenterPhotoIndex > 0 ) {
        TTDINFO(@"Moving table to photo position %ld...",(long)lastCenterPhotoIndex);
        // figure out which row to go to based on number of folders and photo columns
        FolderContentsDataSource *ds = (FolderContentsDataSource*) self.dataSource;
        if ( ds != nil ) {
            NSInteger thumbnailColumnCount = [ds columnCountForView:nil];
            NSInteger row = ceilf(lastCenterPhotoIndex / thumbnailColumnCount);
            NSInteger section = [ds numberOfSectionsInTableView:self.tableView] == 2 ? 1 : 0;
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:section];
            TTDINFO(@"Row to move to: %@",indexPath);
            [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
        }
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderForKey:(NSString *)key {
    return 0.0f;
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{

}

-(NSUInteger)supportedInterfaceOrientations {
	if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
		return UIInterfaceOrientationMaskAll;
	} else {
		return UIInterfaceOrientationMaskAllButUpsideDown;
	}
}

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
	if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        return YES;
    }
    return toInterfaceOrientation != UIInterfaceOrientationPortraitUpsideDown;
}


-(BOOL) shouldAutorotate {
    return YES;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}

#pragma
#pragma mark UI Actions

-(void)refreshAction:(id)sender {
	doReload = YES;
    [self refresh];
	[self invalidateModel];
    [self invalidateView];
}

-(void)maybeRefreshDataSourceIfPrivateAccessHasChanged {
	if ( displayMode == DisplayModeFiles ) {
		FolderContentsDataSource *ds = (FolderContentsDataSource*)self.dataSource;
		
		if ( [[ds folders] count] == 0 ) {
			[self refreshAction:nil];
			return;
		}
		
		[ds refreshFolders];
		[self.tableView reloadData];
	}
}

#ifdef FREE_UPGRADABLE_VERSION
-(void)showUpgradeView:(id)sender {
	if ([SKPaymentQueue canMakePayments]) {
		BuyUpgradeController *upgradeController = [[BuyUpgradeController alloc] init];
		[upgradeController setManagedObjContext:self.managedObjContext];
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
            upgradeController.modalPresentationStyle = UIModalPresentationFormSheet;
		[self.navigationController presentModalViewController:upgradeController animated:YES];
//		[upgradeController release];
	} else {
		TTDINFO(@"Cannot make payments!");
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Cannot purchase upgrade",
																				  @"Alert backg when tapping Upgrade button but not able to make payments")
														message:NSLocalizedString(@"Sorry, you are not authorized to purchase from the App Store",
																				  @"Sorry, you are not authorized to purchase from the App Store")
													   delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];
		[alert release];
	}
}
#endif


-(void) popAllViewControllers {
    [self.navigationController popToRootViewControllerAnimated:YES];
}	

-(void)showPrivatePasswordAccess:(id)sender {
	if ( [[PrivateFolderPasswordChecker sharedInstance] allowAccess] ) {
		// private folders are currently viewable, so user wants to lock access
		[[PrivateFolderPasswordChecker sharedInstance] setAllowAccess:NO];
		
		CGRect lockFlashRect = CGRectMake(self.view.center.x - 100.0f, self.view.center.y - 100.0f, 200.0f, 200.0f);
		lockFlashView = [[UIImageView alloc] initWithFrame:lockFlashRect];
		lockFlashView.image = [UIImage imageNamed:@"img-lock.png"];
		lockFlashView.alpha = 0.75;
		[self.navigationController.view addSubview:lockFlashView];
		[UIView beginAnimations:nil context:nil];
		[UIView setAnimationDuration:1.2];
		[UIView setAnimationTransition:UIViewAnimationTransitionNone forView:self.navigationController.view cache:YES];
		[UIView setAnimationDelegate:self];
		[UIView setAnimationDidStopSelector:@selector(lockFlashAnimationDone)];
		lockFlashView.alpha = 0.0;
		[UIView commitAnimations];
		
	} else {
		[privatePasswordDialog showPrivatePasswordDialog];
	}
}

-(void)lockFlashAnimationDone {
	[lockFlashView removeFromSuperview];
	if ( [self.currentDirectory isPrivate] ) {
		[self popAllViewControllers];
		return;
	}
	self.privateFoldersAccessButton.image = [UIImage imageNamed:@"icon_lock.png"];
	[self maybeRefreshDataSourceIfPrivateAccessHasChanged];
}	

-(void)appGoingIntoBackground:(id)sender {
    [[PrivateFolderPasswordChecker sharedInstance] setDateWentIntoBackground:[NSDate date]];
	// hide the main view
	if ( self.currentDirectory.isPrivate ) {
		self.navigationController.view.alpha = 0.0;
	}
    if ( self.isAtRootDirectory ) {
        [self popAllViewControllers];
    }
//	[privatePasswordDialog dismissDialog];
}

-(void)appGoingIntoForeground:(id)sender {
    if ( [[PrivateFolderPasswordChecker sharedInstance] shouldCheckAccess] || [privatePasswordDialog isAlertShown]) {
        [privatePasswordDialog dismissDialog];
        if ( isOfflineFolder ) {
            [[PrivateFolderPasswordChecker sharedInstance] setAllowOfflineAccess:NO];
        } else {
            [[PrivateFolderPasswordChecker sharedInstance] setAllowAccess:NO];
        }

        if (self.currentDirectory.isPrivate) {
            [privatePasswordDialog setIsResuming:YES];
            [privatePasswordDialog showPrivatePasswordDialog];
        } else {
            self.navigationController.navigationBar.hidden = NO;
            self.navigationController.view.alpha = 1.0;
        }
        
    } else {
        [privatePasswordDialog dismissDialog];
        self.navigationController.navigationBar.hidden = NO;
        self.navigationController.view.alpha = 1.0;
    }
}

-(void) uploadImageUsingPhotoLibrary {
	picker = [[UIImagePickerController alloc] init];
	picker.delegate = self;
	picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
	[self presentModalViewController:picker animated:YES];
}

-(void) uploadImageUsingCamera {
	picker = [[UIImagePickerController alloc] init];
	picker.delegate = self;
	picker.sourceType = UIImagePickerControllerSourceTypeCamera;
	[self presentModalViewController:picker animated:YES];
}

-(void) uploadImageUsingClipboard {
	UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
	if ( pasteboard.image != nil ) {
		NSData *imgdata = UIImageJPEGRepresentation(pasteboard.image,1.0);
		if ( imgdata == nil || [imgdata length] == 0 ) {
			TTAlert(NSLocalizedString(@"Sorry, only jpeg images can be uploaded from the clipboard",@"Sorry, only jpeg images can be uploaded from the clipboard"));
			return;
		}
		[[FileUploadNotifier sharedInstance] addUploadOperation:imgdata 
												   forDirectory:self.currentDirectory 
												 withController:self
													orientation:self.interfaceOrientation];
	}
}

-(void)showUploadImageActionSheet:(id)sender {
	UIActionSheet *actionSheet = [[UIActionSheet alloc] init];
	actionSheetButtons = [[NSMutableArray alloc] initWithObjects:@"photolib",@"cancel",@"",@"",nil];
	[actionSheet setTitle:NSLocalizedString(@"Upload image to this folder using:",@"Upload image to this folder using:")];
	[actionSheet setDelegate:self];
	[actionSheetButtons replaceObjectAtIndex:[actionSheet addButtonWithTitle:NSLocalizedString(@"Photo Library",@"Photo Library")]
								  withObject:@"photolib"];
	UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
	if ( pasteboard.image != nil ) {
		[actionSheetButtons replaceObjectAtIndex:[actionSheet addButtonWithTitle:NSLocalizedString(@"Clipboard",@"Clipboard")]
									   withObject:@"clipboard"];
	}	
	if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
		[actionSheetButtons replaceObjectAtIndex:[actionSheet addButtonWithTitle:NSLocalizedString(@"Camera",@"Camera")]
									  withObject:@"camera"];
	}
	[actionSheet setCancelButtonIndex:[actionSheet addButtonWithTitle:NSLocalizedString(@"Cancel",@"Cancel")]];
	[actionSheet showFromToolbar:self.navigationController.toolbar];
	[actionSheet release];
}

-(void)saveOfflineImages:(id)sender {
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Select where to save your photos to.", @"") 
                                                             delegate:self 
                                                    cancelButtonTitle:NSLocalizedString(@"Cancel",@"Cancel")
                                               destructiveButtonTitle:nil 
                                                    otherButtonTitles:NSLocalizedString(@"Saved Folders",@""),
                                                        NSLocalizedString(@"Camera Roll",@""),nil];
    [actionSheet setTag:[(UIBarButtonItem*)sender tag]];
    [actionSheet showFromToolbar:self.navigationController.toolbar];
    [actionSheet release];
}

-(void)startSelectingPhotos:(id)sender {
    if ( [self isSavingForOfflineExecuting] ) {
        [self cancelCurrentDownloads];
    } else {
        isSelectingPhotosForDownload = YES;
        [self setToolbarItems:[self toolbarItemsForSelectingPhotos] animated:YES];
        self.navigationItem.hidesBackButton = YES;
        self.title = NSLocalizedString(@"Select Photos", @"");
    }
}

-(void)doneSelectingPhotos:(id)sender {
    isSelectingPhotosForDownload = NO;
    [self setToolbarItems:[self toolbarItems:NO] animated:YES];
    self.navigationItem.hidesBackButton = NO;
    self.title = titleForView;
    PhotoSource *ps = (PhotoSource*) [(FolderContentsDataSource*) self.dataSource photoSource];
    for (NSInteger i = 0; i <= [ps maxPhotoIndex]; i++) {
        id<TTPhoto> thePhoto = [ps photoAtIndex:i];
        [thePhoto setIsSelected:NO];
    }
    [self.downloadSelectedButton setEnabled:NO];
    
    if ( [sender isKindOfClass:[OfflineFolderSaveController class]] ) {
        [self.tableView reloadData];
    } else {
        [self.tableView reloadRowsAtIndexPaths:[self.tableView indexPathsForVisibleRows] withRowAnimation:UITableViewRowAnimationNone];
    }
}

-(NSString*)getDirName {
    return (self.parentDirectory != nil && [self.currentDirectory isKindOfClass:[RandomDirectoryFile class]] ) ?
    [self.parentDirectory name] : [self.currentDirectory name];
}

#pragma mark
#pragma mark UIActionSheet
- (void)actionSheet:(UIActionSheet *)modalView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    switch (buttonIndex) {
        case 0: {
            OfflineFolderSaveController *saveController = [[OfflineFolderSaveController alloc] initWithStyle:UITableViewStylePlain];
            [saveController setParentVC:self];
            [saveController setFolderContentsModel:(FolderContentsModel*) [self.dataSource model]];
            [saveController setParentDirectory:self.parentDirectory];

            if ( modalView.tag == kDownloadAllButtonTag ) {   
                [saveController setIsDownloadingAll:YES];
            }
            
           	UINavigationController *aNavController = [[UINavigationController alloc] initWithRootViewController:saveController];
            aNavController.toolbarHidden = NO;
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                aNavController.modalPresentationStyle = UIModalPresentationFormSheet;
            [self presentModalViewController:aNavController animated:YES];
            [saveController release];
            [aNavController release];

            break;
        }
		case 1: {
            CreateOfflineFilesOperation *operation = nil;
            FileListModel *model = [(FolderContentsModel*) [self.dataSource model] fileListModel];

            if ( modalView.tag == kDownloadAllButtonTag ) {   
                operation = [[CreateOfflineFilesOperation alloc] initWithFileListModel:model withOrientation:self.interfaceOrientation withDirName:[self getDirName]];
            } else {
                // use photosource
                PhotoSource *currentPhotoSource = [(FolderContentsModel*) [self.dataSource model] photosModel];
                operation = [[CreateOfflineFilesOperation alloc] 
                             initWithFileListModel:model withPhotoSource:currentPhotoSource withOrientation:self.interfaceOrientation
                             withDirName:[self getDirName]];
            }
            
            [operation setSaveToCameraRoll:YES];
            [[NSOperationQueue sharedOperationQueue] addOperation:operation];
            [self changeOfflineButtonToInProgress];
            [self doneSelectingPhotos:self];
 			break;
        }
		default: {
			break;
        }
	}
    
    

//	NSString *action = [actionSheetButtons objectAtIndex:buttonIndex];
//	if ( [action isEqualToString:@"photolib"] ) {
//		[self uploadImageUsingPhotoLibrary];
//	} else if ( [action isEqualToString:@"clipboard"] ) {
//		[self uploadImageUsingClipboard];
//	} else if ( [action isEqualToString:@"camera"] ) {
//		[self uploadImageUsingCamera];
//	}
//	actionSheetButtons = nil;
}

	
#pragma mark
#pragma mark ImagePicker methods
- (void)imagePickerControllerDidCancel:(UIImagePickerController *) Picker {
	[[Picker parentViewController] dismissModalViewControllerAnimated:YES];
	if ( Picker.sourceType == UIImagePickerControllerSourceTypeCamera ) {
		// reload since the camera uses lots o memory
		[self refreshAction:nil];
	}
}
- (void)imagePickerController:(UIImagePickerController *) Picker
didFinishPickingMediaWithInfo:(NSDictionary *)info {
	UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
	TTDINFO(@"image picked!");
	[[FileUploadNotifier sharedInstance] addUploadOperation:UIImageJPEGRepresentation(image,1.0) 
											   forDirectory:self.currentDirectory
											 withController:self
												orientation:self.interfaceOrientation];
	[[Picker parentViewController] dismissModalViewControllerAnimated:YES];	
}

#pragma mark
#pragma mark UI Helpers

-(void) changeOfflineButtonToInProgress {
    self.offlineButton.image = [UIImage imageNamed:@"icon_cancel.png"];
}

-(void) didFinishSaveForOffline {
    [self setToolbarItems:[self toolbarItems:NO]];
}

-(void) cancelCurrentDownloads {
    [[NSOperationQueue sharedOperationQueue] cancelAllOperations];
    [self didFinishSaveForOffline];
}

-(BOOL) canShowSaveOfflineButton {
    return ((!isAtRootDirectory && !isOfflineFolder && [[(FolderContentsDataSource*) self.dataSource photoSource] numberOfPhotos] > 0) || [self isSavingForOfflineExecuting]) 
            && [self.model isLoaded];
}

-(void)revealSideMenu:(id)sender {
    [self.revealSideViewController pushOldViewControllerOnDirection:PPRevealSideDirectionLeft withOffset:[UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad ? 470.0 : 70.0 animated:YES];
}

-(NSArray*) toolbarItems:(BOOL)hidden {	
    NSMutableArray *items = [[[NSMutableArray alloc] init] autorelease];
    
	UIBarItem *space = [[[UIBarButtonItem alloc] initWithBarButtonSystemItem:
						 UIBarButtonSystemItemFlexibleSpace target:nil action:nil] autorelease];
	

    if ( [self isSavingForOfflineExecuting] ) {
        [self changeOfflineButtonToInProgress];
    } else {
        self.offlineButton.image = [UIImage imageNamed:@"icon_folder_down.png"];
    }

    UIBarButtonItem *sideMenuButton =
        [[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ButtonMenu.png"]
                                      style:UIBarButtonItemStyleBordered
                                     target:self
                                     action:@selector(revealSideMenu:)] autorelease];
    [items addObject:sideMenuButton];
    
    if ( [[PrivateFolderPasswordChecker sharedInstance] showButton] ) {
        [items addObject:space];
        [items addObject:privateFoldersAccessButton];
        [items addObject:space];
    } else {
        [items addObject:space];
    }

    BOOL showOfflineSave = [self canShowSaveOfflineButton];
#ifdef FREE_UPGRADABLE_VERSION
    if ( ![MKStoreManager fullVersionUpgradePurchased] ) {
        showOfflineSave = NO;
    }
#endif
    
    
    if ( showOfflineSave )
        [items addObject:self.offlineButton];
    
#ifdef FREE_UPGRADABLE_VERSION
	if ( ![MKStoreManager fullVersionUpgradePurchased] ) {
		upgradeButton = [[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Upgrade",@"Upgrade to full version button")
													  style:UIBarButtonItemStyleBordered
													 target:self
	  											     action:@selector(showUpgradeView:)] autorelease];
        [items addObject:upgradeButton];
	}
#endif


    
	return items;
}


-(NSArray*)toolbarItemsForSelectingPhotos {
    NSMutableArray *items = [[[NSMutableArray alloc] init] autorelease];
    [items addObject:self.downloadSelectedButton];
    
    if ( ![self.currentDirectory isKindOfClass:[RandomDirectoryFile class]] ) {
        UIBarButtonItem *downloadAllButton = [[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Download All",@"") style:UIBarButtonItemStyleBordered target:self action:@selector(saveOfflineImages:)] autorelease];
        [downloadAllButton setTag:kDownloadAllButtonTag];
        [items addObject:downloadAllButton];
    }
    [items addObject:[[[UIBarButtonItem alloc] initWithBarButtonSystemItem:
                       UIBarButtonSystemItemFlexibleSpace target:nil action:nil] autorelease]];
    UIBarButtonItem *cancelButton = [[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Cancel",@"") style:UIBarButtonItemStyleBordered target:self action:@selector(doneSelectingPhotos:)] autorelease];
    [cancelButton setTintColor:[UIColor colorWithRed:0.7 green:0 blue:0 alpha:1]];
    [items addObject:cancelButton]; 
    return items;
}

-(void)setupTableView {
    self.tableView.rowHeight = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) ? 191 : 79;
    self.tableView.autoresizingMask =
    UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
//    self.tableView.backgroundColor = [UIColor clearColor];

//	if ( displayMode == DisplayModeFiles ) {
//		self.tableView.rowHeight = 45;
//		self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
//	} else {		
//		self.tableView.rowHeight = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) ? 191 : 79;
//		self.tableView.autoresizingMask =
//		UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
//		self.tableView.backgroundColor = TTSTYLEVAR(backgroundColor);
//		self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
//	}		
}

///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark TTThumbsTableViewCellDelegate


///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)thumbsTableViewCell:(TTThumbsTableViewCell*)cell didSelectPhoto:(id<TTPhoto>)photo {
    if ( isSelectingPhotosForDownload ) {
        return;
    }
    
	[_delegate thumbsViewController:self didSelectPhoto:photo];
	
	BOOL shouldNavigate = YES;
	if ([_delegate respondsToSelector:@selector(thumbsViewController:shouldNavigateToPhoto:)]) {
		shouldNavigate = [_delegate thumbsViewController:self shouldNavigateToPhoto:photo];
	}
	
	if (shouldNavigate) {
		NSString* URL = [self URLForPhoto:photo];
		if (URL) {
			TTOpenURL(URL);
		} else {
			TTPhotoViewController* controller = [self createPhotoViewController];
			controller.centerPhoto = photo;
			[self.navigationController pushViewController:controller animated:YES];
		}
	}
}

-(void)didSelectThumbView:(TTThumbView *)thumbView withPhoto:(id<TTPhoto>)photo{
    if ( !isSelectingPhotosForDownload ) {
        return;
    }
    TTDINFO(@"selected thumbview: %@",[thumbView debugDescription]);
    [photo setIsSelected:![photo isSelected]];
    [self.tableView reloadRowsAtIndexPaths:[self.tableView indexPathsForVisibleRows] withRowAnimation:UITableViewRowAnimationNone];
    [self updatePhotosSelectedStatus];
}

-(void) updatePhotosSelectedStatus {
    // update number selected
    NSInteger numSelected = 0;
    PhotoSource *ps = (PhotoSource*) [(FolderContentsDataSource*) self.dataSource photoSource];
    for (NSInteger i = 0; i <= [ps maxPhotoIndex]; i++) {
        id<TTPhoto> thePhoto = [ps photoAtIndex:i];
        if ( [thePhoto isSelected] ) {
            numSelected++;
        }
    }
    self.title = numSelected > 0 ? [NSString stringWithFormat:NSLocalizedString(@"Photos Selected", @""),numSelected] : NSLocalizedString(@"Select Photos", @"");
    [self.downloadSelectedButton setEnabled:(numSelected > 0)];
}
#pragma mark
#pragma mark ProtectedContentDelegate
-(void) doAfterPrivateAccessAllowed {
	self.navigationController.view.alpha = 1.0;	
	self.privateFoldersAccessButton.image = [UIImage imageNamed:@"icon_unlock.png"];
	[self maybeRefreshDataSourceIfPrivateAccessHasChanged];
}

-(UIView*) getView {
	return self.navigationController.view;
}

-(void) doAfterPrivateAccessDenied {
	self.navigationController.view.alpha = 1.0;
}

-(void) doAfterPrivateAccessDeniedOnResume {
    [self.navigationController popToRootViewControllerAnimated:NO];
}


///////////////////////////////////////////////////////////////////////////////////////////////////
- (TTPhotoViewController*)createPhotoViewController {
	PhotoController *controller = [[[PhotoController alloc] init] autorelease];
	[controller setIsFromPrivateFolder:self.currentDirectory.isPrivate];
	[controller setManagedObjContext:self.managedObjContext];
    [controller setIsOfflineFolder:isOfflineFolder];
    [controller setParentVC:self];
    [controller setFolderContentsModel:(FolderContentsModel*) [self.dataSource model]];
    [controller setDelegate:self];
	return controller;
}



#pragma mark -
#pragma mark PopOverContentDelegate

- (void)showUpgradeViewController {
#ifdef FREE_UPGRADABLE_VERSION
    [self showUpgradeView:nil];
#endif
}

-(void)showSettingsController {
	IASKAppSettingsViewController *appSettingsViewController = [[IASKAppSettingsViewController alloc] initWithNibName:@"IASKAppSettingsView" bundle:nil];
	appSettingsViewController.delegate = self;
	UINavigationController *aNavController = [[UINavigationController alloc] initWithRootViewController:appSettingsViewController];
    [appSettingsViewController setShowCreditsFooter:NO];
    appSettingsViewController.showDoneButton = YES;
	aNavController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
	if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
		aNavController.modalPresentationStyle = UIModalPresentationFormSheet;
    [self presentModalViewController:aNavController animated:YES];
	[appSettingsViewController release];
    [aNavController release];
}

-(void)popToRootController {
    [self popAllViewControllers];
}

#pragma mark -
#pragma mark PPRevealSideViewControllerDelegate
-(void) pprevealSideViewController:(PPRevealSideViewController *)controller didPopToController:(UIViewController *)centerController {
    self.tableView.userInteractionEnabled = YES;
    NSDictionary *sortOptions = [Utilities folderSortOptions];
    NSString *name = [sortOptions objectForKey:@"sort_by_field"];
    NSString *isAscValue = [sortOptions objectForKey:@"is_asc"];
    
    // check if the saved values differ from that of the initially loaded values
    if ( ![self.sortByField isEqualToString:name] || sortAscending != [isAscValue isEqualToString:@"yes"]) {
        [self setSortByField:name];
        sortAscending = [isAscValue isEqualToString:@"yes"];
        [self refreshAction:nil];
    } else {
        NSLog(@"Sort options remained the same, not refreshing folder contents...");
    }
}

- (void) pprevealSideViewController:(PPRevealSideViewController *)controller didPushController:(UIViewController *)pushedController {
    self.tableView.userInteractionEnabled = NO;
}


#pragma mark
#pragma mark IAskSettingsDelegate
- (void)settingsViewControllerDidEnd:(IASKAppSettingsViewController*)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
    if ( ![[NSUserDefaults standardUserDefaults] boolForKey:@"pref_save_server_passwords"] ) {
        [Utilities destroySavedPasswords];
    }
}

#pragma mark
#pragma mark UISearchBarDelegate
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    NSArray *urlParts = [self.currentDirectory.url componentsSeparatedByString:@"/"];
    NSString *lastPart = [urlParts lastObject];
    NSArray *queryParams = [lastPart componentsSeparatedByString:@"?"];
    NSString *queryParams2 = [queryParams lastObject];
    NSArray *queryParams3 = [queryParams2 componentsSeparatedByString:@"&"];
    
    
    NSString *searchResultsUrl = [NSString stringWithFormat:@"http://%@/files/getSearchResults?%@&q=%@",[urlParts objectAtIndex:2],[queryParams3 objectAtIndex:0],[Utilities encodeToPercentEscapeString:[searchBar text]]];
    TTDINFO(@"Search results url = %@",searchResultsUrl);
    SearchResultsDirectoryFile *dir = [[SearchResultsDirectoryFile alloc] initWithName:NSLocalizedString(@"Search Results",@"") url:searchResultsUrl filetype:FileTypeDirectory];
    [dir setIsPrivate:self.currentDirectory.isPrivate];
    
    FolderContentsController *controller = [[FolderContentsController alloc] initWithNavigatorURL:nil query:[NSDictionary dictionaryWithObjectsAndKeys:dir,@"file",self.currentDirectory,@"parent_directory",self.managedObjContext,@"managedObjectContext",nil]];
    [self.navigationController pushViewController:controller animated:YES];
    [controller release];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
    [searchBar setShowsCancelButton:NO animated:YES];
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    [searchBar setShowsCancelButton:YES animated:YES];
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    [searchBar setShowsCancelButton:NO animated:YES];
}

#pragma mark
#pragma mark PhotoControllerDelegate
-(void)setCurrentPhotoIndex:(NSInteger)index {
    lastCenterPhotoIndex = index;
    TTDINFO(@"Last photo index = %ld",(long) lastCenterPhotoIndex);
}

-(void)dealloc {
	[self.currentDirectory release];
    [self.parentDirectory release];
	[self.managedObjContext release];
    [self.offlineButton release];
    [self.downloadSelectedButton release];
    [self.indexPathAtBeginDragging release];

	[[NSNotificationCenter defaultCenter] removeObserver:self];
	[titleForView release];
	if ( privatePasswordDialog ) {
		[privatePasswordDialog release];
	}
	[privateFoldersAccessButton release];
	if ( lockFlashView ) {
		[lockFlashView release];
	}
	if ( picker ) {
		TT_RELEASE_SAFELY(picker);
	}
	if ( actionSheetButtons ) {
		TT_RELEASE_SAFELY(actionSheetButtons);
	}
    [folderSearchBar release];
	[super dealloc];
}
@end
