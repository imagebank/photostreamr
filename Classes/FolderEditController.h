//
//  FolderEditController.h
//  ImageBankFree
//
//  Created by Yoshi Sugawara on 5/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FolderEditController : UITableViewController<UITextFieldDelegate> {
    IBOutlet UITableViewCell *customCell;
    IBOutlet UITextField *folderNameField;
    NSString *fullPath;
    NSString *folderName;
    BOOL isPrivate;
}

-(IBAction)clearTextField:(id)sender;


@property (nonatomic,assign) IBOutlet UITableViewCell *customCell;
@property (nonatomic,assign) IBOutlet UITextField *folderNameField;
@property (nonatomic,retain) NSString  *fullPath;
@property (nonatomic,retain) NSString *folderName;
@property (nonatomic,assign) BOOL isPrivate;

@end
