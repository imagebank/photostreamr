//
//  CreateOfflineFilesOperation.m
//  ImageBankFree
//
//  Created by Yoshi Sugawara on 5/14/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CreateOfflineFilesOperation.h"
#import "Utilities.h"
#import "File.h"
#import "NSObject+SBJSON.h"
#import "SBJSON.h"

@implementation CreateOfflineFilesOperation

@synthesize error = error_;
@synthesize orientation, popUpView, model, selectedImageFiles, fileData, pathToDocumentsDirectory, pathToTempDirectory, saveToCameraRoll, directoryName;

#pragma mark -
#pragma mark Initialization & Memory Management

-(id)initWithFileListModel:(FileListModel*)fileListModel withOrientation:(UIInterfaceOrientation)orient withDirName:(NSString*)dirName {
    if( (self = [super init]) ) {
        [self setModel:fileListModel];
        [self setOrientation:orient];
        [self setDirectoryName:dirName];
        [self setup];
    }
    return self;
}

-(id)initWithFileListModel:(FileListModel *)fileListModel withPhotoSource:(PhotoSource*)photoSource withOrientation:(UIInterfaceOrientation)orient withDirName:(NSString*)dirName {
    if ( (self = [super init]) ) {
        [self setModel:fileListModel];
        NSMutableArray *imageFiles = [[[NSMutableArray alloc] init] autorelease];
        for (NSInteger i = 0; i <= [photoSource maxPhotoIndex]; i++) {
            Photo *photo = (Photo*) [photoSource photoAtIndex:i];
            if ( [photo isSelected] ) {
                ImageFile *file = [[[ImageFile alloc] initWithName:[photo getFilename]
                                                        url:[photo URLForVersion:TTPhotoVersionLarge]
                                                       size:[photo size] 
                                               thumbnailUrl:[photo URLForVersion:TTPhotoVersionThumbnail]
                                             smallThumbnailUrl:[photo URLForVersion:TTPhotoVersionSmall] metadataUrl:[photo getMetadataUrl]] autorelease];
                [imageFiles addObject:file];
            }
        }
        [self setSelectedImageFiles:imageFiles];
        [self setOrientation:orient];
        [self setDirectoryName:dirName];
        [self setup];
    }
    return self;
}

-(void)setup {
    [self setPopUpView:[[PopUpNotificationView alloc] initWithFrame:CGRectZero]];
    [self.popUpView.spinner setHidesWhenStopped:YES];
    [self setFileData:[[NSMutableArray alloc] init]];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES); 
    NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
    
    [self setPathToDocumentsDirectory:[documentsDirectory stringByAppendingPathComponent:self.directoryName]];
    [self setPathToTempDirectory:[NSTemporaryDirectory() stringByAppendingPathComponent:self.directoryName]];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(cancel)
                                                 name:UIApplicationWillResignActiveNotification object:nil];
}

- (void)dealloc
{
    [popUpView release];
    [model release];
    [fileData release];
    [pathToDocumentsDirectory release];
    [pathToTempDirectory release];
    [directoryName release];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [super dealloc];
}

- (BOOL)isExecuting
{
    return executing_;
}

- (BOOL)isFinished
{
    return finished_;
}

#pragma mark -
#pragma mark Start & Utility Methods

- (void)done
{
    if ( error_ != nil ) {
        [self updateStatusMessageFail];
    } else if ( self.fileData != nil && [self.fileData count] > 0 ) {
        // save json model
        NSString *json = [fileData JSONRepresentation];
        NSError *error = nil;
        
        // mark as private if directory is private
        if ( [[self.model directory] isPrivate] ) {
            [Utilities enableOfflinePrivateFolder:self.pathToTempDirectory];            
        }
        
        // copy from temp dir to docs dir
        
        NSString *existingModelJsonFilePath = [self.pathToDocumentsDirectory 
                                               stringByAppendingPathComponent:@"model.json"];
        
        if ( [[NSFileManager defaultManager] fileExistsAtPath:existingModelJsonFilePath] ) {
            // merge the contents if the directory already exists
            
            // move the individual files over
            for (NSString *file in [[NSFileManager defaultManager] 
                                    contentsOfDirectoryAtPath:self.pathToTempDirectory 
                                    error:&error] )
            {
                [[NSFileManager defaultManager] 
                 moveItemAtPath:[self.pathToTempDirectory stringByAppendingPathComponent:file]
                 toPath:[self.pathToDocumentsDirectory stringByAppendingPathComponent:file]
                 error:&error];
            }

            // rebuild model json
            // get original model json
            NSString *existingModelJson = [NSString 
                                           stringWithContentsOfFile:existingModelJsonFilePath
                                           encoding:NSUTF8StringEncoding error:&error];
            
            SBJSON *parser = [[SBJSON alloc] init];
            NSArray *existingFileData = [parser objectWithString:existingModelJson error:&error];
            NSMutableArray *subpredicates = [NSMutableArray array];
            for (NSDictionary *data in existingFileData) {
                NSPredicate *subpredicate = [NSPredicate predicateWithFormat:@"NOT (SELF.url like[cd] %@)",(NSString*) [data objectForKey:@"url"]];
                [subpredicates addObject:subpredicate];
            }                
            NSPredicate *filter = [NSCompoundPredicate andPredicateWithSubpredicates:subpredicates];
            
            // only include images that don't exist in the original model json
            [fileData filterUsingPredicate:filter];
            NSArray *mergedFileData = [existingFileData arrayByAddingObjectsFromArray:fileData];
            NSString *mergedJson = [mergedFileData JSONRepresentation];
            
            TTDINFO(@"merged model json: %@",mergedJson);
            
            [[NSFileManager defaultManager] removeItemAtPath:existingModelJsonFilePath error:nil];
            [mergedJson writeToFile:existingModelJsonFilePath 
                         atomically:YES encoding:NSUTF8StringEncoding error:&error];
        } else {

            NSString *modelJson = [self.pathToTempDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"model.json"]];
            [json writeToFile:modelJson atomically:YES encoding:NSUTF8StringEncoding error:&error];
            
            NSError *moveError = nil;
            if (![[NSFileManager defaultManager] moveItemAtPath:self.pathToTempDirectory toPath:self.pathToDocumentsDirectory error:&moveError]) {
                NSLog(@"Error moving from tmp to docs dir: %@", [moveError localizedDescription]);
            }
        }
        
        [self updateStatusMessageSuccess];
    }
    
    // Alert anyone that we are finished
    [self willChangeValueForKey:@"isExecuting"];
    [self willChangeValueForKey:@"isFinished"];
    executing_ = NO;
    finished_  = YES;
    [self didChangeValueForKey:@"isFinished"];
    [self didChangeValueForKey:@"isExecuting"];
    
    [self hideView];
    [self release];
}

-(void)cancelled {
    [self willChangeValueForKey:@"isExecuting"];
    [self willChangeValueForKey:@"isFinished"];
    executing_ = NO;
    finished_ = YES;
    [self willChangeValueForKey:@"isExecuting"];
    [self willChangeValueForKey:@"isFinished"];
    
    if ( [[NSFileManager defaultManager] fileExistsAtPath:self.pathToTempDirectory] ) {
        NSError *error = nil;
        [[NSFileManager defaultManager] removeItemAtPath:self.pathToTempDirectory error:&error];
    }
    [self updateStatusMessage:NSLocalizedString(@"Canceling Download...",@"")];
    [self hideView];
    [self release];	
}

- (void)start
{
    // Ensure that this operation starts on the main thread
//    if (![NSThread isMainThread])
//    {
//        [self performSelectorOnMainThread:@selector(start)
//                               withObject:nil waitUntilDone:NO];
//        return;
//    }
    
    // Ensure that the operation should exute
    if( finished_ || [self isCancelled] ) { [self done]; return; }
    
    // From this point on, the operation is officially executing--remember, isExecuting
    // needs to be KVO compliant!
    [self willChangeValueForKey:@"isExecuting"];
    executing_ = YES;
    [self didChangeValueForKey:@"isExecuting"];
    self.popUpView.progressView.hidden = NO;
    self.popUpView.progressView.progress = 0.0f;
    [self slideInPopUpView];
    [self generateFiles];
}

-(void) generateFiles {
    // reload model
    if ( selectedImageFiles == nil ) {
        TTDINFO(@"reloading model...");
        [self.model loadSynchronously:TTURLRequestCachePolicyDefault];
        [self setSelectedImageFiles:[self.model files]];
    }
    
    // create directory
    TTDINFO(@"creating offline directory: %@",self.pathToTempDirectory);
    NSError *createDirError = nil;
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:self.pathToTempDirectory])
        [[NSFileManager defaultManager] createDirectoryAtPath:self.pathToTempDirectory withIntermediateDirectories:NO attributes:nil error:&createDirError];
    
    NSInteger totalNumFiles = [selectedImageFiles count];
    NSInteger fileCount = 0;
    
    for (id f in selectedImageFiles) {
        NSAutoreleasePool *p = [[NSAutoreleasePool alloc] init];
        
        File *file = (File*) f;
        if ([file isKindOfClass:[ImageFile class]]) {
            
            if ( [self isCancelled] ) {
                [self cancelled];
                return;
            }
            
            [self updateStatusMessage:[NSString stringWithFormat:NSLocalizedString(@"Downloading Photos to Saved Folders progress", @""),++fileCount,totalNumFiles]];
            
            // fetch image and save it
            ImageFile *imageFile = (ImageFile*)file;
            TTDINFO(@"fetching image for offline: %@",[imageFile url]);
            NSString *imageUrl = [imageFile url];

            if ( saveToCameraRoll && [[NSUserDefaults standardUserDefaults] boolForKey:@"pref_save_full_size"] ) {
                // save full size images for camera roll
                imageUrl = [NSString stringWithFormat:@"%@&noresize=1",imageUrl];
            }
            
            TTURLRequest *request = [TTURLRequest requestWithURL:imageUrl delegate:self];
            request.response = [[[TTURLImageResponse alloc] init] autorelease];
            request.httpMethod = @"GET";
            [request setCachePolicy:TTURLRequestCachePolicyNoCache];
            [request sendSynchronously];
//            NSData *imageData = UIImageJPEGRepresentation(((TTURLImageResponse*) request.response).image,1.0);
            NSData *imageData = ((TTURLImageResponse*) request.response).rawData;
            NSString *offlineImageFilename = [self.pathToTempDirectory stringByAppendingPathComponent:[imageFile name]];
            TTDINFO(@"saving image for offline: %@",offlineImageFilename);
            
            if ( saveToCameraRoll ) {
                UIImage *imageToSave = [UIImage imageWithData:imageData];
                UIImageWriteToSavedPhotosAlbum(imageToSave, nil, nil, nil);
            } else {
                [imageData writeToFile:offlineImageFilename atomically:YES];
                
                // fetch thumbnail and save it
                TTDINFO(@"fetching thumbnail image for offline: %@",[imageFile thumbnailUrl]);
                request = [TTURLRequest requestWithURL:[imageFile thumbnailUrl] delegate:self];
                request.response = [[[TTURLImageResponse alloc] init] autorelease];
                request.httpMethod = @"GET";
                [request sendSynchronously];
                imageData = UIImageJPEGRepresentation(((TTURLImageResponse*) request.response).image,1.0);
                
                NSString *offlineThumbnailFilename = [self.pathToTempDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.thm",[imageFile name]]];
                TTDINFO(@"saving thumbnail image for offline: %@",offlineThumbnailFilename);
                [imageData writeToFile:offlineThumbnailFilename atomically:YES];
                
                // write out file as NSDictionary and append to data array for serialization later
                NSMutableDictionary *fileDataDict = [[[NSMutableDictionary alloc] init] autorelease];
                [fileDataDict setObject:[NSString stringWithFormat:@"documents://%@/%@",self.directoryName, [imageFile name]] forKey:@"url"];
                
                [fileDataDict setObject:[imageFile name] forKey:@"name"];
                [fileDataDict setObject:[NSString stringWithFormat:@"%f",[imageFile size].width] forKey:@"size_x"];
                [fileDataDict setObject:[NSString stringWithFormat:@"%f",[imageFile size].height] forKey:@"size_y"];
                [fileDataDict setObject:[NSString stringWithFormat:@"documents://%@/%@",self.directoryName, [NSString stringWithFormat:@"%@.thm",[imageFile name]]] forKey:@"thumbnail_url"];
                if ( [imageFile metadataUrl] != nil ) {
                    [fileDataDict setObject:[imageFile metadataUrl] forKey:@"metadata_url"];
                }
                [fileData addObject:fileDataDict];
            }
            [self updateProgress:[NSNumber numberWithFloat:(( (float) fileCount / (float)totalNumFiles))]];
            
        }
        [p drain];
    }
    TTDINFO(@"Done generating offline files");
    [self done];
}

-(void)slideInPopUpView {
    if (![NSThread isMainThread]) {
        [self performSelectorOnMainThread:@selector(slideInPopUpView) withObject:nil waitUntilDone:NO];
        return;
    }
    [popUpView slideInPopUpView:self.orientation localizedTextKey:@"Downloading Photos to Saved Folders"];
}

-(void) hideView {
    if (![NSThread isMainThread]) {
        [self performSelectorOnMainThread:@selector(hideView) withObject:nil waitUntilDone:NO];
        return;
    }
    [[NSNotificationCenter defaultCenter] 
     postNotificationName:@"SaveForOfflineDidFinish" 
     object:self];
    [popUpView hideView];
}

-(void)slideOutPopUpFromView {
    if (![NSThread isMainThread]) {
        [self performSelectorOnMainThread:@selector(slideOutPopUpFromView) withObject:nil waitUntilDone:NO];
        return;
    }
	[self.popUpView slideOutPopUpFromView];
}

-(void)updateStatusMessageSuccess {
    if (![NSThread isMainThread]) {
        [self performSelectorOnMainThread:@selector(updateStatusMessageSuccess) withObject:nil waitUntilDone:NO];
        return;
    }
    [self.popUpView.spinner stopAnimating];
    [self.popUpView.doneImage setHidden:NO];
    [self.popUpView bringSubviewToFront:self.popUpView.doneImage];
    [self.popUpView.statusMsg setText:NSLocalizedString(@"Photos saved in Saved Folders",@"")];
}

-(void)updateStatusMessageFail {
    if (![NSThread isMainThread]) {
        [self performSelectorOnMainThread:@selector(updateStatusMessageFail) withObject:nil waitUntilDone:NO];
        return;
    }
    [self.popUpView.spinner stopAnimating];
    [self.popUpView.failImage setHidden:NO];
    [self.popUpView bringSubviewToFront:self.popUpView.failImage];
    [self.popUpView.statusMsg setText:NSLocalizedString(@"Could not save photos",@"Could not save photos")];    
}

-(void)updateStatusMessage:(NSString*)msg {
    if (![NSThread isMainThread]) {
        [self performSelectorOnMainThread:@selector(updateStatusMessage:) withObject:msg waitUntilDone:NO];
        return;
    }
    [self.popUpView.statusMsg setText:msg];
}

-(void)updateProgress:(NSNumber*)progress {
    if (![NSThread isMainThread]) {
        [self performSelectorOnMainThread:@selector(updateProgress:) withObject:progress
                waitUntilDone:NO];
        return;
    }
    TTDINFO(@"progress = %f",[progress floatValue]);
    [self.popUpView.progressView setProgress:[progress floatValue] animated:YES];
}

@end
