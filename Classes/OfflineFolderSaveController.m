//
//  OfflineFolderSaveController.m
//  ImageBankFree
//
//  Created by Yoshi Sugawara on 7/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "OfflineFolderSaveController.h"
#import "Utilities.h"
#import "CreateOfflineFilesOperation.h"
#import "NSOperationQueue+SharedQueue.h"
#import "PrivateFolderPasswordChecker.h"
#import "Constants.h"


@interface OfflineFolderSaveController ()

@end

@implementation OfflineFolderSaveController

@synthesize existingFolders, folderContentsModel, parentDirectory, isDownloadingAll, parentVC, alertView, privateFoldersAccessButton, lastSavedFolder, isSavingFromPhotoController;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:UITableViewStyleGrouped];
    if (self) {
        [self setExistingFolders:[[NSMutableArray alloc] init]];
        [self setAlertView:[[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"New Folder",@"")
                                               message:NSLocalizedString(@"Enter a name for the folder.",@"")
                                              delegate:self 
                                     cancelButtonTitle:NSLocalizedString(@"Cancel",nil) 
                                     otherButtonTitles:NSLocalizedString(@"Save",nil), nil] autorelease]];
        self.alertView.alertViewStyle = UIAlertViewStylePlainTextInput;
    }
    return self;
}

-(void) dealloc {
    [existingFolders release];
    [folderContentsModel release];
    [parentVC release];
    [alertView release];
    [privateFoldersAccessButton release];
    [parentDirectory release];
    [lastSavedFolder release];
    [super dealloc];
}

-(NSMutableArray*) getOfflineFolders {
    NSMutableArray *folders = [Utilities offlineFolders];
    NSMutableArray *entries = [[[NSMutableArray alloc] init] autorelease];
    for (DirectoryFile* dir in folders) {
        [entries addObject:[dir name]];
    }
    return entries;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.privateFoldersAccessButton = [[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:[[PrivateFolderPasswordChecker sharedInstance] allowOfflineAccess] ? @"icon_unlock.png" : @"icon_lock.png"]
																		style:UIBarButtonItemStyleBordered
																	   target:self
																	   action:@selector(showPrivatePasswordAccess:)] autorelease];
    privateFoldersAccessButton.tag = kPrivateFoldersAccessToolbarButton;
    
    privatePasswordDialog = [[PrivatePasswordDialog alloc] initWithVC:self];
    [privatePasswordDialog setIsForOfflineFolders:YES];

    UIBarButtonItem *space = [[[UIBarButtonItem alloc] 
                               initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                               target:nil action:nil] autorelease];
    UIBarButtonItem *cancelButton = [[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Cancel",@"") style:UIBarButtonItemStyleBordered target:self action:@selector(cancel:)] autorelease];
    [cancelButton setTintColor:[UIColor colorWithRed:0.7 green:0 blue:0 alpha:1]];

    NSMutableArray *toolbarButtons = [NSMutableArray arrayWithObjects:space, nil];
    
    if ( [Utilities isOfflinePrivateFoldersPasswordSetup] ) {
        [toolbarButtons addObject:privateFoldersAccessButton];
        [toolbarButtons addObject:space];
    }
    
    [toolbarButtons addObject:cancelButton];    
    [self setToolbarItems:toolbarButtons];
    
    self.title = NSLocalizedString(@"Select Destination Folder", @"");
    
    [self.tableView setBackgroundColor:[UIColor colorWithRed:211/255.0f green:211/255.0f blue:211/255.0f alpha:1.0f]];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

-(void) viewWillAppear:(BOOL)animated {
    [self.existingFolders removeAllObjects];
    [self setExistingFolders:[self getOfflineFolders]];
    self.navigationController.navigationBar.barStyle = UIBarStyleDefault;
    self.navigationController.toolbar.tintColor = [UIColor grayColor];
    self.navigationController.navigationBar.tintColor = [UIColor grayColor];
    
    // get last saved folder
    [self setLastSavedFolder:[Utilities lastSavedFolder]];
    
    [super viewWillAppear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ( section == 0 ) {
        if ( lastSavedFolder != nil ) {
            if ( [lastSavedFolder isPrivate] && (![[PrivateFolderPasswordChecker sharedInstance] allowOfflineAccess] || ![[PrivateFolderPasswordChecker sharedInstance] allowAccess]) ) {
                return 1;
            } else {
                return 2;
            }
        }
        return 1;
    }
    return [self.existingFolders count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if ( section == 0) {
        return NSLocalizedString(@"New/Recent",@"New/Recent");
    } else if ( section == 1 ){
        return NSLocalizedString(@"All Saved Folders",@"All Saved Folders");
    }
    return @"";
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }

    UIImage *image = [UIImage imageNamed:@"cell-gradient.png"];
    cell.backgroundColor = [UIColor colorWithPatternImage:image];
    cell.textLabel.backgroundColor = [UIColor clearColor];
    cell.imageView.image = [UIImage imageNamed:@"tableitem-folder.png"];
    cell.textLabel.font = [UIFont systemFontOfSize:15];
    cell.textLabel.lineBreakMode = UILineBreakModeCharacterWrap;
    cell.textLabel.numberOfLines = 0;

    // Set up the cell...
    if ( indexPath.section == 0 ) {
        cell.textLabel.text = NSLocalizedString(@"New Folder", @"");
        if ( indexPath.row == 1 && self.lastSavedFolder != nil) {
            cell.textLabel.text = [NSString stringWithFormat:NSLocalizedString(@"Last Used: %@", @""),[lastSavedFolder name]];
        }
    } else {
    
        NSString *fullPath = [self.existingFolders objectAtIndex:indexPath.row];
        NSArray *parts = [fullPath componentsSeparatedByString:@"/"];
        NSString *filename = [parts objectAtIndex:[parts count]-1];
        
        cell.textLabel.text = filename;
        
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    
    if ( indexPath.section == 1 ) {
        NSString *text = [self.existingFolders objectAtIndex:[indexPath row]];
        NSArray *parts = [text componentsSeparatedByString:@"/"];
        NSString *filename = [parts objectAtIndex:[parts count]-1];
        
        CGSize constraint = CGSizeMake(self.tableView.bounds.size.width, 144.0f);
        
        CGSize size = [filename sizeWithFont:[UIFont systemFontOfSize:15.0] constrainedToSize:constraint lineBreakMode:UILineBreakModeCharacterWrap];
        
        CGFloat height = MAX(size.height + 40, 44.0f);
        
        return height;
    }
    return 44.0f;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

-(NSString*)getDirName {
    return (parentDirectory != nil && [[self.folderContentsModel.fileListModel directory] isKindOfClass:[RandomDirectoryFile class]] ) ?
    [parentDirectory name] : [[self.folderContentsModel.fileListModel directory] name];
}

-(void) startSaveOperation:(NSString*)folderName {
    CreateOfflineFilesOperation *operation = nil;
    FileListModel *model = [self.folderContentsModel fileListModel];
    if ( isDownloadingAll ) {
        operation = [[CreateOfflineFilesOperation alloc] initWithFileListModel:model
                                                               withOrientation:self.interfaceOrientation withDirName:folderName];
    } else {
        // use current photosource
        PhotoSource *currentPhotoSource = [self.folderContentsModel photosModel];
        operation = [[CreateOfflineFilesOperation alloc] 
                     initWithFileListModel:model withPhotoSource:currentPhotoSource withOrientation:self.interfaceOrientation
                     withDirName:folderName];
    }
    [Utilities storeLastSavedFolder:folderName isPrivate:model.directory.isPrivate];
    [[NSOperationQueue sharedOperationQueue] addOperation:operation];
        [parentVC changeOfflineButtonToInProgress];
        [parentVC doneSelectingPhotos:self];
    [self dismissModalViewControllerAnimated:YES];
}

-(void)saveInNewFolder:(id)sender {
    // show alert view
    UITextField *folderNameField = [self.alertView textFieldAtIndex:0];
    folderNameField.text = [self getDirName];
    [self.alertView show];
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ( indexPath.section == 0 && indexPath.row == 0) {
        [self saveInNewFolder:nil];
        return;
    }
    
    if ( indexPath.section == 0 && indexPath.row == 1) {
        [self startSaveOperation:[lastSavedFolder name]];
        return;
    }
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.accessoryType = UITableViewCellAccessoryCheckmark;
   
    NSString *fullPath = [self.existingFolders objectAtIndex:indexPath.row];
    NSArray *parts = [fullPath componentsSeparatedByString:@"/"];
    NSString *filename = [parts objectAtIndex:[parts count]-1];    
    [self startSaveOperation:filename];
}


-(void)cancel:(id)sender {
        [parentVC doneSelectingPhotos:self];
    [UIViewController attemptRotationToDeviceOrientation];
    [self dismissModalViewControllerAnimated:YES];
}



#pragma mark
#pragma mark UIAlertViewDelegate
- (void)alertView:(UIAlertView *)thisAlertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if ( buttonIndex == 1 ) {
        // hit save button
        UITextField *textField = [thisAlertView textFieldAtIndex:0];
        [self startSaveOperation:textField.text];
    }
}

- (BOOL)alertViewShouldEnableFirstOtherButton:(UIAlertView *)thisAlertView
{
    UITextField *textField = [thisAlertView textFieldAtIndex:0];
    if ([textField.text length] == 0)
    {
        return NO;
    }
    return YES;
}

#pragma mark
#pragma mark ProtectedContentDelegate
-(void) doAfterPrivateAccessAllowed {
	self.navigationController.view.alpha = 1.0;	
	self.privateFoldersAccessButton.image = [UIImage imageNamed:@"icon_unlock.png"];
    [self.existingFolders removeAllObjects];
    [self setExistingFolders:[self getOfflineFolders]];
    [self.tableView reloadData];
}

-(UIView*) getView {
	return parentVC.navigationController.view;
}

-(void) doAfterPrivateAccessDenied {
	self.navigationController.view.alpha = 1.0;
}

-(void) doAfterPrivateAccessDeniedOnResume {
    [self.existingFolders removeAllObjects];
    [self setExistingFolders:[self getOfflineFolders]];
    [self.tableView reloadData];
}

-(void)showPrivatePasswordAccess:(id)sender {
    
	if ( [[PrivateFolderPasswordChecker sharedInstance] allowOfflineAccess] ) {
		// private folders are currently viewable, so user wants to lock access
		[[PrivateFolderPasswordChecker sharedInstance] setAllowOfflineAccess:NO];
		
		CGRect lockFlashRect = CGRectMake(self.view.center.x - 100.0f, self.view.center.y - 100.0f, 200.0f, 200.0f);
		lockFlashView = [[UIImageView alloc] initWithFrame:lockFlashRect];
		lockFlashView.image = [UIImage imageNamed:@"img-lock.png"];
		lockFlashView.alpha = 0.75;
		[self.navigationController.view addSubview:lockFlashView];
		[UIView beginAnimations:nil context:nil];
		[UIView setAnimationDuration:1.2];
		[UIView setAnimationTransition:UIViewAnimationTransitionNone forView:self.navigationController.view cache:YES];
		[UIView setAnimationDelegate:self];
		[UIView setAnimationDidStopSelector:@selector(lockFlashAnimationDone)];
		lockFlashView.alpha = 0.0;
		[UIView commitAnimations];
		
	} else {
		[privatePasswordDialog showPrivatePasswordDialog];
	}
}

-(void)lockFlashAnimationDone {
	[lockFlashView removeFromSuperview];
	self.privateFoldersAccessButton.image = [UIImage imageNamed:@"icon_lock.png"];
    [self.existingFolders removeAllObjects];
    [self setExistingFolders:[self getOfflineFolders]];
    [self.tableView reloadData];
}	

@end
