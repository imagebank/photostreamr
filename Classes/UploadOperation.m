//
//  UploadOperation.m
//  ImageBankFree
//
//  Created by yoshi on 4/12/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "UploadOperation.h"
#import "FileUploadNotifier.h"
#import "Utilities.h"


@implementation UploadOperation

@synthesize fileUploadingView,imageData,dir,orientation;

-(id)initWithData:(NSData*)data forDirectory:(DirectoryFile*)d withOrientation:(UIInterfaceOrientation)orient {
	if ((self = [super init])) {
		[self setImageData:data];
		[self setDir:d];
		[self setOrientation:orient];
		[self setFileUploadingView:[[PopUpNotificationView alloc] initWithFrame:CGRectZero]];
		[self.fileUploadingView.spinner setHidesWhenStopped:YES];
	}
	return self;
}

-(void)start {
	CGRect uploadProgressFrame = self.fileUploadingView.frame;
	// set it out of frame
	UIWindow *window = [[UIApplication sharedApplication] keyWindow];

	if ( UIDeviceOrientationIsLandscape(self.orientation) ) {
		CGAffineTransform rotationTransform = CGAffineTransformIdentity;
		uploadProgressFrame.origin.y = 0;
		uploadProgressFrame.origin.x = window.frame.size.width + 20;
		if ( self.orientation == UIDeviceOrientationLandscapeLeft ) {
			rotationTransform = CGAffineTransformRotate(rotationTransform, DegreesToRadians(90));
			uploadProgressFrame.origin.y = 100;
		} else {
			rotationTransform = CGAffineTransformRotate(rotationTransform, DegreesToRadians(270));
			uploadProgressFrame.origin.y = window.frame.size.height - 100;
		}
		self.fileUploadingView.transform = rotationTransform;
	} else {
		uploadProgressFrame.origin.y = -80;
	}

	self.fileUploadingView.frame = uploadProgressFrame;
	self.fileUploadingView.statusMsg.text = NSLocalizedString(@"Upload in progress",@"Upload in progress...");
	[window addSubview:self.fileUploadingView];
	
	// show view indicating image being uploaded
	// todo: animate the view into position
	[UIView beginAnimations:@"showUploadingView" context:NULL];
	//	[UIView setAnimationDelay:1];
	[UIView setAnimationCurve:UIViewAnimationCurveLinear];
	[UIView setAnimationDuration:0.5];
	CGRect uploadFrame = self.fileUploadingView.frame;
	if ( UIDeviceOrientationIsLandscape(self.orientation) ) {
		uploadFrame.origin.x = window.frame.size.width - 40;
		if ( self.orientation == UIDeviceOrientationLandscapeRight ) {
			uploadFrame.origin.y = window.frame.size.height - 100;
		} else {
			uploadFrame.origin.y = 100;
		}
	} else {
		uploadFrame.origin.y = 20;
	}
	self.fileUploadingView.frame = uploadFrame;
	[UIView commitAnimations];
	[self.fileUploadingView setHidden:NO];
	[[self.fileUploadingView spinner] startAnimating];
	[NSTimer scheduledTimerWithTimeInterval:1.5
									 target:self
								   selector:@selector(doUploadRequest:)
								   userInfo:nil
									repeats:NO];
}

-(void)doUploadRequest:(NSTimer*)timer {
	// get url, in a hackish way
	NSDate *today = [NSDate date];
	NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
	[dateFormat setDateFormat:@"yyyy-MM-dd-hh-mm-ss-a"];	
	NSArray *parts = [[self.dir url] componentsSeparatedByString:@"/"];
	NSString *fullPathEscaped = [[Utilities encodeToPercentEscapeString:[self.dir url]] autorelease];
	NSString *url = [NSString stringWithFormat:@"http://%@/%@/upload?updir=%@&filename=%@",
					 (NSString*)[parts objectAtIndex:2],
					 (NSString*)[parts objectAtIndex:3],
					 fullPathEscaped,
					 [NSString stringWithFormat:@"imagebank-upload-%@.jpg",[dateFormat stringFromDate:today]]];
	//	NSString *url = [NSString stringWithFormat:@"http://%@/%@/upload",
	//					 (NSString*)[parts objectAtIndex:2],
	//					 (NSString*)[parts objectAtIndex:3]];
	
	NSLog(@"upload url = %@",url);
	TTURLRequest *request = [TTURLRequest requestWithURL:url
												delegate:self];
	request.response = [[[TTURLDataResponse alloc] init] autorelease];
	request.httpMethod = @"POST";
	[request.parameters setObject:@"blah" forKey:@"wtf"];
	request.cachePolicy = TTURLRequestCachePolicyNetwork;
	[request addFile:self.imageData mimeType:@"image/jpg" fileName:@"upload.jpg"];
	NSLog(@"sending request..");
	[request send];
    [dateFormat release];
}	

#pragma mark
#pragma mark TTURLRequestDelegate for image upload
- (void)requestDidFinishLoad:(TTURLRequest*)request {
	NSLog(@"request finish load..");
	TTURLDataResponse *response = request.response;
	NSString *responseBody = [[[NSString alloc] initWithData:[response data] encoding:NSUTF8StringEncoding] autorelease];
	NSString *responseText = [responseBody stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
	[self.fileUploadingView.spinner stopAnimating];
	if ( [responseText isEqualToString:@"OK"] ) {
		[self.fileUploadingView.statusMsg setText:NSLocalizedString(@"Done!",@"Done!")];
	} else {
		[self.fileUploadingView.statusMsg setText:NSLocalizedString(@"Upload failed",@"Upload failed")];
	}
	[self hideView];
}

-(void)request:(TTURLRequest*)request:didFailLoadWithError:(NSError*)error {
	NSLog(@"upload request error..");
	[self.fileUploadingView.spinner stopAnimating];
	[self.fileUploadingView.statusMsg setText:NSLocalizedString(@"Upload failed",@"Upload failed")];
	[self hideView];
}

-(void) hideView {
	[UIView beginAnimations:@"hideUploadingView" context:NULL];
	[UIView setAnimationDelay:0.5];
	[UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
	[UIView setAnimationDuration:1];
	[UIView setAnimationDelegate:self];
	[UIView setAnimationDidStopSelector:@selector(finish)];
	CGRect uploadViewFrame = self.fileUploadingView.frame;
	UIWindow *window = [[UIApplication sharedApplication] keyWindow];
	if ( UIDeviceOrientationIsLandscape(self.orientation) ) {
		uploadViewFrame.origin.x = window.frame.size.width + 40;
		if ( self.orientation == UIDeviceOrientationLandscapeRight ) {
			uploadViewFrame.origin.y = window.frame.size.height - 100;
		} else {
			uploadViewFrame.origin.y = 100;
		}
	} else {
		uploadViewFrame.origin.y = -80;
	}
	self.fileUploadingView.frame = uploadViewFrame;
	[UIView commitAnimations];
}

-(void)finish {
	[self.fileUploadingView removeFromSuperview];
	[[FileUploadNotifier sharedInstance] notifyComplete:self];	
}

-(void)dealloc {
	[imageData release];
	[dir release];
	[fileUploadingView release];
	[super dealloc];
}

@end
