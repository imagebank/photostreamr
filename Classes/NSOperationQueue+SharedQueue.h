//
//  SharedQueue.h
//  ImageBankFree
//
//  Created by Yoshi Sugawara on 7/29/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NSOperationQueue (SharedQueue)

+(NSOperationQueue*)sharedOperationQueue;

@end
