//
//  FileUploadingView.h
//  ImageBankFree
//
//  Created by yoshi on 4/10/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface PopUpNotificationView : UIView {
	IBOutlet UIActivityIndicatorView *spinner;
	IBOutlet UILabel *statusMsg;
    IBOutlet UIImageView *doneImage;
    IBOutlet UIImageView *failImage;
    IBOutlet UIProgressView *progressView;
    
    CGRect originalFrame;
}

@property(nonatomic,retain) IBOutlet UIActivityIndicatorView *spinner;
@property(nonatomic,retain) IBOutlet UILabel *statusMsg;
@property(nonatomic,retain) IBOutlet UIImageView *doneImage;
@property(nonatomic,retain) IBOutlet UIImageView *failImage;
@property(nonatomic,retain) IBOutlet UIProgressView *progressView;

-(void)slideInPopUpView:(UIInterfaceOrientation)orientation localizedTextKey:(NSString*)textKey;
-(void)hideView;
-(void)slideOutPopUpFromView;

@end
