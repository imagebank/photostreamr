//
//  ChooseServerController.h
//  PhotoStreamr
//
//  Created by yoshi on 5/31/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Three20/Three20.h>
#import "Server.h"
#import "PopOverContentDelegate.h"
#import "IASKAppSettingsViewController.h"
#import "PPRevealSideViewController.h"


@interface ChooseServerController : UIViewController<IASKSettingsDelegate,UITableViewDelegate,
                                    UITableViewDataSource, 
									UITextFieldDelegate,UIAlertViewDelegate,TTURLResponse,
                                    TTURLRequestDelegate,
                                    PopOverContentDelegate,
                                    NSNetServiceDelegate,NSNetServiceBrowserDelegate,
                                    PPRevealSideViewControllerDelegate
                                    > 
{	
    NSNetServiceBrowser *serviceBrowser;
	NSMutableArray *servers;
    // servers discovered by bonjour
    NSMutableArray *availableBonjourServers;
    // choosable bonjour servers
    NSMutableArray *choosableBonjourServers;
	NSManagedObjectContext *managedObjContext;
	IBOutlet UITableView *serversTableView;
	IBOutlet UIView *connectingView;
	IBOutlet UIActivityIndicatorView *connectingIndicator;
	IBOutlet UIView *mainView;
	IBOutlet UIWebView *webView;
	NSString *selectedServer;
	TTURLRequest *currentRequest;                                        
}

- (id)initWithNavigatorURL:(NSURL*)URL query:(NSDictionary*)query;
-(void)showPasswordAlert;
-(void)startCheckingPasswordForServer:(NSString*)serverUrl password:(NSString*)password;
-(void)startLookupAddressByPIN:(NSString*)serverUrl;
- (void) openFileListControllerWithServer: (NSString *) serverUrl;
-(void)showServerUnreachableAlert;
-(void)showHelp:(id)sender;

-(IBAction)cancelConnectToServer:(id)sender;

-(NSError*)processResponseForCheckPassword:(id)data;
-(NSError*)processResponseForLookupAddressByPIN:(id)data;

#ifdef FREE_UPGRADABLE_VERSION
-(void) showAlertOnFirstLoad;
-(void)showUpgradeView:(id)sender;
#endif

@property (readwrite,nonatomic,retain) NSNetServiceBrowser *serviceBrowser;
@property (readwrite,nonatomic,retain) NSMutableArray *servers;
@property (readwrite,nonatomic,retain) NSMutableArray *availableBonjourServers;
@property (readwrite,nonatomic,retain) NSMutableArray *choosableBonjourServers;
@property (readwrite,nonatomic,retain) NSManagedObjectContext *managedObjContext;
@property (readwrite,nonatomic,retain)	IBOutlet UITableView *serversTableView;
@property (readwrite,nonatomic,retain) NSString *selectedServer;
@property (readwrite,nonatomic,retain) UIView *connectingView;
@property (readwrite,nonatomic,retain) UIActivityIndicatorView *connectingIndicator;
@property (readwrite,nonatomic,retain) TTURLRequest *currentRequest;
@property (readwrite,nonatomic,retain) UIView *mainView;
@property (readwrite,nonatomic,retain) IBOutlet UIWebView *webView;

@end
