//
//  FolderEditController.m
//  ImageBankFree
//
//  Created by Yoshi Sugawara on 5/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FolderEditController.h"
#import "Utilities.h"
#import "PrivateFolderPasswordChecker.h"

@implementation FolderEditController

@synthesize customCell, folderName, isPrivate, fullPath, folderNameField;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void) dealloc {
    [folderName release];
    [super dealloc];
}

//-(void)setFullPath:(NSString *)path {
//    self.fullPath = [path retain];
//    
//}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(saveFolder:)] autorelease];
    self.title = NSLocalizedString(@"Edit Folder",@"");
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    // get folder name
    NSArray *parts = [self.fullPath componentsSeparatedByString:@"/"];
    [self setFolderName:[parts objectAtIndex:[parts count]-1]];
    
    // get private option
    [self setIsPrivate:[Utilities isOfflinePrivateFolder:self.fullPath]];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    if ( [[PrivateFolderPasswordChecker sharedInstance] allowOfflineAccess] ) {
        return 2;
    } else {
        return 1;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if ( cell == nil ) {
        if ( indexPath.section == 0 ) {
            NSString *nibName = @"EditableTextTableCell";
            [[NSBundle mainBundle] loadNibNamed:nibName owner:self options:nil];
            cell = customCell;
            customCell = nil;
        } else {
            cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        }
    }
    
    if ( indexPath.section == 1 ) {
        UISwitch *switchview = [[[UISwitch alloc] initWithFrame:CGRectZero] autorelease];
        switchview.tag = 21;
        if ( self.isPrivate ) {
            [switchview setOn:YES];
        }
        cell.accessoryView = switchview;
        cell.textLabel.text = NSLocalizedString(@"Private", @"");
    } else if ( indexPath.section == 0 ) {
        UITextField *textField = (UITextField*) [cell viewWithTag:31];
        textField.text = self.folderName;
    }
    
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     [detailViewController release];
     */
}

-(IBAction)clearTextField:(id)sender {
    folderNameField.text = @"";
    [folderNameField becomeFirstResponder];
}

-(void)saveFolder:(id)sender {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSFileManager *fm = [NSFileManager defaultManager];
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    UITextField *folderField = (UITextField*) [cell viewWithTag:31];
    NSString *newFolderName = [folderField text];
    NSString *newPath = [documentsDirectory stringByAppendingPathComponent:newFolderName];
    
    // validate folder name
    if ( [newFolderName isEqualToString:@""] ) {
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Missing Required Information", @"")
                                                            message:NSLocalizedString(@"A folder name is required.",@"")
                                                           delegate:self
                                                  cancelButtonTitle:@"OK" 
                                                  otherButtonTitles:nil];
        [alertView show];
        [alertView release];
        folderField.text = self.folderName;
        [folderField becomeFirstResponder];
        return;
    }
    
    
    NSError *folderMoveError = nil;
    if ( ![newFolderName isEqualToString:self.folderName] ) {
        [fm moveItemAtPath:self.fullPath toPath:newPath error:&folderMoveError];
        // read model.json and replace references
        NSString *modeljsonfilename = [newPath stringByAppendingPathComponent:@"model.json"];
        NSMutableString *modeljson = [NSMutableString stringWithContentsOfFile:modeljsonfilename encoding:NSUTF8StringEncoding error:&folderMoveError];
        NSString *oldUrl = [NSString stringWithFormat:@"documents://%@",self.folderName];
        NSString *newUrl = [NSString stringWithFormat:@"documents://%@",newFolderName];
        TTDINFO("model.json: %@",modeljson);
        TTDINFO("old url = %@, new url = %@",oldUrl,newUrl);
        [modeljson replaceOccurrencesOfString:oldUrl withString:newUrl options:NSCaseInsensitiveSearch range:NSMakeRange(0, [modeljson length])];
        [modeljson writeToFile:modeljsonfilename atomically:YES encoding:NSUTF8StringEncoding error:&folderMoveError];
    }
    
    // save private setting, if applicable
    if ( [self.tableView numberOfSections] == 2 ) {
        
        cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]];
        UISwitch *switchView = (UISwitch*) [cell viewWithTag:21];
        if ( [switchView isOn] ) {
            [Utilities enableOfflinePrivateFolder:newPath];
        } else {
            [Utilities disableOfflinePrivateFolder:newPath];
        }
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}

@end
