//
//  FolderContentsSearchResultsWorker.m
//  ImageBank
//
//  Created by yoshi on 11/3/12.
//
//

#import "FolderContentsSearchResultsController.h"
#import "Utilities.h"
#import "FolderContentsController.h"

@implementation FolderContentsSearchResultsController

@synthesize parentController, searchDisplayController;

-(void)dealloc {
    [parentController release];
    [searchDisplayController release];
    [super dealloc];
}

-(id) init {
    if ( self = [super init] ) {
        isSearchEnabled = YES;
    }
    return self;
}

#pragma mark
#pragma mark UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)aTableView {
    // Return the number of sections.
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSInteger rowCount = 0;
    if ( resultCount > 0) {
        rowCount = 3;
    } else {
        rowCount = 1;
    }
    
    if (!isSearchEnabled || isSearchIndexRunning || isSearchIndexBuildingFromScratch || isSearchNotAvailable ) {
        rowCount++;
    }
    
    return rowCount;
}

- (void)updateSearchIndexStatusForCell:(UITableViewCell *)cell {
    cell.accessoryType = UITableViewCellAccessoryNone;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.textLabel.lineBreakMode = UILineBreakModeWordWrap;
    cell.detailTextLabel.numberOfLines = 5;
    if ( isSearchNotAvailable ) {
        cell.textLabel.font = [UIFont boldSystemFontOfSize:18];
        cell.textLabel.textColor = [UIColor redColor];
        cell.textLabel.text = NSLocalizedString(@"Search needs version title", @"");
        cell.detailTextLabel.text = NSLocalizedString(@"Search needs version", @"");
    } else if ( !isSearchEnabled ) {
        cell.textLabel.font = [UIFont boldSystemFontOfSize:18];
        cell.textLabel.textColor = [UIColor redColor];
        cell.textLabel.text = NSLocalizedString(@"Search not enabled title",@"");
        cell.detailTextLabel.text = NSLocalizedString(@"Search not enabled",@"");
    } else if ( isSearchIndexRunning ) {
        if ( isSearchIndexBuildingFromScratch ) {
            cell.textLabel.font = [UIFont boldSystemFontOfSize:18];
            cell.textLabel.textColor = [UIColor blueColor];
            cell.textLabel.text = NSLocalizedString(@"Search index rebuild title",@"");
            cell.detailTextLabel.text = NSLocalizedString(@"Search index rebuild",@"");
        } else {
            cell.textLabel.font = [UIFont boldSystemFontOfSize:18];
            cell.textLabel.textColor = [UIColor blueColor];
            cell.textLabel.text = NSLocalizedString(@"Updating search index",@"");
            cell.detailTextLabel.text = NSLocalizedString(@"Search index running",@"");
        }
    }
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if ( cell == nil ) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier] autorelease];
    }
    
    cell.textLabel.font = [UIFont boldSystemFontOfSize:18];
    cell.textLabel.numberOfLines = 1;
    cell.detailTextLabel.numberOfLines = 1;
    cell.textLabel.textColor = [UIColor blackColor];
    
    if ( indexPath.row == 0 ) {
    
        if ( resultCount > 0 ) {
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            cell.selectionStyle = UITableViewCellSelectionStyleBlue;
            cell.textLabel.text = NSLocalizedString(@"Search Result Cell", @"");
            cell.detailTextLabel.text = resultCount == 1 ?
                NSLocalizedString(@"1 image found", @"") :
                [NSString stringWithFormat:NSLocalizedString(@"n images found", @""),resultCount];
        } else {
            cell.textLabel.text = NSLocalizedString(@"No Search Result Found", @"");
            cell.detailTextLabel.text = @"";
            cell.accessoryType = UITableViewCellAccessoryNone;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
    
    }
    
    if ( indexPath.row == 1) {
        
        if (resultCount < 1 && (!isSearchEnabled || isSearchIndexRunning || isSearchIndexBuildingFromScratch || isSearchNotAvailable) ) {
            [self updateSearchIndexStatusForCell:cell];
            
        } else {
            // matches by filename
            cell.textLabel.numberOfLines = 1;
            if ( filenameResultCount < 1) {
                cell.accessoryType = UITableViewCellAccessoryNone;
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
            } else {
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                cell.selectionStyle = UITableViewCellSelectionStyleBlue;
            }
            cell.textLabel.text = [NSString stringWithFormat:NSLocalizedString(@"Search Matches By Filename",@""),filenameResultCount];
            cell.detailTextLabel.text = filenameResultCount == 1 ?
                NSLocalizedString(@"1 image found", @"") :
            [NSString stringWithFormat:NSLocalizedString(@"n images found",@""),filenameResultCount];
            
        }
    }
    
    if ( indexPath.row == 2) {
        // matches by metadata
        if ( metadataResultCount < 1) {
            cell.accessoryType = UITableViewCellAccessoryNone;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        } else {
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            cell.selectionStyle = UITableViewCellSelectionStyleBlue;
        }
        cell.textLabel.text = [NSString stringWithFormat:NSLocalizedString(@"Search Matches By Metadata",@""),metadataResultCount];
        cell.detailTextLabel.text = metadataResultCount == 1 ?
        NSLocalizedString(@"1 image found", @"") :
        [NSString stringWithFormat:NSLocalizedString(@"n images found",@""),metadataResultCount];
    }
    
    if ( indexPath.row == 3) {
        [self updateSearchIndexStatusForCell:cell];
    }
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if ( ((indexPath.row == 1 && resultCount < 1) || indexPath.row == 3) && (!isSearchEnabled || isSearchIndexRunning || isSearchIndexBuildingFromScratch || isSearchNotAvailable) ) {
        return  (44.0 + (5 - 1) * 14.0);
    }
    return 44.0;
}

#pragma mark
#pragma mark UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ( indexPath.row == 0 && resultCount < 1 ) {
        return;
    } else if ( indexPath.row == 1 && filenameResultCount < 1) {
        return;
    } else if ( indexPath.row == 2 && metadataResultCount < 1) {
        return;
    } else if ( indexPath.row == 3 ) {
        return;
    }
 
    NSArray *urlParts = [self.parentController.currentDirectory.url componentsSeparatedByString:@"/"];
    NSString *lastPart = [urlParts lastObject];
    NSArray *queryParams = [lastPart componentsSeparatedByString:@"?"];
    NSString *queryParams2 = [queryParams lastObject];
    NSArray *queryParams3 = [queryParams2 componentsSeparatedByString:@"&"];
    
    NSString *scope = @"all";
    if ( indexPath.row == 1 ) {
        scope = @"filename";
    } else if ( indexPath.row == 2) {
        scope = @"metadata";
    }
    
    NSString *searchResultsUrl = [NSString stringWithFormat:@"http://%@/files/getSearchResults?%@&q=%@&scope=%@",[urlParts objectAtIndex:2],[queryParams3 objectAtIndex:0],[Utilities encodeToPercentEscapeString:self.searchDisplayController.searchBar.text],scope];
    TTDINFO(@"Search results url = %@",searchResultsUrl);
    SearchResultsDirectoryFile *dir = [[SearchResultsDirectoryFile alloc] initWithName:NSLocalizedString(@"Search Results",@"") url:searchResultsUrl filetype:FileTypeDirectory];
    [dir setIsPrivate:self.parentController.currentDirectory.isPrivate];
    
    FolderContentsController *controller = [[FolderContentsController alloc] initWithNavigatorURL:nil query:[NSDictionary dictionaryWithObjectsAndKeys:dir,@"file",self.parentController.currentDirectory,@"parent_directory",self.parentController.managedObjContext,@"managedObjectContext",nil]];
    [self.parentController.navigationController pushViewController:controller animated:YES];
    [controller release];

}

#pragma mark
#pragma mark Search Results Fetching
-(void)doSearchRequest:(NSString*) term {
    NSArray *urlParts = [self.parentController.currentDirectory.url componentsSeparatedByString:@"/"];
    NSString *lastPart = [urlParts lastObject];
    NSArray *queryParams = [lastPart componentsSeparatedByString:@"?"];
    NSString *queryParams2 = [queryParams lastObject];
    NSArray *queryParams3 = [queryParams2 componentsSeparatedByString:@"&"];

    NSString *searchCountUrl = [NSString stringWithFormat:@"http://%@/files/getSearchCount?%@&q=%@",[urlParts objectAtIndex:2],[queryParams3 objectAtIndex:0],[Utilities encodeToPercentEscapeString:term]];
    TTDINFO(@"Search count url = %@",searchCountUrl);
    
    TTURLRequest *request = [TTURLRequest requestWithURL:searchCountUrl delegate:self];
    [request setCachePolicy:TTURLRequestCachePolicyNoCache];
    request.response = self;
    request.httpMethod = @"GET";
    [request send];
}

#pragma mark
#pragma mark TTURLResponse

-(NSError*) request:(TTURLRequest *)request processResponse:(NSHTTPURLResponse *)response data:(id)data {
    NSString *responseBody = [[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] autorelease];
	NSString *responseText = [responseBody stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    TTDINFO(@"search result count result: %@",responseText);
    
    isSearchNotAvailable = [responseText isEqualToString:@""];
    
    if( isSearchNotAvailable) {
        resultCount = 0;
        filenameResultCount = 0;
        metadataResultCount = 0;
        isSearchEnabled = NO;
        isSearchIndexBuildingFromScratch = NO;
        isSearchIndexRunning = NO;
        [self.searchDisplayController.searchResultsTableView reloadData];
        return nil;
    }
    
    NSArray *chunks = [responseText componentsSeparatedByString:@" "];
    resultCount = [(NSString*)[chunks objectAtIndex:0] integerValue];
    filenameResultCount = [(NSString*)[chunks objectAtIndex:1] integerValue];
    metadataResultCount = [(NSString*)[chunks objectAtIndex:2] integerValue];
    isSearchEnabled = [(NSString*)[chunks objectAtIndex:3] boolValue];
    isSearchIndexRunning = [(NSString*)[chunks objectAtIndex:4] boolValue];
    isSearchIndexBuildingFromScratch = [(NSString*)[chunks objectAtIndex:5] boolValue];
    
    [self.searchDisplayController.searchResultsTableView reloadData];
    return nil;
}

#pragma mark
#pragma mark UISearchDisplayDelegate
- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    [self doSearchRequest:searchString];
    return NO;
}

#pragma mark
#pragma mark UISearchBarDelegate
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    
//    if ( resultCount < 1 ) {
//        [searchBar becomeFirstResponder];
//        return;
//    }
    
}

//- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
//    [searchBar resignFirstResponder];
//    [searchBar setShowsCancelButton:NO animated:YES];
//}
//
//- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
//    [searchBar setShowsCancelButton:YES animated:YES];
//}
//
//- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
//    [searchBar setShowsCancelButton:NO animated:YES];
//}

- (void) searchDisplayControllerWillBeginSearch:(UISearchDisplayController *)controller {
    [self.searchDisplayController.searchBar performSelector:@selector(becomeFirstResponder) withObject:nil afterDelay:0.1f];
}

- (void) searchDisplayControllerDidEndSearch:(UISearchDisplayController *)controller {
    [self.searchDisplayController setActive:NO animated:YES];
    TTDINFO(@"did end search!");
}

@end
