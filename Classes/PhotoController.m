//
//  PhotoController.m
//  ImageBank
//
//  Created by yoshi on 5/14/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "PhotoController.h"
#import "PhotoSource.h"
#import "File.h"
#import "PrivateFolderPasswordChecker.h"
#import "PrivatePasswordDialog.h"
#import "TTPhotoViewControllerCategory.h"
#import "InternetConnectionStatusChecker.h"
#import "ImageMetaDataViewController.h"
#import "DownloadURLOperation.h"
#import "NSOperationQueue+SharedQueue.h"
#import "OfflineFolderSaveController.h"
#import "PPRevealSideViewController.h"
#import "Utilities.h"

@implementation PhotoController

@synthesize isFromPrivateFolder, managedObjContext, metaVC, isOfflineFolder, folderContentsModel, parentDirectory, parentVC, delegate;

-(BOOL)prefersStatusBarHidden {
    return YES;
}

-(void)viewDidLoad {
	[super viewDidLoad];
	privatePasswordDialog = [[PrivatePasswordDialog alloc] initWithVC:self];
    [privatePasswordDialog setIsForOfflineFolders:isOfflineFolder];
    CGRect screenFrame = [UIScreen mainScreen].bounds;
    self.revealSideViewController.view.frame = screenFrame;
    self.navigationBarTintColor = [Utilities globalAppTintColor];
}

-(void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
    isSavingOffline = NO;
//    TTDINFO(@"photocontroller Wants full screen? %@",[self wantsFullScreenLayout] ? @"yes" : @"no");
//    TTDINFO(@"nav controller Wants full screen? %@",[self.navigationController wantsFullScreenLayout] ? @"yes" : @"no");
//    TTDINFO(@"side reveal controller wants full screen? %@",[self.revealSideViewController wantsFullScreenLayout] ? @"yes" : @"no");
//    [self.revealSideViewController setWantsFullScreenLayout:YES];

    // move nav bar down
//    CGRect navbarFrame = self.navigationController.navigationBar.frame;
//    self.navigationController.navigationBar.frame = CGRectMake(navbarFrame.origin.x, 20.0f, navbarFrame.size.width, navbarFrame.size.height);
//    [self.navigationController.navigationBar setNeedsLayout];
//    [self.revealSideViewController.view setNeedsLayout];
//    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleBlackTranslucent animated:YES];
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(appGoingIntoBackground:)
												 name:UIApplicationWillResignActiveNotification object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(appGoingIntoForeground:)
												 name:UIApplicationDidBecomeActiveNotification object:nil];
    self.revealSideViewController.panInteractionsWhenClosed = PPRevealSideInteractionNone;
    
    self.revealSideViewController.childControllerForStatusBar = self;
    [self.revealSideViewController setNeedsStatusBarAppearanceUpdate];
}

-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

-(void)viewWillDisappear:(BOOL)animated {
	[super viewWillDisappear:animated];
    self.revealSideViewController.childControllerForStatusBar = nil;
    [self.revealSideViewController setNeedsStatusBarAppearanceUpdate];
	[[NSNotificationCenter defaultCenter] removeObserver:self];
    [[UIApplication sharedApplication] setIdleTimerDisabled:NO];
    // tell the parent controller to set the current photo index so that it can move to it
    if ( self.delegate != nil ) {
        [self.delegate setCurrentPhotoIndex:self.centerPhotoIndex];
    }
    
//    if ( !isSavingOffline ) {
//        [self.revealSideViewController setWantsFullScreenLayout:NO];
//
//    CGRect frame = self.revealSideViewController.view.frame;
//    
//    UIInterfaceOrientation interfaceOrientation = [UIApplication sharedApplication].statusBarOrientation;
//
//    if ( interfaceOrientation == UIInterfaceOrientationPortrait ) {
//        self.revealSideViewController.view.frame = CGRectMake(frame.origin.x,20.0f,frame.size.width,frame.size.height - 20.0f);
//    } else if ( interfaceOrientation == UIInterfaceOrientationLandscapeLeft ) {
//        self.revealSideViewController.view.frame = CGRectMake(20.0f,frame.origin.y,frame.size.width - 20.0f,frame.size.height);
//    } else if ( interfaceOrientation == UIInterfaceOrientationLandscapeRight ) {
////        CGRect screenFrame = [UIScreen mainScreen].bounds;
//        self.revealSideViewController.view.frame = CGRectMake(frame.origin.x,frame.origin.y,frame.size.width -20.0f,frame.size.height);
//    } else if ( interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown ) {
//        CGRect screenFrame = [UIScreen mainScreen].bounds;
//        self.revealSideViewController.view.frame = CGRectMake(frame.origin.x,screenFrame.origin.y,frame.size.width,frame.size.height - 20.0f);
//    }
//    CGRect navbarFrame = self.navigationController.navigationBar.frame;
//    self.navigationController.navigationBar.frame = CGRectMake(navbarFrame.origin.x, 0.0f, navbarFrame.size.width, navbarFrame.size.height);
//    }
}

-(void)appGoingIntoBackground:(id)sender {
    [[PrivateFolderPasswordChecker sharedInstance] setDateWentIntoBackground:[NSDate date]];
    
    [self pauseSlideshow];
	// hide the main view
	self.navigationController.view.alpha = 0.0;
//	[privatePasswordDialog dismissDialog];
}

-(void)appGoingIntoForeground:(id)sender {
    if ( [[PrivateFolderPasswordChecker sharedInstance] shouldCheckAccess] ||
        [privatePasswordDialog isAlertShown]) {
        [privatePasswordDialog dismissDialog];
        if ( isOfflineFolder ) {
            [[PrivateFolderPasswordChecker sharedInstance] setAllowOfflineAccess:NO];
        } else {
            [[PrivateFolderPasswordChecker sharedInstance] setAllowAccess:NO];
        }

        if (self.isFromPrivateFolder) {
            [privatePasswordDialog setIsResuming:YES];
            [privatePasswordDialog showPrivatePasswordDialog];
        } else {
            self.navigationController.view.alpha = 1.0;
        }
    } else {
        [privatePasswordDialog dismissDialog];
        self.navigationController.view.alpha = 1.0;
    }
    
}

-(NSUInteger)supportedInterfaceOrientations {
	if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
		return UIInterfaceOrientationMaskAll;
	} else {
		return UIInterfaceOrientationMaskAllButUpsideDown;
	}
}

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
	if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        return YES;
    }
    return toInterfaceOrientation != UIInterfaceOrientationPortraitUpsideDown;
}


#pragma mark
#pragma mark ProtectedContentDelegate
-(void) doAfterPrivateAccessAllowed {
	self.navigationController.view.alpha = 1.0;	
}

-(UIView*) getView {
	return self.navigationController.view;
}


-(void) doAfterPrivateAccessDeniedOnResume {
    self.navigationController.view.alpha = 1.0;
    [self.navigationController popToRootViewControllerAnimated:NO];
}

#pragma mark
#pragma mark Pre-fetching operation
-(void) preFetchNextImage {
    NSInteger nextIndex = self.centerPhotoIndex + 1;
    if ( nextIndex >= [[self photoSource] numberOfPhotos] ) {
        // we're at the end, don't do anything
        return;
    }
    [self preFetchImageAtIndex:nextIndex];
}

-(void) preFetchPreviousImage {
    NSInteger nextIndex = self.centerPhotoIndex - 1;
    TTDINFO(@"Getting previous image maybe at index %d - numbre of photos = %d",nextIndex,[[self photoSource] numberOfPhotos]);
    if ( nextIndex < 0 ) {
        return;
    }
    [self preFetchImageAtIndex:nextIndex];    
}

-(void) preFetchNextImages:(NSInteger)count {
    for (int i = 1; i<=count; i++) {
        NSInteger nextIndex = self.centerPhotoIndex + i;
        if ( nextIndex >= [[self photoSource] numberOfPhotos] ) {
            // we're at the end, stop
            return;
        }
        [self preFetchImageAtIndex:nextIndex];        
    }
}

-(void) preFetchImageAtIndex:(NSInteger)index {
    id<TTPhoto> nextPhoto = [[self photoSource] photoAtIndex:index];
    TTDINFO(@"prefetching photo at index: %d file: %@",index,[nextPhoto URLForVersion:TTPhotoVersionLarge]);
    TTURLRequest *request = [TTURLRequest requestWithURL:[nextPhoto URLForVersion:TTPhotoVersionLarge] delegate:self];
    request.response = [[[TTURLImageResponse alloc] init] autorelease];
    [request send];
}

- (void)updateChrome {
    [super updateChrome];
    TTDINFO(@"update chrome called: prefetching next image");
    NSInteger numberOfImagesToPrefetch = 5;
    if ( [[InternetConnectionStatusChecker sharedInstance] useLowQualityImages] )
        numberOfImagesToPrefetch = 1;
    [self preFetchNextImages:numberOfImagesToPrefetch];
    TTDINFO(@"update chrome called: prefetching previous image");
    [self preFetchPreviousImage];
    [[TTURLCache sharedCache] logMemoryUsage];
    
    // update the metaimage view controlller if it exists
    if ( self.metaVC != nil ) {
        [self.metaVC setMetadataUrl:[(Photo*)_centerPhoto getMetadataUrl]];
        [self.metaVC loadMetadataContent];
    }
}

- (void)requestDidFinishLoad:(TTURLRequest*)request {
    // refresh images in scroll view
    [self loadImages];
}

-(void) getMetadata {
    
    if ( self.metaVC == nil ) {
        NSString *nibName = @"ImageMetaDataView";
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
            nibName = @"ImageMetaDataView-iPad";
        
        [self setMetaVC:[[[ImageMetaDataViewController alloc] initWithNibName:nibName bundle:nil] autorelease]];
        NSString *url = [(Photo*)_centerPhoto getMetadataUrl];
        TTDINFO(@"metadata url: %@",url);
        [self.metaVC setMetadataUrl:url];
        
        CGPoint innerViewCenter = _innerView.center;
        innerViewCenter.y = innerViewCenter.y + 100;
        self.metaVC.view.center = innerViewCenter;
        
        self.metaVC.view.transform = CGAffineTransformMakeScale(0.1, 0.1);
        
        [_innerView addSubview:self.metaVC.view];
        [_innerView bringSubviewToFront:self.metaVC.view];
        
        
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.15];
        self.metaVC.view.transform = CGAffineTransformIdentity;
        self.metaVC.view.transform = CGAffineTransformMakeScale(1.2, 1.2);
        self.metaVC.view.transform = CGAffineTransformIdentity;
       	[UIView commitAnimations];

    } else {
        if ( self.metaVC.view.hidden ) {
            self.metaVC.view.transform = CGAffineTransformMakeScale(0.1, 0.1);
            self.metaVC.view.hidden = NO;
            [UIView beginAnimations:nil context:NULL];
            [UIView setAnimationDuration:0.15];
            self.metaVC.view.transform = CGAffineTransformIdentity;
            self.metaVC.view.transform = CGAffineTransformMakeScale(1.2, 1.2);
            self.metaVC.view.transform = CGAffineTransformIdentity;
            [UIView commitAnimations];           
        } else {
            [UIView animateWithDuration:0.15 
                             animations:^{
                                 self.metaVC.view.transform = CGAffineTransformMakeScale(0.1, 0.1);
                             }
                             completion:^ (BOOL finished) {
                                 if (finished) {
                                     self.metaVC.view.hidden = YES;
                                 }                                 
                             }];
        }
    }
}

-(void)saveToCameraRoll:(id)sender {

    UIActionSheet *actionSheet = nil;
    if ( !isOfflineFolder) {
        actionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Select where to save your photos to.",@"Select where to save your photos to.")
															 delegate:self
													cancelButtonTitle:NSLocalizedString(@"Cancel",@"Cancel")
											   destructiveButtonTitle:nil
													otherButtonTitles:
                                                        NSLocalizedString(@"Saved Folders",@"Saved Folders"),NSLocalizedString(@"Camera Roll",@"Camera Roll"),nil];
    } else {
        actionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Select where to save your photos to.",@"Select where to save your photos to.")
                                                  delegate:self
                                         cancelButtonTitle:NSLocalizedString(@"Cancel",@"Cancel")
                                    destructiveButtonTitle:nil
                                         otherButtonTitles:NSLocalizedString(@"Camera Roll",@"Camera Roll"),nil];
        
    }
	[actionSheet showInView:self.view];
	[actionSheet release];
}

- (void)actionSheet:(UIActionSheet *)modalView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if ( !isOfflineFolder && buttonIndex == 0 ) {
        // modify the folder contents model with a new photo source containing only this photo
        [_centerPhoto setIsSelected:YES];
        PhotoSource *ps = [[[PhotoSource alloc] initWithPhoto:_centerPhoto] autorelease];
        [self.folderContentsModel setPhotosModel:ps];
        
        OfflineFolderSaveController *saveController = [[OfflineFolderSaveController alloc] initWithStyle:UITableViewStylePlain];
        [saveController setParentVC:self.parentVC];
        [saveController setFolderContentsModel:self.folderContentsModel];
        [saveController setParentDirectory:self.parentDirectory];
        [saveController setIsSavingFromPhotoController:YES];
        
        UINavigationController *aNavController = [[UINavigationController alloc] initWithRootViewController:saveController];
        aNavController.toolbarHidden = NO;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
            aNavController.modalPresentationStyle = UIModalPresentationFormSheet;
        isSavingOffline = YES;
        [self presentModalViewController:aNavController animated:YES];
        [saveController release];
        [aNavController release];
    } else if ( (!isOfflineFolder && buttonIndex == 1) || (isOfflineFolder && buttonIndex == 0) ) {
        NSString *photoUrl = [_centerPhoto URLForVersion:TTPhotoVersionLarge];
        // documents:// are for offline photos
        BOOL isOffline = [photoUrl hasPrefix:@"documents://"];
        
        if ( isOffline ) {
            // save file directly
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
            NSString *imagePath = [documentsDirectory stringByAppendingPathComponent:[photoUrl substringFromIndex:12]];
            UIImage *image = [[[UIImage alloc] initWithContentsOfFile:imagePath] autorelease];
            UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
        } else {
            NSURL *aUrl  =  [NSURL URLWithString:[[NSUserDefaults standardUserDefaults] boolForKey:@"pref_save_full_size"] ? [NSString stringWithFormat:@"%@&noresize=1",photoUrl] : photoUrl];
            DownloadURLOperation *download = [[DownloadURLOperation alloc]
                                              initWithURL:aUrl
                                              withOrientation:self.interfaceOrientation];
            [[NSOperationQueue sharedOperationQueue] addOperation:download];
        }
    }
    
}

//- (BOOL)isShowingChrome {
//    UINavigationBar* bar = self.navigationController.navigationBar;
//    return bar ? bar.alpha != 0 : 1;
//}
//
//- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
//{
//    TTDINFO(@"photo view controller will rotate - showing chrome? %@",[self isShowingChrome] ? @"yes" : @"no");
//    if (![self isShowingChrome])
//    {
//        self.statusBarStyle = UIStatusBarStyleBlackTranslucent;
//        [[UIApplication sharedApplication] setStatusBarHidden:NO];
//    }
//    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
//}
//
//- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
//{
//    TTDINFO(@"photo view controller will animate rotate - showing chrome? %@",[self isShowingChrome] ? @"yes" : @"no");
//    if (![self isShowingChrome])
//    {
//                self.statusBarStyle = UIStatusBarStyleBlackTranslucent;
//        [[UIApplication sharedApplication] setStatusBarHidden:YES];
//    }
//    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
//}


-(void)dealloc {
	[self.managedObjContext release];
	[privatePasswordDialog release];
    metaVC = nil;
    [metaVC release];
    [folderContentsModel release];
    [parentDirectory release];
    [parentVC release];
	[[NSNotificationCenter defaultCenter] removeObserver:self];
	[super dealloc];
}

@end
