//
//  ImageMetaDataViewControllerViewController.m
//  ImageBankFree
//
//  Created by Yoshi Sugawara on 4/26/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ImageMetaDataViewController.h"
#import "AutoScrollLabel.h"
#import "JSON.h"

@implementation ImageMetaDataViewController

@synthesize tableView, activityIndicator, loadingLabel, metadataUrl, customCell, metadataDataSource;


- (void)viewDidLoad
{
    [super viewDidLoad];

    self.loadingLabel.text = NSLocalizedString(@"Loading...", @"Loading...");
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    self.tableView = nil;
    self.activityIndicator = nil;
    self.loadingLabel = nil;
    self.metadataUrl = nil;
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self loadMetadataContent];
}

-(void) loadMetadataContent {
    self.loadingLabel.text = NSLocalizedString(@"Loading...", @"Loading...");
    self.activityIndicator.hidden = NO;
    [self.activityIndicator startAnimating];
    self.loadingLabel.hidden = NO;
    self.tableView.hidden = YES;
    
    TTDINFO(@"metadata url: %@",metadataUrl);
    
    TTURLRequest *request = [TTURLRequest requestWithURL:metadataUrl delegate:self];
	request.response = self;
	request.httpMethod = @"GET";
	[request send];    
}

#pragma mark
#pragma mark TTURLRequestDelegate
- (void)request:(TTURLRequest*)request didFailLoadWithError:(NSError*)error {
    self.activityIndicator.hidden = YES;
    if ( [error code] == NSURLErrorBadURL ) {
        [self.loadingLabel setFont:[UIFont systemFontOfSize:12]];
        self.loadingLabel.text = NSLocalizedString(@"Sorry, PhotoStreamr Home 2.2 or higher is required for viewing EXIF data. Please upgrade to the latest version.",@"PhotoStreamr Home 2.2 or higher is required for viewing EXIF data.");
    } else {
        self.loadingLabel.text = NSLocalizedString(@"EXIF data not found.",@"EXIF data not found.");
    }
    self.tableView.hidden = YES;
}

#pragma mark
#pragma mark TTURLResponse
- (NSError*)request:(TTURLRequest*)request processResponse:(NSHTTPURLResponse*)response data:(id)data {
    NSString *responseBody = [[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] autorelease];
    TTDINFO(@"metadata response body: %@",responseBody);
    SBJSON *parser = [[SBJSON alloc] init];
    NSDictionary *metadata = [parser objectWithString:responseBody];
    [self setMetadataDataSource:metadata];
    [parser release];
    
    if ( metadata == nil || [metadata count] < 1 ) {
        self.activityIndicator.hidden = YES;
        self.loadingLabel.text = NSLocalizedString(@"EXIF data not found.",@"EXIF data not found.");
        self.tableView.hidden = YES;
        return nil;
    }

    self.activityIndicator.hidden = YES;
    self.loadingLabel.hidden = YES;
    self.tableView.hidden = NO;

    [tableView reloadData];
    
    return nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if ( metadataDataSource != nil ) {
        return [metadataDataSource count];
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableview cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableview dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        NSString *nibName = @"ImageMetaViewTableCell";
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
            nibName = @"ImageMetaViewTableCell-iPad";
        [[NSBundle mainBundle] loadNibNamed:nibName owner:self options:nil];
        cell = customCell;
        self.customCell = nil;
    }
    
    NSArray *keys = [[metadataDataSource allKeys] sortedArrayUsingSelector: @selector(localizedCaseInsensitiveCompare:)];;
    NSString *currentKey = (NSString*) [keys objectAtIndex:indexPath.row];
    // Configure the cell...
    UILabel *label;
    label = (UILabel*) [cell viewWithTag:210];
    label.text = currentKey;

    float fontSize = 10.0;
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
        fontSize = 14.0;
    
    AutoScrollLabel *description;
    description = (AutoScrollLabel*) [cell viewWithTag:211];
    [description setFont:[UIFont systemFontOfSize:fontSize]];
    [description setTextColor:[UIColor whiteColor]];
    [description setText:(NSString*) [metadataDataSource objectForKey:currentKey]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView
  willDisplayCell:(UITableViewCell *)cell
forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [cell setBackgroundColor:[UIColor clearColor]];
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     [detailViewController release];
     */
}

@end
