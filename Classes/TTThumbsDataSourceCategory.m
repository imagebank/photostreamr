//
//  TTThumbsDataSourceCategory.m
//  ImageBankFree
//
//  Created by yoshi on 11/27/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "TTThumbsDataSourceCategory.h"

static CGFloat kThumbSize = 75;
static CGFloat kThumbSpacing = 4;
static CGFloat kThumbSizeForIPad = 187;

@implementation TTThumbsDataSource(Category)

- (NSInteger)columnCountForView:(UIView *)view {
	CGFloat width = TTScreenBounds().size.width;
	CGFloat thumbSize = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) ? kThumbSizeForIPad : kThumbSize;
	if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad && width >= 1024 ) {
		thumbSize = 194;
	}
	return round((width - kThumbSpacing*2) / (thumbSize+kThumbSpacing));
}

@end
