//
//  TTPhotoViewControllerCategory.m
//  ImageBank
//
//  Created by yoshi on 5/17/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "TTPhotoViewControllerCategory.h"
#import "PhotoSource.h"
#import "MovableViewWithInsetView.h"

@implementation TTPhotoViewController(Category)

-(void)initWithPhotoSource:(id <TTPhotoSource>)photoSource atIndex:(NSInteger)photoIndex {
	if ( self = [self initWithPhotoSource:photoSource] ) {
		_centerPhotoIndex = photoIndex;
        [self.navigationController.view setAutoresizesSubviews:NO];
	}
}

- (void)loadView {
	CGRect screenFrame = [UIScreen mainScreen].bounds;
	self.view = [[[UIView alloc] initWithFrame:screenFrame] autorelease];
	
	CGRect innerFrame = CGRectMake(0, 0,
								   screenFrame.size.width, screenFrame.size.height);
	_innerView = [[UIView alloc] initWithFrame:innerFrame];
	_innerView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
	[self.view addSubview:_innerView];
	
	_scrollView = [[TTScrollView alloc] initWithFrame:screenFrame];
	_scrollView.delegate = self;
	_scrollView.dataSource = self;
	_scrollView.backgroundColor = [UIColor blackColor];
	_scrollView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
	[_innerView addSubview:_scrollView];
	
	_nextButton = [[UIBarButtonItem alloc] initWithImage:
				   TTIMAGE(@"bundle://Three20.bundle/images/nextIcon.png")
												   style:UIBarButtonItemStylePlain target:self action:@selector(nextAction)];
	_previousButton = [[UIBarButtonItem alloc] initWithImage:
					   TTIMAGE(@"bundle://Three20.bundle/images/previousIcon.png")
													   style:UIBarButtonItemStylePlain target:self action:@selector(previousAction)];
	
	UIBarButtonItem* playButton = [[[UIBarButtonItem alloc] initWithBarButtonSystemItem:
									UIBarButtonSystemItemPlay target:self action:@selector(playAction)] autorelease];
	playButton.tag = 1;
	
	UIBarItem* space = [[[UIBarButtonItem alloc] initWithBarButtonSystemItem:
						 UIBarButtonSystemItemFlexibleSpace target:nil action:nil] autorelease];
	
	_toolbar = [[UIToolbar alloc] initWithFrame:
				CGRectMake(0, screenFrame.size.height - TT_ROW_HEIGHT,
						   screenFrame.size.width, TT_ROW_HEIGHT)];
	if (self.navigationBarStyle == UIBarStyleDefault) {
		_toolbar.tintColor = TTSTYLEVAR(toolbarTintColor);
	}
	
    UIBarItem *infoButton = [[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"infoButton.png"] style:UIBarButtonItemStylePlain target:self action:@selector(getMetadata)] autorelease];
    
    if ( ![_centerPhoto respondsToSelector:@selector(getMetadataUrl)] ) {
        infoButton = space;
    }
    
	UIBarButtonItem *addToCameraRollButton = [[[UIBarButtonItem alloc] 
											   initWithBarButtonSystemItem:UIBarButtonSystemItemAction 
											   target:self action:@selector(saveToCameraRoll:)] autorelease];
	self.navigationItem.rightBarButtonItem = addToCameraRollButton;
	
	_toolbar.barStyle = self.navigationBarStyle;
	_toolbar.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleTopMargin;
	_toolbar.items = [NSArray arrayWithObjects:
					  infoButton, space, _previousButton, space, playButton, space, _nextButton, space, space,nil];
	[_innerView addSubview:_toolbar];
}

- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
	[self.navigationController setToolbarHidden:YES];
	[self updateToolbarWithOrientation:self.interfaceOrientation];
}

- (void)playAction {
	NSTimeInterval slideshowInterval = [[[NSUserDefaults standardUserDefaults] objectForKey:@"pref_slideshow_duration"] intValue];
	if (!_slideshowTimer) {
		UIBarButtonItem* pauseButton = [[[UIBarButtonItem alloc] initWithBarButtonSystemItem:
										 UIBarButtonSystemItemPause target:self action:@selector(pauseAction)] autorelease];
		pauseButton.tag = 1;		
		[_toolbar replaceItemWithTag:1 withItem:pauseButton];		
		_slideshowTimer = [NSTimer scheduledTimerWithTimeInterval:slideshowInterval
														   target:self selector:@selector(slideshowTimer) userInfo:nil repeats:YES];
	}
    [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
}

- (void)pauseAction {
    if (_slideshowTimer) {
        UIBarButtonItem* playButton =
        [[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemPlay
                                                       target:self
                                                       action:@selector(playAction)]
         autorelease];
        playButton.tag = 1;
        
        [_toolbar replaceItemWithTag:1 withItem:playButton];
        
        [_slideshowTimer invalidate];
        _slideshowTimer = nil;
    }
    [[UIApplication sharedApplication] setIdleTimerDisabled:NO];
}


- (void) pauseSlideshow {
    [self pauseAction];
}

@end
