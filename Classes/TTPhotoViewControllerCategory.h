//
//  TTPhotoViewControllerCategory.h
//  ImageBank
//
//  Created by yoshi on 5/17/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Three20/Three20.h>
#import "ImageMetaDataViewController.h"

@interface TTPhotoViewController(Category)

-(void)initWithPhotoSource:(id <TTPhotoSource>)photoSource atIndex:(NSInteger)photoIndex;
- (void)loadImages;
- (void) pauseSlideshow;
-(void)saveToCameraRoll:(id)sender;
-(void) updateChrome;

@end
