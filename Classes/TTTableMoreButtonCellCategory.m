//
//  TTTableMoreButtonCellCategory.m
//  ImageBankFree
//
//  Created by yoshi on 8/29/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "TTTableMoreButtonCellCategory.h"


@implementation TTTableMoreButtonCell(Category)

- (void)setAnimating:(BOOL)animating {
	if (_animating != animating) {
		_animating = animating;
		
		if (_animating) {
			[self.activityIndicatorView startAnimating];
			
		} else {
			[_activityIndicatorView stopAnimating];
			self.userInteractionEnabled = YES;
		}
		
		[self setNeedsLayout];
	}
}

@end
