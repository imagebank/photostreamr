//
//  Stylesheet.m
//  ImageBank
//
//  Created by yoshi on 5/29/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "Stylesheet.h"
#import "Utilities.h"


@implementation Stylesheet

- (UIFont*)tableFont {
	return [UIFont fontWithName:@"HelveticaNeue-Light" size:16];
}

- (UIColor*)navigationBarTintColor {
	return [UINavigationBar appearance].barTintColor;
}

- (UIColor*) toolbarTintColor {
	return [UIToolbar appearance].barTintColor;
}

- (UIColor*) tablePlainBackgroundColor {
    return [UIColor clearColor];
}

-(UIColor*) moreLinkTextColor {
    return [Utilities globalAppTintColor];
}

@end
