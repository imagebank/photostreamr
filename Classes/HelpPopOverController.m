//
//  HelpPopOverController.m
//  ImageBankFree
//
//  Created by Yoshi Sugawara on 2/5/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "HelpPopOverController.h"
#import "Crittercism.h"
#import "Utilities.h"
#import "PPRevealSideViewController.h"
#import "PhotoSortOrderTableCell.h"
#import "UVConfig.h"
#import "UserVoice.h"

#ifdef FREE_UPGRADABLE_VERSION
#import "MKStoreManager.h"
#import "BuyUpgradeController.h"
#endif

@implementation HelpPopOverController

@synthesize delegate, isFromOfflineFoldersController;

#pragma mark -
#pragma mark Initialization

- (id)initWithStyle:(UITableViewStyle)style {
    // Override initWithStyle: if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
    if ((self = [super initWithStyle:style])) {
//		self.contentSizeForViewInPopover = CGSizeMake(240, 2 * 44 - 1);
    }
    return self;
}

#pragma mark -
#pragma mark View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
	self.tableView.rowHeight = 44.0;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
	self.tableView.backgroundColor = [UIColor whiteColor];
    
    UIColor* color = [[UIColor redColor] retain];
    CGColorRef colorref = [color CGColor];
    int numComponents = CGColorGetNumberOfComponents(colorref);
    
    if (numComponents == 4) {
        const CGFloat *components = CGColorGetComponents(colorref);
        CGFloat red     = components[0];
        CGFloat green = components[1];
        CGFloat blue   = components[2];
    }
    [color release];

    NSBundle *bundle = [NSBundle mainBundle];
    NSString *nibPath = [[NSBundle mainBundle] pathForResource:@"PhotoSortOrderTableCell" ofType:@"nib"];
            TTDINFO(@"nib path = %@",nibPath);
    if ( nibPath == nil ) {
        // load english
        NSError *error;
        NSArray *dirContents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:[[NSBundle mainBundle] pathForResource:@"en" ofType:@"lproj"] error:&error];
        bundle = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:@"en" ofType:@"lproj"]];
        TTDINFO(@"bundle = %@",bundle);
        nibPath = [bundle pathForResource:@"PhotoSortOrderTableCell" ofType:@"nib"];
        TTDINFO(@"nib path = %@",nibPath);
    }
    

    [self.tableView registerNib:[UINib nibWithNibName:@"PhotoSortOrderTableCell" bundle:bundle] forCellReuseIdentifier:@"PhotoSortOrderCell"];
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self sortOrderChanged:nil];
    [self.tableView setContentInset:UIEdgeInsetsMake(20.0f, 0.0f, 0.0f, 0.0f)];
}

/*
 - (void)viewDidAppear:(BOOL)animated {
 [super viewDidAppear:animated];
 }
 */
/*
 - (void)viewWillDisappear:(BOOL)animated {
 [super viewWillDisappear:animated];
 }
 */
/*
 - (void)viewDidDisappear:(BOOL)animated {
 [super viewDidDisappear:animated];
 }
 */
/*
 // Override to allow orientations other than the default portrait orientation.
 - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
 // Return YES for supported orientations
 return (interfaceOrientation == UIInterfaceOrientationPortrait);
 }
 */


#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 4;
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    BOOL isPhotoSortOrderCell = indexPath.row == 3;
    NSString *cellIdentifier = isPhotoSortOrderCell ? @"PhotoSortOrderCell" : @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier] autorelease];
    }

    if ( isPhotoSortOrderCell) {
        
        PhotoSortOrderTableCell *sortOrderTableCell = (PhotoSortOrderTableCell*) cell;
        NSDictionary *sortOptions = [Utilities folderSortOptions];
        NSString *name = [sortOptions objectForKey:@"sort_by_field"];
        NSString *isAscValue = [sortOptions objectForKey:@"is_asc"];
        
        sortOrderTableCell.thumbImage.image = [sortOrderTableCell.thumbImage.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        sortOrderTableCell.thumbImage.tintColor = [Utilities globalAppTintColor];
        sortOrderTableCell.displayOrderingLabel.textColor = [Utilities globalAppTintColor];
        
        sortOrderTableCell.sortByFieldControl.selectedSegmentIndex = [name isEqualToString:@"name"] ? 0 : 1;
        sortOrderTableCell.sortOrderControl.selectedSegmentIndex = [isAscValue isEqualToString:@"yes"] ? 0 : 1;

    } else {
    
        cell.textLabel.textColor = [Utilities globalAppTintColor];
        cell.backgroundColor = [UIColor clearColor];
        cell.imageView.tintColor = [Utilities globalAppTintColor];
        
        if ( indexPath.row == 0 ) {
            cell.imageView.image = [[UIImage imageNamed:@"home-icon.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            cell.textLabel.text = NSLocalizedString(@"Home",@"Home");
        } else if ( indexPath.row == 1 ) {
            cell.imageView.image = [[UIImage imageNamed:@"gear.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            cell.textLabel.text = NSLocalizedString(@"Settings", @"Settings");
        } else if ( indexPath.row == 2 ) {
            cell.imageView.image = [[UIImage imageNamed:@"chats.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            cell.textLabel.text = NSLocalizedString(@"Support Forum",@"Support Forum");
        }
    }
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if ( indexPath.row == 3 ) {
        return 211.0f;
    }
    return 44.0f;
}

#pragma mark - 
#pragma mark UI Actions


-(void)crittercismPressed:(id) sender {
    UVConfig *config = [UVConfig configWithSite:@"photostreamr.uservoice.com"
                                         andKey:@"6It4gMMaL3E1yIdua2JPQ"
                                      andSecret:@"otETCV5WCPypGWC8OWYILyZRH3FS44eqL1jSMMIPFmU"];
    [UserVoice presentUserVoiceInterfaceForParentViewController:self andConfig:config];
//    [Crittercism showCrittercism:(UIViewController*) delegate];
}

-(IBAction)sortOrderChanged:(id) sender {
//    NSString *sortBy = self.sortByFieldControl.selectedSegmentIndex == 1 ? @"date" : @"name";
//    BOOL sortAscending = self.sortOrderControl.selectedSegmentIndex == 0;
//    [delegate didChangeSortByField:sortBy isAscending:sortAscending];
}

#ifdef FREE_UPGRADABLE_VERSION
#pragma mark
#pragma mark UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
	if ( buttonIndex == 0 ) {
		[alertView dismissWithClickedButtonIndex:0 animated:YES];
	} else {
		[alertView dismissWithClickedButtonIndex:0 animated:YES];
		[delegate showUpgradeViewController];
    }
}
#endif


#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self retain];
    if ( indexPath.row == 0 ) {
        [delegate popToRootController];
        [self.revealSideViewController popViewControllerAnimated:YES];
    } else if ( indexPath.row == 1 ) {
        [delegate showSettingsController];
    } else if ( indexPath.row == 2 ) {

#ifdef FREE_UPGRADABLE_VERSION
        // selected to add remote server
        if ( ![MKStoreManager fullVersionUpgradePurchased] ) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Requires Full Version"
                                                                                      ,@"Requires Full Version")
                                                            message:NSLocalizedString(@"Upgrade for Support",@"Upgrade for Remote Server")
                                                           delegate:self cancelButtonTitle:NSLocalizedString(@"Not Now", @"Not Now") otherButtonTitles:NSLocalizedString(@"Upgrade",@"Upgrade to full version button"),nil];
            [alert show];
            [alert release];
            return;
        }
#endif

        [self crittercismPressed:tableView];
    } 
}

#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    // Relinquish ownership of anything that can be recreated in viewDidLoad or on demand.
    // For example: self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}


@end

