//
//  PhotoSortOrderTableCell.m
//  ImageBank
//
//  Created by yoshi on 9/16/12.
//
//

#import "PhotoSortOrderTableCell.h"
#import "Utilities.h"

@implementation PhotoSortOrderTableCell

@synthesize sortByFieldControl,sortOrderControl, thumbImage, displayOrderingLabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self.thumbImage.image = [self.thumbImage.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(IBAction)sortOrderChanged:(id) sender {
    NSLog(@"sort order changed!");
    NSString *sortBy = self.sortByFieldControl.selectedSegmentIndex == 1 ? @"date" : @"name";
    BOOL sortAscending = self.sortOrderControl.selectedSegmentIndex == 0;
    [Utilities storeFolderSortOptionsWithField:sortBy isAscending:sortAscending];
}


-(void)dealloc {
    [sortOrderControl release];
    [sortByFieldControl release];
    [displayOrderingLabel release];
    [thumbImage release];
    [super dealloc];
}

@end
