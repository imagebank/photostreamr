//
//  PhotoSortOrderTableCell.h
//  ImageBank
//
//  Created by yoshi on 9/16/12.
//
//

#import <UIKit/UIKit.h>

@interface PhotoSortOrderTableCell : UITableViewCell {
    IBOutlet UIImageView *thumbImage;
    IBOutlet UILabel *displayOrderingLabel;
    IBOutlet UISegmentedControl *sortByFieldControl;
    IBOutlet UISegmentedControl *sortOrderControl;
}

@property (nonatomic,retain) IBOutlet UILabel *displayOrderingLabel;
@property (nonatomic,retain) IBOutlet UIImageView *thumbImage;
@property (nonatomic,retain) IBOutlet UISegmentedControl *sortByFieldControl;
@property (nonatomic,retain) IBOutlet UISegmentedControl *sortOrderControl;

-(IBAction)sortOrderChanged:(id) sender;

@end
