//
//  ImageBankAppDelegate.m
//  PhotoStreamr
//
//  Created by yoshi on 5/12/10.
//  Copyright __MyCompanyName__ 2010. All rights reserved.
//

#import "AppDelegate.h"
#import "PhotoController.h"
#import "ServerBrowserViewController.h"
#import "ChooseServerController.h"
#import "FolderContentsController.h"
#import "AddServerController.h"
#import "Stylesheet.h"
#import "Utilities.h"
#import "FileUploadNotifier.h"
#import "HTMLHelpController.h"
#import "InternetConnectionStatusChecker.h"

#import "PPRevealSideViewController.h"
#import "UIDeviceHardware.h"

#ifdef FREE_UPGRADABLE_VERSION
#import "MKStoreManager.h"
#endif

#import "Crittercism.h"


#define kStoreType      NSSQLiteStoreType
#define kStoreFilename  @"db.sqlite"


///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
@interface AppDelegate()
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator;
- (NSString *)applicationDocumentsDirectory;

@end


///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
@implementation AppDelegate


///////////////////////////////////////////////////////////////////////////////////////////////////
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {	
	// Forcefully removes the model db and recreates it.
	//_resetModel = YES;

#ifdef FREE_UPGRADABLE_VERSION
	// Init store manager
	[MKStoreManager sharedManager];
	MKStoreObserver *observer = [[MKStoreObserver alloc] init];
	[[SKPaymentQueue defaultQueue] addTransactionObserver:observer];
#endif

    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];

	TTNavigator* navigator = [TTNavigator navigator];
	navigator.persistenceMode = TTNavigatorPersistenceModeNone;
    
    // Set stylesheet
    [TTStyleSheet setGlobalStyleSheet:[[[Stylesheet alloc]
										init] autorelease]];
	
	// Unlimited file size
	[[TTURLRequestQueue mainQueue] setMaxContentLength:0]; 
	
    // max cache size: 20 images of size 1024x1024
    [[TTURLCache sharedCache] setMaxPixelCount:(1024*1024*20)];
    
	// Flush cache for now
	[[TTURLCache sharedCache] removeAll:YES];
	
	// Set default prefs if needed
	[self setDefaultPreferences];
	
	[[FileUploadNotifier sharedInstance] retain];
    
    [[InternetConnectionStatusChecker sharedInstance] retain];
	
	TTURLMap* map = navigator.URLMap;
	
	// Adding the ? indicates that there is a query dictionary being passed to initWithNavigatorURL:URL:query
	[map from:@"tt://servers" toViewController:[ServerBrowserViewController class]];
	[map from:@"tt://photos?" toViewController:[PhotoController class]];
	[map from:@"tt://chooseserver?" toViewController:[ChooseServerController class]];
	[map from:@"tt://folderview?" toViewController:[FolderContentsController class]];
	[map from:@"tt://addservermanually" toViewController:[AddServerController class]];
    [map from:@"tt://htmlhelp?" toViewController:[HTMLHelpController class]];
	
	[self.window makeKeyAndVisible];
	
//    splashView = [[UIImageView alloc] initWithFrame:[self.window frame]];
//    splashView.image = [UIImage imageNamed:[[UIDeviceHardware platformString] isEqualToString:@"iPhone 5"] || [[UIDeviceHardware platformString] isEqualToString:@"Simulator"] ? @"Default-568h@2x.png" : @"Default.png"];
//    [self.window addSubview:splashView];
//    [self.window bringSubviewToFront:splashView];
    
    ChooseServerController *rootController = [[ChooseServerController alloc] initWithNavigatorURL:nil query:[NSDictionary dictionaryWithObjectsAndKeys:self.managedObjectContext,@"managedObjectContext",nil]];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:rootController];
    PPRevealSideViewController *sideController = [[PPRevealSideViewController alloc] initWithRootViewController:nav];
    [sideController setDirectionsToShowBounce:PPRevealSideDirectionLeft];
    sideController.panInteractionsWhenClosed = PPRevealSideInteractionNavigationBar |PPRevealSideInteractionContentView;
    
    [self.window setRootViewController:sideController];
    [rootController release];
    [nav release];

    
//    [UIView animateWithDuration:0.5 animations:^{
//        splashView.transform = CGAffineTransformMakeScale(2.0, 2.0);
//        splashView.alpha = 0.0;
//    }
//                     completion:(void (^)(BOOL)) ^{
//                         ChooseServerController *rootController = [[ChooseServerController alloc] initWithNavigatorURL:nil query:[NSDictionary dictionaryWithObjectsAndKeys:self.managedObjectContext,@"managedObjectContext",nil]];
//                         UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:rootController];
//                         PPRevealSideViewController *sideController = [[PPRevealSideViewController alloc] initWithRootViewController:nav];
//                         [sideController setDirectionsToShowBounce:PPRevealSideDirectionLeft];
//                         sideController.panInteractionsWhenClosed = PPRevealSideInteractionNavigationBar |PPRevealSideInteractionContentView;
//                         
//                         [self.window setRootViewController:sideController];
//                         [rootController release];
//                         [nav release];
//                         [splashView removeFromSuperview];
//                     }
//     ];
//
//    
//	if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
//		[self startupAnimationDone];
//	} else {
//		splashView = [[UIImageView alloc] initWithFrame:[self.window frame]];
//		splashView.image = [UIImage imageNamed:@"Default.png"];
//		[self.window addSubview:splashView];
//		[self.window bringSubviewToFront:splashView];
//        
//		[UIView beginAnimations:nil context:nil];
//		[UIView setAnimationDuration:0.8];
//		[UIView setAnimationTransition:UIViewAnimationTransitionNone forView:self.window cache:YES];
//		[UIView setAnimationDelegate:self];
//		[UIView setAnimationDidStopSelector:@selector(startupAnimationDone)];
//		splashView.alpha = 0.0;
//	
//		if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
//			splashView.frame = CGRectMake(-120, -120, 968, 1224);
//		} else {
//			splashView.frame = CGRectMake(-60, -60, 440, 600);
//		}
//		[UIView commitAnimations];
//	}
    
#ifndef DEBUG
    // Crittercism init
    [Crittercism enableWithAppID:@"4f26cc2ab09315289d0001ba"];
#endif
    
    [UINavigationBar appearance].barTintColor = [UIColor whiteColor];
    [UIToolbar appearance].barTintColor = [UIColor whiteColor];
    [UINavigationBar appearance].tintColor = [Utilities globalAppTintColor];
    [UIToolbar appearance].tintColor = [Utilities globalAppTintColor];
    [UIButton appearance].tintColor = [Utilities globalAppTintColor];
    [UISegmentedControl appearance].tintColor = [Utilities globalAppTintColor];

    [[UINavigationBar appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObject:[Utilities globalAppTintColor] forKey:NSForegroundColorAttributeName]];
    
	return YES;
}

-(void)startupAnimationDone {
    ChooseServerController *rootController = [[ChooseServerController alloc] initWithNavigatorURL:nil query:[NSDictionary dictionaryWithObjectsAndKeys:self.managedObjectContext,@"managedObjectContext",nil]];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:rootController];
    PPRevealSideViewController *sideController = [[PPRevealSideViewController alloc] initWithRootViewController:nav];
    [sideController setDirectionsToShowBounce:PPRevealSideDirectionLeft];
    sideController.panInteractionsWhenClosed = PPRevealSideInteractionNavigationBar |PPRevealSideInteractionContentView;

    [self.window setRootViewController:sideController];
    [rootController release];
    [nav release];
//	TTNavigator* navigator = [TTNavigator navigator];
//	if (![navigator restoreViewControllers]) {
//		
//		TTURLAction *action = [TTURLAction actionWithURLPath:@"tt://chooseserver?"];
//		[action applyQuery:[NSDictionary dictionaryWithObjectsAndKeys:self.managedObjectContext,@"managedObjectContext",
//							nil]];
//		[navigator openURLAction:action];
//		
//	}
}

-(void)setDefaultPreferences {
	id value = (id) [[NSUserDefaults standardUserDefaults] boolForKey:@"pref_large_size"];
	if ( value == nil ) {
		NSString *bPath = [[NSBundle mainBundle] bundlePath];
		NSString *settingsPath = [bPath stringByAppendingPathComponent:@"Settings.bundle"];
		NSString *plistFile = [settingsPath stringByAppendingPathComponent:@"Root.plist"];
		
		//Get the Preferences Array from the dictionary
		NSDictionary *settingsDictionary = [NSDictionary dictionaryWithContentsOfFile:plistFile];
		NSArray *preferencesArray = [settingsDictionary objectForKey:@"PreferenceSpecifiers"];
		
		NSMutableDictionary *prefs = [[[NSMutableDictionary alloc] init] autorelease];
		for (NSDictionary *item in preferencesArray) {
			if ( [item objectForKey:@"Key"] != nil )
				[prefs setObject:[item objectForKey:@"DefaultValue"] forKey:[item objectForKey:@"Key"]];
		}

		//Register and save the dictionary to the disk
		[[NSUserDefaults standardUserDefaults] registerDefaults:prefs];
		[[NSUserDefaults standardUserDefaults] synchronize];
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)dealloc {
  TT_RELEASE_SAFELY(_managedObjectContext);
  TT_RELEASE_SAFELY(_managedObjectModel);
  TT_RELEASE_SAFELY(_persistentStoreCoordinator);

	[super dealloc];
}


///////////////////////////////////////////////////////////////////////////////////////////////////
- (BOOL)navigator:(TTNavigator*)navigator shouldOpenURL:(NSURL*)URL {
  return YES;
}


///////////////////////////////////////////////////////////////////////////////////////////////////
- (BOOL)application:(UIApplication*)application handleOpenURL:(NSURL*)URL {
  [[TTNavigator navigator] openURLAction:[TTURLAction actionWithURLPath:URL.absoluteString]];
  return YES;
}


///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)applicationWillTerminate:(UIApplication *)application {
  NSError* error = nil;
  if (_managedObjectContext != nil) {
    if ([_managedObjectContext hasChanges] && ![_managedObjectContext save:&error]) {
      NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
      abort();
    }
  }
}


///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Core Data stack


///////////////////////////////////////////////////////////////////////////////////////////////////
- (NSManagedObjectContext*)managedObjectContext {
  if( _managedObjectContext != nil ) {
    return _managedObjectContext;
  }
	
  NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
  if (coordinator != nil) {
    _managedObjectContext = [[NSManagedObjectContext alloc] init];
    [_managedObjectContext setPersistentStoreCoordinator: coordinator];
    [_managedObjectContext setUndoManager:nil];
    [_managedObjectContext setRetainsRegisteredObjects:YES];
  }
  return _managedObjectContext;
}


///////////////////////////////////////////////////////////////////////////////////////////////////
- (NSManagedObjectModel*)managedObjectModel {
  if( _managedObjectModel != nil ) {
    return _managedObjectModel;
  }
  _managedObjectModel = [[NSManagedObjectModel mergedModelFromBundles:nil] retain];
  return _managedObjectModel;
}


///////////////////////////////////////////////////////////////////////////////////////////////////
- (NSString*)storePath {
  return [[self applicationDocumentsDirectory]
    stringByAppendingPathComponent: kStoreFilename];
}


///////////////////////////////////////////////////////////////////////////////////////////////////
- (NSURL*)storeUrl {
  return [NSURL fileURLWithPath:[self storePath]];
}


///////////////////////////////////////////////////////////////////////////////////////////////////
- (NSDictionary*)migrationOptions {
  return nil;
}


///////////////////////////////////////////////////////////////////////////////////////////////////
- (NSPersistentStoreCoordinator*)persistentStoreCoordinator {
  if( _persistentStoreCoordinator != nil ) {
    return _persistentStoreCoordinator;
  }

  NSString* storePath = [self storePath];
  NSURL *storeUrl = [self storeUrl];

	NSError* error;
  _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc]
    initWithManagedObjectModel: [self managedObjectModel]];

  NSDictionary* options = [self migrationOptions];

  // Check whether the store already exists or not.
  NSFileManager* fileManager = [NSFileManager defaultManager];
  BOOL exists = [fileManager fileExistsAtPath:storePath];

  TTDINFO(@"%@",storePath);
  if( !exists ) {
    _modelCreated = YES;
  } else {
    if( _resetModel ||
        [[NSUserDefaults standardUserDefaults] boolForKey:@"erase_all_preference"] ) {
      [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"erase_all_preference"];
      [fileManager removeItemAtPath:storePath error:nil];
      _modelCreated = YES;
    }
  }

  if (![_persistentStoreCoordinator
    addPersistentStoreWithType: kStoreType
                 configuration: nil
                           URL: storeUrl
                       options: options
                         error: &error
  ]) {
    // We couldn't add the persistent store, so let's wipe it out and try again.
    [fileManager removeItemAtPath:storePath error:nil];
    _modelCreated = YES;

    if (![_persistentStoreCoordinator
      addPersistentStoreWithType: kStoreType
                   configuration: nil
                             URL: storeUrl
                         options: nil
                           error: &error
    ]) {
      // Something is terribly wrong here.
    }
  }

  return _persistentStoreCoordinator;
}


///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Application's documents directory


///////////////////////////////////////////////////////////////////////////////////////////////////
- (NSString*)applicationDocumentsDirectory {
  return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)
    lastObject];
}


@end

