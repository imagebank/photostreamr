//
//  OfflineFoldersController.m
//  ImageBankFree
//
//  Created by Yoshi Sugawara on 5/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "OfflineFoldersController.h"
#import "File.h"
#import "HelpPopOverController.h"
#import "Utilities.h"
#import "PrivateOfflineFoldersPasswordController.h"
#import "PrivateFolderPasswordChecker.h"
#import "Constants.h"
#import "FolderEditController.h"
#import "FolderContentsController.h"
#import "Stylesheet.h"


@implementation OfflineFoldersController

@synthesize offlineFolders, managedObjContext, privateFoldersAccessButton, changePasswordButton, noFoldersView, savedFoldersDescription, savedFoldersStorageDescription, tableView, scrollView, sortByField;


-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self setOfflineFolders:[[NSMutableArray alloc] init]];
        NSDictionary *sortOptions = [Utilities folderSortOptions];
        NSString *sortByFieldName = [sortOptions objectForKey:@"sort_by_field"];
        NSString *isAscValue = [sortOptions objectForKey:@"is_asc"];
        [self setSortByField:sortByFieldName];
        sortAscending = [isAscValue isEqualToString:@"yes"];
    }
    return self;    
}

-(void)dealloc {
    [offlineFolders release];
    [managedObjContext release];
    [privateFoldersAccessButton release];
    [changePasswordButton release];
    [noFoldersView release];
    [savedFoldersDescription release];
    [tableView release];
    [savedFoldersStorageDescription release];
    [scrollView release];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [super dealloc];
}

-(void)revealSideMenu:(id)sender {
    [self.revealSideViewController pushOldViewControllerOnDirection:PPRevealSideDirectionLeft withOffset:[UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad ? 470.0 : 70.0 animated:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    [savedFoldersDescription setText:[NSString stringWithFormat:[savedFoldersDescription text],[UIDevice currentDevice].model]];
    [savedFoldersStorageDescription setText:[NSString stringWithFormat:[savedFoldersStorageDescription text],[UIDevice currentDevice].model]];

    [self.view setBackgroundColor:[UIColor colorWithRed:211/255.0f green:211/255.0f blue:211/255.0f alpha:1.0f]];
    [self.tableView setBackgroundColor:[UIColor clearColor]];
    
    if ( scrollView ) {
        scrollView.contentSize = CGSizeMake(self.view.bounds.size.width, self.view.bounds.size.height);
    }

    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    self.title = NSLocalizedString(@"Saved Folders", @"");
    
	self.navigationItem.rightBarButtonItem = self.editButtonItem;
	self.navigationItem.rightBarButtonItem.enabled = YES;

    
    self.privateFoldersAccessButton = [[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:[[PrivateFolderPasswordChecker sharedInstance] allowOfflineAccess] ? @"icon_unlock.png" : @"icon_lock.png"]
																		style:UIBarButtonItemStyleBordered
																	   target:self
																	   action:@selector(showPrivatePasswordAccess:)] autorelease];
    privateFoldersAccessButton.tag = kPrivateFoldersAccessToolbarButton;
    
    privatePasswordDialog = [[PrivatePasswordDialog alloc] initWithVC:self];
    [privatePasswordDialog setIsForOfflineFolders:YES];

    UIBarButtonItem *space = [[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil] autorelease];
    
    UIBarButtonItem *sideMenuButton =
    [[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ButtonMenu.png"]
                                      style:UIBarButtonItemStyleBordered
                                     target:self
                                     action:@selector(revealSideMenu:)] autorelease];

    [self setToolbarItems:[NSArray arrayWithObjects:sideMenuButton,space,privateFoldersAccessButton, space, nil]];
    
    [self setChangePasswordButton:[[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Change Password for Saved Folders",@"") style:UIBarButtonItemStyleBordered target:self action:@selector(showPrivateOfflineFoldersSettingsController)] autorelease]];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

-(void)maybeShowDescriptionView {
    if ( [offlineFolders count] == 0 ) {
        self.tableView.hidden = YES;
        self.noFoldersView.hidden = NO;
    } else {
        self.tableView.hidden = NO;
        self.noFoldersView.hidden = YES;
    }
}

- (void) viewWillAppear:(BOOL)animated {
    [self.offlineFolders removeAllObjects];
    [self refreshDataModel];
    // fix the private password button based on the current state
    [self updatePrivateFoldersAccessButtonStatus];
    [self.tableView reloadData];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(appGoingIntoBackground:)
												 name:UIApplicationWillResignActiveNotification object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(appGoingIntoForeground:)
												 name:UIApplicationDidBecomeActiveNotification object:nil];

    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"titlebar"]];

    [self maybeShowDescriptionView];
    
    [super viewWillAppear:animated];
    
    self.revealSideViewController.panInteractionsWhenClosed = PPRevealSideInteractionNavigationBar;
    [self.revealSideViewController setDelegate:self];
    HelpPopOverController *sideViewController = (HelpPopOverController*) [self.revealSideViewController  controllerForSide:PPRevealSideDirectionLeft];
    [sideViewController setDelegate:self];
}

-(void)viewWillDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [super viewWillDisappear:animated];
}

-(void)appGoingIntoBackground:(id)sender {
    [[PrivateFolderPasswordChecker sharedInstance] setDateWentIntoBackground:[NSDate date]];
    
	// hide the main view
	self.navigationController.view.alpha = 0.0;
//	[privatePasswordDialog dismissDialog];
}

-(void)appGoingIntoForeground:(id)sender {
    if ( [[PrivateFolderPasswordChecker sharedInstance] shouldCheckAccess] ||
        [privatePasswordDialog isAlertShown]) {
        [privatePasswordDialog dismissDialog];
        [[PrivateFolderPasswordChecker sharedInstance] setAllowOfflineAccess:NO];
        [self refreshDataModel];
        [self.tableView reloadData];
        [self updatePrivateFoldersAccessButtonStatus];
    }
    self.navigationController.view.alpha = 1.0;
}

-(void)updatePrivateFoldersAccessButtonStatus {
    self.privateFoldersAccessButton.image = 
    [Utilities isOfflinePrivateFoldersPasswordSetup] ?
    [UIImage imageNamed:[[PrivateFolderPasswordChecker sharedInstance] allowOfflineAccess] ? @"icon_unlock.png" : @"icon_lock.png"]
    : [UIImage imageNamed:@"icon_unlock.png"];
}

-(void)refreshDataModel {
    [self.offlineFolders removeAllObjects];
    
    // sort
    NSArray *folders = [[Utilities offlineFolders]
                               sortedArrayUsingComparator:^NSComparisonResult(id first, id second) {
                                   DirectoryFile *firstDir = (DirectoryFile*) first;
                                   DirectoryFile *secondDir = (DirectoryFile*) second;
                                   if ( [self.sortByField isEqualToString:@"date"] ) {
                                       return sortAscending ? [[firstDir modifiedDate] compare:[secondDir modifiedDate]] :
                                       [[secondDir modifiedDate] compare:[firstDir modifiedDate]];
                                   }
                                   // name
                                   NSArray *firstparts = [[firstDir name] componentsSeparatedByString:@"/"];
                                   NSString *firstFilename = [firstparts objectAtIndex:[firstparts count]-1];
                                   NSArray *secondparts = [[secondDir name] componentsSeparatedByString:@"/"];
                                   NSString *secondFilename = [secondparts objectAtIndex:[secondparts count]-1];
                                   return sortAscending ? [[firstFilename uppercaseString] compare:[secondFilename uppercaseString]] :
                                   [[secondFilename uppercaseString] compare:[firstFilename uppercaseString]];
                               }];
    
    NSMutableArray *tableEntries = [[NSMutableArray alloc] init];
    for (DirectoryFile* dir in folders) {
        [tableEntries addObject:[dir name]];
    }
    [self setOfflineFolders:tableEntries];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.offlineFolders count];
}

- (UITableViewCell *)tableView:(UITableView *)thisTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Configure the cell...
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [thisTableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    
    UIImage *image = [UIImage imageNamed:@"cell-gradient.png"];
    UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
    imageView.contentMode = UIViewContentModeScaleToFill;
    cell.backgroundView = imageView;
    cell.textLabel.backgroundColor = [UIColor clearColor];
    cell.imageView.image = [UIImage imageNamed:@"tableitem-iphoto.png"];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.editingAccessoryType = UITableViewCellAccessoryDetailDisclosureButton;
    cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:16];
    cell.textLabel.lineBreakMode = UILineBreakModeCharacterWrap;
    cell.textLabel.numberOfLines = 0;
    
    // Set up the cell...
    NSString *fullPath = [self.offlineFolders objectAtIndex:indexPath.row];
    NSArray *parts = [fullPath componentsSeparatedByString:@"/"];
    NSString *filename = [parts objectAtIndex:[parts count]-1];

    cell.textLabel.text = filename;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    NSString *text = [self.offlineFolders objectAtIndex:[indexPath row]];
    NSArray *parts = [text componentsSeparatedByString:@"/"];
    NSString *filename = [parts objectAtIndex:[parts count]-1];
    
    CGSize constraint = CGSizeMake(self.tableView.bounds.size.width, 144.0f);
    
    CGSize size = [filename sizeWithFont:[UIFont systemFontOfSize:15.0] constrainedToSize:constraint lineBreakMode:UILineBreakModeCharacterWrap];
    
    CGFloat height = MAX(size.height + 40, 44.0f);
    
    return height;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        NSString *fullPath = [[self.offlineFolders objectAtIndex:indexPath.row] retain];

        // remove the object from the ds
        [offlineFolders removeObjectAtIndex:indexPath.row];

        // delete the folder
        NSError *error = nil;
        [[NSFileManager defaultManager] removeItemAtPath:fullPath error:&error];
        
        [fullPath release];
        
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
        
        if ( [self.offlineFolders count] == 0 ) {
            [self setEditing:NO animated:YES];
        }
        
        [self maybeShowDescriptionView];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    NSString *fullPath = (NSString *)[offlineFolders objectAtIndex:indexPath.row];
    
    NSArray *parts = [fullPath componentsSeparatedByString:@"/"];
    NSString *filename = [parts objectAtIndex:[parts count]-1];
    DirectoryFile *dir = [[[DirectoryFile alloc] initWithName:filename url:fullPath imageCount:0 dirCount:0] autorelease];
    [dir setIsPrivate:[Utilities isOfflinePrivateFolder:fullPath]];
    
    FolderContentsController *fc = [[FolderContentsController alloc] initWithNavigatorURL:nil query:[NSDictionary dictionaryWithObjectsAndKeys:dir,@"file",
            self.managedObjContext,@"managedObjectContext",
            @"yes",@"isOfflineFolder",nil]];
    [self.navigationController pushViewController:fc animated:YES];
}

- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath {
    NSString *fullPath = (NSString *)[self.offlineFolders objectAtIndex:indexPath.row];
    FolderEditController *folderEditController = [[[FolderEditController alloc] initWithNibName:@"FolderEditController" bundle:nil] autorelease
                                                  ];
    [folderEditController setFullPath:fullPath];
    [self.navigationController pushViewController:folderEditController animated:YES];
}

-(void)setEditing:(BOOL)editing animated:(BOOL)animated {
    [super setEditing:editing animated:animated];
    [self.tableView setEditing:editing animated:animated];
    NSMutableArray *newItems = [NSMutableArray arrayWithArray:[self toolbarItems]];
    UIBarButtonItem *space = [[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil] autorelease];
    if ( editing ) {
        [newItems removeLastObject];
        [newItems removeLastObject];
        if ( [[PrivateFolderPasswordChecker sharedInstance] allowOfflineAccess] ) {
            [newItems addObject:self.changePasswordButton];
        } 
    } else {
        if ( [[PrivateFolderPasswordChecker sharedInstance] allowOfflineAccess] ) {
            [newItems removeLastObject];
        }
        [newItems addObject:self.privateFoldersAccessButton];
        [newItems addObject:space];
    }
    [self setToolbarItems:newItems animated:YES];
}

#pragma mark
#pragma mark IAskSettingsDelegate
- (void)settingsViewControllerDidEnd:(IASKAppSettingsViewController*)sender {
    [self dismissModalViewControllerAnimated:YES];
    if ( ![[NSUserDefaults standardUserDefaults] boolForKey:@"pref_save_server_passwords"] ) {
        [Utilities destroySavedPasswords];
    }
}

#pragma mark
#pragma mark PopOverContentDelegate
-(void)showSettingsController {
	IASKAppSettingsViewController *appSettingsViewController = [[IASKAppSettingsViewController alloc] initWithNibName:@"IASKAppSettingsView" bundle:nil];
	appSettingsViewController.delegate = self;
	UINavigationController *aNavController = [[UINavigationController alloc] initWithRootViewController:appSettingsViewController];
    [appSettingsViewController setShowCreditsFooter:NO];
    appSettingsViewController.showDoneButton = YES;
	aNavController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
	if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
		aNavController.modalPresentationStyle = UIModalPresentationFormSheet;
    [self presentModalViewController:aNavController animated:YES];
	[appSettingsViewController release];
    [aNavController release];
}

- (void)showUpgradeViewController {
}

-(void) showPrivateOfflineFoldersSettingsController {
    PrivateOfflineFoldersPasswordController *controller = [[[PrivateOfflineFoldersPasswordController alloc] init] autorelease];
  	UINavigationController *aNavController = [[UINavigationController alloc] initWithRootViewController:controller];
	if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
		aNavController.modalPresentationStyle = UIModalPresentationFormSheet;
    [self presentModalViewController:aNavController animated:YES];
    [aNavController release];
}

-(void)popToRootController {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

#pragma mark -
#pragma mark PPRevealSideViewControllerDelegate
-(void) pprevealSideViewController:(PPRevealSideViewController *)controller didPopToController:(UIViewController *)centerController {
    self.tableView.userInteractionEnabled = YES;
    NSDictionary *sortOptions = [Utilities folderSortOptions];
    NSString *name = [sortOptions objectForKey:@"sort_by_field"];
    NSString *isAscValue = [sortOptions objectForKey:@"is_asc"];
    
    // check if the saved values differ from that of the initially loaded values
    if ( ![self.sortByField isEqualToString:name] || sortAscending != [isAscValue isEqualToString:@"yes"]) {
        [self setSortByField:name];
        sortAscending = [isAscValue isEqualToString:@"yes"];
        [self refreshDataModel];
        [self.tableView reloadData];
    } else {
        NSLog(@"Sort options remained the same, not refreshing folder contents...");
    }
}

- (void) pprevealSideViewController:(PPRevealSideViewController *)controller didPushController:(UIViewController *)pushedController {
    self.tableView.userInteractionEnabled = NO;
}

#pragma mark
#pragma mark ProtectedContentDelegate
-(void) doAfterPrivateAccessAllowed {
	self.navigationController.view.alpha = 1.0;	
	self.privateFoldersAccessButton.image = [UIImage imageNamed:@"icon_unlock.png"];
	[self refreshDataModel];
    [self.tableView reloadData];
    self.tableView.hidden = NO;
    self.noFoldersView.hidden = YES;
    [self maybeShowDescriptionView];
}

-(UIView*) getView {
	return self.navigationController.view;
}

-(void) doAfterPrivateAccessDenied {
	self.navigationController.view.alpha = 1.0;
}

-(void) doAfterPrivateAccessDeniedOnResume {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

#pragma mark
#pragma mark UI Helpers

-(void)showPrivatePasswordAccess:(id)sender {
    
    // show setup if offline PF password not setup yet
    if ( ![Utilities isOfflinePrivateFoldersPasswordSetup] ) {
        [self showPrivateOfflineFoldersSettingsController];
        return;
    }
    
	if ( [[PrivateFolderPasswordChecker sharedInstance] allowOfflineAccess] ) {
		// private folders are currently viewable, so user wants to lock access
		[[PrivateFolderPasswordChecker sharedInstance] setAllowOfflineAccess:NO];
		
		CGRect lockFlashRect = CGRectMake(self.view.center.x - 100.0f, self.view.center.y - 100.0f, 200.0f, 200.0f);
		lockFlashView = [[UIImageView alloc] initWithFrame:lockFlashRect];
		lockFlashView.image = [UIImage imageNamed:@"img-lock.png"];
		lockFlashView.alpha = 0.75;
		[self.navigationController.view addSubview:lockFlashView];
		[UIView beginAnimations:nil context:nil];
		[UIView setAnimationDuration:1.2];
		[UIView setAnimationTransition:UIViewAnimationTransitionNone forView:self.navigationController.view cache:YES];
		[UIView setAnimationDelegate:self];
		[UIView setAnimationDidStopSelector:@selector(lockFlashAnimationDone)];
		lockFlashView.alpha = 0.0;
		[UIView commitAnimations];
		
	} else {
		[privatePasswordDialog showPrivatePasswordDialog];
	}
}

-(void)lockFlashAnimationDone {
	[lockFlashView removeFromSuperview];
	self.privateFoldersAccessButton.image = [UIImage imageNamed:@"icon_lock.png"];
	[self refreshDataModel];
    [self.tableView reloadData];
    [self maybeShowDescriptionView];
}	

@end
