//
//  TTURLCache+AnimatedGIFSupport.m
//  ImageBank
//
//  Created by yoshi on 3/26/13.
//
//

#import "TTURLCache+AnimatedGIFSupport.h"
#import "UIImage+animatedGIF.h"
#import "Utilities.h"

@implementation TTURLCache (AnimatedGIFSupport)

- (UIImage*)loadImageFromDocuments:(NSString*)URL {
    NSLog(@"in custom loadImageFromDocuments!");
    UIImage *image;
    NSString* path = TTPathForDocumentsResource([URL substringFromIndex:12]);
    NSData* data = [NSData dataWithContentsOfFile:path];
    NSString *imageType = [Utilities contentTypeForImageData:data];
    if ( [imageType isEqualToString:@"image/gif"] ) {
        image = [UIImage animatedImageWithAnimatedGIFData:data];
    } else {
        image = [UIImage imageWithData:data];
    }    
    return image;
}

@end
