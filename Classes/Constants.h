/*
 *  Constants.h
 *  ImageBank
 *
 *  Created by yoshi on 6/4/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#define kUISegmentedControlTag	9000
#define kNameFieldTag 10
#define kAddressFieldTag 20
#define kPortFieldTag 30
#define kPINFieldTag 40
#define kPrivateFoldersAccessToolbarButton 2000
#define kMaxFolderImagesDisplayForFreeVersion 60
#define kMaxShuffleImagesDisplayForFreeVersion 8
#define kDownloadSelectedButtonTag 410
#define kDownloadAllButtonTag 411


