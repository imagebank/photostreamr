//
//  BuyUpgradeController.h
//  PhotoStreamr
//
//  Created by yoshi on 8/4/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <StoreKit/StoreKit.h>
#import "MKStoreManager.h"


@interface BuyUpgradeController : UIViewController<SKProductsRequestDelegate,MKStoreKitDelegate,SKRequestDelegate> {
	IBOutlet UILabel *upgradeNowLabel;
	IBOutlet UIActivityIndicatorView *activityIndicator;
	IBOutlet UIView *purchaseInProgressView;
	IBOutlet UIActivityIndicatorView *purchasingActivityIndicator;
	IBOutlet UIButton *purchaseButton;
    IBOutlet UIButton *restoreButton;
    IBOutlet UILabel *purchasingNowLabel;
	SKProductsRequest *_productRequest;
	NSManagedObjectContext *managedObjContext;
    SKProduct *fullVersionUpgradeProduct;
}
- (void) requestProductData;
- (IBAction) cancel:(id)sender;
- (IBAction) buyUpgrade:(id)sender;

@property (nonatomic,readwrite,retain) IBOutlet UILabel *upgradeNowLabel;
@property (nonatomic,readwrite,retain) IBOutlet UILabel *purchasingNowLabel;
@property (readwrite,nonatomic,retain) NSManagedObjectContext *managedObjContext;
@property (nonatomic,readwrite,retain) SKProduct *fullVersionUpgradeProduct;

@end
