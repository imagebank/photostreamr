//
//  FolderContentsModel.m
//  ImageBankFree
//
//  Created by Yoshi Sugawara on 9/26/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "FolderContentsModel.h"
#import "FileListModel.h"
#import "OfflineFileListModel.h"

@implementation FolderContentsModel

@synthesize fileListModel, photosModel, isRootDirectory;

-(id)initWithTitle:(NSString*)title directory:(DirectoryFile*)dir isOffline:(BOOL)isOffline {
    if ( self = [super init] ) {
        FileListModel *flm = isOffline ? [[OfflineFileListModel alloc] initWithDocumentsDirectory:[dir url]]
            : [[[FileListModel alloc] initWithDirectory:dir] autorelease];
        [[flm delegates] addObject:self];
       	[self setFileListModel:flm];
        PhotoSource *ps = [[[PhotoSource alloc] initWithTitle:title
                                                    directory:dir
                                          totalNumberOfPhotos:0
                                       forceReloadFromNetwork:YES] autorelease];
        [[ps delegates] addObject:self];
        [self setPhotosModel:ps];
    }
    return self;
}

-(void)dealloc {
    [fileListModel release];
    [photosModel release];
    [super dealloc];
}

- (BOOL)isLoaded {
    return fileListModel.isLoaded && (isRootDirectory || photosModel.isLoaded);
}

- (BOOL)isLoading {
    if ( fileListModel.isLoading ) 
        return YES;
    
    if ( !isRootDirectory && photosModel.isLoading )
        return YES;
    
    return NO;
}

-(void)modelDidStartLoad:(id<TTModel>)model {
    [self didStartLoad];
}

- (void)load:(TTURLRequestCachePolicy)cachePolicy more:(BOOL)more {
    if ( more && ![self isLoading] ) {
        [self.photosModel load:cachePolicy more:more];
    } else {
        [fileListModel load:cachePolicy more:more];
    }
}

- (void)modelDidFinishLoad:(id<TTModel>)model {
    TTDINFO(@"folder contents model did finish load called! %@",[model class]);
    
    if ( [model isKindOfClass:[FileListModel class]] ) {
        FileListModel *flm = (FileListModel*)model;
        // if we don't have images we only need the file list and so we're done
        if ( [flm imageCount] < 1 ) {
            [photosModel setLoadedTime:[NSDate date]];
            [self didFinishLoad];
            return;
        }

        [self.photosModel setTotalNumberOfPhotos:[flm imageCount]];
        [self.photosModel setPhotosFromFileListModel:flm];
//        [[self.photosModel delegates] addObject:self];
//        [self.photosModel load:TTURLRequestCachePolicyDefault more:NO];
    }
    
    if ( self.isLoaded ) {
        [self didFinishLoad];
    }
}

- (id)copyWithZone:(NSZone *)zone{
    FolderContentsModel *copy = [[[self class] allocWithZone:zone]
initWithTitle:self.photosModel.title directory:self.fileListModel.directory
isOffline:[self isKindOfClass:[OfflineFileListModel class]]];
    return copy;
}

@end
