//
//  PopOverContentDelegate.h
//  ImageBankFree
//
//  Created by Yoshi Sugawara on 2/5/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol PopOverContentDelegate <NSObject>

- (void)showUpgradeViewController;
- (void)showSettingsController;

@optional
-(void) showPrivateOfflineFoldersSettingsController;
-(void)didChangeSortByField:(NSString*)sortBy isAscending:(BOOL)isAsc;
-(void)popToRootController;

@end
