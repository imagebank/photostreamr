//
//  FilesListModel.m
//  ImageBank
//
//  Created by yoshi on 5/13/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "FileListModel.h"
#import "FileListResponseProcessor.h"
#import "Constants.h"

#define kNumberOfPhotosToFetch 20

@implementation FileListModel

@synthesize forceLoadingFromNetwork, directory, sortByField, sortAscending;

-(id)initWithDirectory:(DirectoryFile*)dir {
	if ((self = [super init])) {
		[self setDirectory:dir];
		urlForExternalDirectoryContents = [[self.directory url] retain];
		responseProcessor = [[FileListResponseProcessor alloc] init];
        [self setSortByField:@"name"];
        [self setSortAscending:YES];
	}
	return self;
}


-(NSArray*) files {
	NSArray *files = [[[responseProcessor files] copy] autorelease];
	return files;
}

-(NSArray*) dirs {
    NSArray *dirs = [[[responseProcessor dirs] copy] autorelease];
    return dirs;
}

-(NSInteger)imageCount {
	return [responseProcessor totalNumberOfPhotos];
}

#pragma mark
#pragma mark TTURLRequest Model

- (void)load:(TTURLRequestCachePolicy)cachePolicy more:(BOOL)more {
	[responseProcessor.files removeAllObjects];
	TTDINFO(@"Creating request for file list model...");
    NSString *url = nil;
    NSInteger numberOfPhotosToFetch = [[[NSUserDefaults standardUserDefaults] objectForKey:@"pref_thumbnail_count"] intValue];
    if ( !numberOfPhotosToFetch )
        numberOfPhotosToFetch = kNumberOfPhotosToFetch;
    url = [NSString stringWithFormat:@"%@&i=1&start=%i&n=%i&sort_by=%@&sort_asc=%@",directory.url,0,numberOfPhotosToFetch,self.sortByField,(self.sortAscending ? @"true" : @"false")];

    TTDINFO(@"request url = %@",url);
	TTURLRequest *request = [TTURLRequest requestWithURL:url delegate:self];
	request.cachePolicy = cachePolicy;
	if ( self.forceLoadingFromNetwork )
		request.cachePolicy = TTURLRequestCachePolicyNetwork;
	request.response = responseProcessor;
	request.httpMethod = @"GET";
	[request send];
}

-(void)loadSynchronously:(TTURLRequestCachePolicy)cachePolicy {
	[responseProcessor.files removeAllObjects];
	TTDINFO(@"Creating request for file list model (synchronous)...");    
    TTDINFO(@"request url = %@",directory.url);
	TTURLRequest *request = [TTURLRequest requestWithURL:directory.url delegate:self];
	request.cachePolicy = cachePolicy;
	request.response = responseProcessor;
	request.httpMethod = @"GET";
	[request sendSynchronously];
}

- (id)copyWithZone:(NSZone *)zone{
    FileListModel *copy = [[[self class] allocWithZone:zone] initWithDirectory:self.directory];
    return copy;
}

- (void) dealloc {
	[urlForExternalDirectoryContents release];
	[responseProcessor release];
	[super dealloc];
}

@end
