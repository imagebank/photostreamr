//
//  NSObject+SharedQueue.m
//  ImageBankFree
//
//  Created by Yoshi Sugawara on 7/29/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "NSObject+SharedQueue.h"
#import "NSOperationQueue+SharedQueue.h"

@implementation NSObject (SharedQueue)

-(void)performSelectorOnBackgroundQueue:(SEL)aSelector withObject:(id)anObject;
{
    NSOperation* operation = [[NSInvocationOperation alloc] initWithTarget:self
                                                                  selector:aSelector
                                                                    object:anObject];
    [[NSOperationQueue sharedOperationQueue] addOperation:operation];
    [operation release];
}

@end
