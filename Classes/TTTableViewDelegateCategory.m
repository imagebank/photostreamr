//
// TTTableViewDelegateCategory.m
// ImageBank
//
// Created by Mike on 5/6/10.
// Copyright 2010 Prime31 Studios. All rights reserved.
//

#import "TTTableViewDelegateCategory.h"
#import "FolderContentsController.h"
#import "AddServerController.h"
#import "HTMLHelpController.h"

static const CGFloat kSectionHeaderHeight = 25;

@implementation TTTableViewDelegate(Category)

- (void)tableView:(UITableView*)tableView didSelectRowAtIndexPath:(NSIndexPath*)indexPath
{
	id<TTTableViewDataSource> dataSource = (id<TTTableViewDataSource>)tableView.dataSource;
	id object = [dataSource tableView:tableView objectForRowAtIndexPath:indexPath];
	
	// Added section to automatically wrap up any TTTableItem userInfo objects. If it is a dictionary, it gets sent directly
	// If it is not, it is put in a dictionary and sent as they __userInfo__ key
	if( [object isKindOfClass:[TTTableLinkedItem class]] )
	{
		TTTableLinkedItem* item = object;
		
		if( item.URL && [_controller shouldOpenURL:item.URL] )
		{

			// If the TTTableItem has userInfo, wrap it up and send it along to the URL
			if( item.userInfo )
			{
				NSDictionary *userInfoDict;
				
				// If userInfo is a dictionary, pass it along else create a dictionary
				if( [item.userInfo isKindOfClass:[NSDictionary class]] )
				{
					userInfoDict = item.userInfo;
				}
				else
				{
					userInfoDict = [NSDictionary dictionaryWithObject:item.userInfo forKey:@"__userInfo__"];
				}
                
                if ( [item.URL isEqualToString:@"tt://folderview?"] ) {
                    FolderContentsController *fc = [[FolderContentsController alloc] initWithNavigatorURL:nil query:userInfoDict];
                    [_controller.navigationController pushViewController:fc animated:YES];
                    [fc release];
                    [_controller didSelectObject:object atIndexPath:indexPath];
                    return;
                } else if ( [item.URL isEqualToString:@"tt://addservermanually"]) {
                    AddServerController *sc = [[AddServerController alloc] initWithNavigatorURL:nil query:userInfoDict];
                    [_controller.navigationController pushViewController:sc animated:YES];
                    [sc release];
                    [_controller didSelectObject:object atIndexPath:indexPath];
                    return;
                } else if ( [item.URL isEqualToString:@"tt://htmlhelp"]) {
                    HTMLHelpController *htmlHelp = [[HTMLHelpController alloc] initWithNavigatorURL:nil query:userInfoDict];
                    [_controller.navigationController pushViewController:htmlHelp animated:YES];
                    [htmlHelp release];
                    [_controller didSelectObject:object atIndexPath:indexPath];
                    return;
                }
				
				[[TTNavigator navigator] openURLAction:[[[TTURLAction actionWithURLPath:item.URL]
														 applyQuery:userInfoDict]
														applyAnimated:YES]];
			}
			else
			{
				TTOpenURL( item.URL );
			}
		}
		
		if( [object isKindOfClass:[TTTableButton class]] )
		{
			[tableView deselectRowAtIndexPath:indexPath animated:YES];
		}
		else if( [object isKindOfClass:[TTTableMoreButton class]] )
		{
			TTTableMoreButton* moreLink = (TTTableMoreButton*)object;
			moreLink.isLoading = YES;
			TTTableMoreButtonCell* cell
			= (TTTableMoreButtonCell*)[tableView cellForRowAtIndexPath:indexPath];
			cell.animating = YES;
			cell.userInteractionEnabled = NO;
			[tableView deselectRowAtIndexPath:indexPath animated:YES];
			
			if( moreLink.model )
				[moreLink.model load:TTURLRequestCachePolicyDefault more:YES];
			else
				[_controller.model load:TTURLRequestCachePolicyDefault more:YES];
		}
	}
	
	[_controller didSelectObject:object atIndexPath:indexPath];
}


- (void)tableView:(UITableView*)tableView touchesBegan:(NSSet*)touches withEvent:(UIEvent*)event
{
	// If we have a menuView up we dismiss it ONLY if the touch was not on the menuView
	if( _controller.menuView )
	{
		UITouch *touch = [touches anyObject];
		CGPoint point = [touch locationInView:_controller.menuView];
		if( point.y < 0 || point.y > _controller.menuView.frame.size.height )
		{
			[_controller hideMenu:YES];
		}
	}
}

// hack: disable section display if there's no section header view
// total hack for iOS 5 fix: tableView with a tag of 100 would show the section header lol
- (CGFloat)tableView:(UITableView*)tableView heightForHeaderInSection:(NSInteger)section {
    if ( tableView.tag == 100 || [self tableView:tableView viewForHeaderInSection:section] != nil ) {
        return kSectionHeaderHeight;
    } 
    return 0;
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if ( [scrollView isKindOfClass:[UITableView class]] ) {
        UITableView *tableView = (UITableView*) scrollView;
        UIView *headerView = tableView.tableHeaderView;
        if ( headerView && [headerView isKindOfClass:[UISearchBar class]] ) {
            [(UISearchBar*) headerView resignFirstResponder];
        }
    }
}

@end

