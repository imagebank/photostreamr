//
//  DownloadURLOperation.m
//  ImageBankFree
//
//  Created by Yoshi Sugawara on 12/8/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "DownloadURLOperation.h"
#import "Utilities.h"

@implementation DownloadURLOperation

@synthesize error = error_, data = data_;
@synthesize connectionURL = connectionURL_;
@synthesize orientation, popUpView;


#pragma mark -
#pragma mark Initialization & Memory Management

- (id)initWithURL:(NSURL *)url withOrientation:(UIInterfaceOrientation)orient
{
    if( (self = [super init]) ) {
        connectionURL_ = [url copy];
        [self setOrientation:orient];
        [self setPopUpView:[[PopUpNotificationView alloc] initWithFrame:CGRectZero]];
        [self.popUpView.spinner setHidesWhenStopped:YES];
    }
    return self;
}

- (void)dealloc
{
    if( connection_ ) {
        [connection_ cancel]; [connection_ release]; connection_ = nil;
    }
    [connectionURL_ release];
    connectionURL_ = nil;
    
    [data_ release];
    data_ = nil;
    
    [error_ release];
    error_ = nil;
    
    [super dealloc];
}

#pragma mark -
#pragma mark Start & Utility Methods

// This method is just for convenience. It cancels the URL connection if it
// still exists and finishes up the operation.
- (void)done
{
    if( connection_ ) {
        [connection_ cancel];
        [connection_ release];
        connection_ = nil;
    }
    
    // Alert anyone that we are finished
    [self willChangeValueForKey:@"isExecuting"];
    [self willChangeValueForKey:@"isFinished"];
    executing_ = NO;
    finished_  = YES;
    [self didChangeValueForKey:@"isFinished"];
    [self didChangeValueForKey:@"isExecuting"];
    
    
    if ( error_ != nil ) {
        [self updateStatusMessageFail];
    } else if ( data_ != nil ) {
        // save image to camera roll
        UIImage  *img  = [[[UIImage alloc] initWithData:data_] autorelease];      
        UIImageWriteToSavedPhotosAlbum(img, nil, nil, nil);
        TTDINFO(@"Save complete");
        [self updateStatusMessageSuccess];
    }
    [self hideView];
    [self release];
}

-(void)canceled {
	// Code for being cancelled
    error_ = [[NSError alloc] initWithDomain:@"DownloadUrlOperation"
                                        code:123
                                    userInfo:nil];
	
    [self done];
	
}
- (void)start
{
    // Ensure that this operation starts on the main thread
    if (![NSThread isMainThread])
    {
        [self performSelectorOnMainThread:@selector(start)
                               withObject:nil waitUntilDone:NO];
        return;
    }
    
    // Ensure that the operation should exute
    if( finished_ || [self isCancelled] ) { [self done]; return; }
    
    // From this point on, the operation is officially executing--remember, isExecuting
    // needs to be KVO compliant!
    [self willChangeValueForKey:@"isExecuting"];
    executing_ = YES;
    [self didChangeValueForKey:@"isExecuting"];
    [self slideInPopUpView];
	[NSTimer scheduledTimerWithTimeInterval:1.5
									 target:self
								   selector:@selector(doRequest:)
								   userInfo:nil
									repeats:NO];
    

}

-(void)slideInPopUpView {
    if (![NSThread isMainThread]) 
        [self performSelectorOnMainThread:@selector(slideInPopUpView) withObject:nil waitUntilDone:NO];
    [popUpView slideInPopUpView:self.orientation localizedTextKey:@"Saving Photo to Camera Roll"];
}

-(void)doRequest:(NSTimer*)timer {
    // Create the NSURLConnection--this could have been done in init, but we delayed
    // until no in case the operation was never enqueued or was cancelled before starting
    connection_ = [[NSURLConnection alloc] initWithRequest:[NSURLRequest requestWithURL:connectionURL_ cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:20.0]
                                                  delegate:self];  
}

-(void) hideView {
    if (![NSThread isMainThread]) 
        [self performSelectorOnMainThread:@selector(hideView) withObject:nil waitUntilDone:NO];
    [popUpView hideView];
}

-(void)slideOutPopUpFromView {
    if (![NSThread isMainThread]) 
        [self performSelectorOnMainThread:@selector(slideOutPopUpFromView) withObject:nil waitUntilDone:NO];
	[self.popUpView slideOutPopUpFromView];
}

-(void)updateStatusMessageSuccess {
    if (![NSThread isMainThread]) 
        [self performSelectorOnMainThread:@selector(updateStatusMessageSuccess) withObject:nil waitUntilDone:NO];
    [self.popUpView.spinner stopAnimating];
    [self.popUpView.doneImage setHidden:NO];
    [self.popUpView bringSubviewToFront:self.popUpView.doneImage];
        [self.popUpView.statusMsg setText:NSLocalizedString(@"Photo Saved to Camera Roll",@"Photo Saved to Camera Roll")];
}

-(void)updateStatusMessageFail {
    if (![NSThread isMainThread]) 
        [self performSelectorOnMainThread:@selector(updateStatusMessageFail) withObject:nil waitUntilDone:NO];
    [self.popUpView.spinner stopAnimating];
    [self.popUpView.failImage setHidden:NO];
    [self.popUpView bringSubviewToFront:self.popUpView.failImage];
    [self.popUpView.statusMsg setText:NSLocalizedString(@"Could not save photo",@"Could not save photo")];    
}

#pragma mark -
#pragma mark Overrides

- (BOOL)isConcurrent
{
    return YES;
}

- (BOOL)isExecuting
{
    return executing_;
}

- (BOOL)isFinished
{
    return finished_;
}

#pragma mark -
#pragma mark Delegate Methods for NSURLConnection

// The connection failed
- (void)connection:(NSURLConnection*)connection didFailWithError:(NSError*)error
{
    // Check if the operation has been cancelled
    if([self isCancelled]) {
        [self canceled];
		return;
    }
	else {
		[data_ release];
		data_ = nil;
		error_ = [error retain];
		[self done];
	}
}

// The connection received more data
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    // Check if the operation has been cancelled
    if([self isCancelled]) {
        [self canceled];
		return;
    }
    
    [data_ appendData:data];
}

// Initial response
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    // Check if the operation has been cancelled
    if([self isCancelled]) {
        [self canceled];
		return;
    }
    
    NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
    NSInteger statusCode = [httpResponse statusCode];
    if( statusCode == 200 ) {
        NSUInteger contentSize = [httpResponse expectedContentLength] > 0 ? [httpResponse expectedContentLength] : 0;
        data_ = [[NSMutableData alloc] initWithCapacity:contentSize];
    } else {
        NSString* statusError  = [NSString stringWithFormat:NSLocalizedString(@"HTTP Error: %ld", nil), statusCode];
        NSDictionary* userInfo = [NSDictionary dictionaryWithObject:statusError forKey:NSLocalizedDescriptionKey];
        error_ = [[NSError alloc] initWithDomain:@"DownloadUrlOperation"
                                            code:statusCode
                                        userInfo:userInfo];
        [self done];
    }
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    // Check if the operation has been cancelled
    if([self isCancelled]) {
        [self canceled];
		return;
    }
	else {
		[self done];
	}
}

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection
                  willCacheResponse:(NSCachedURLResponse *)cachedResponse {
    return nil;
}

@end

