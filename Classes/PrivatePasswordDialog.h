//
//  PrivatePasswordDialog.h
//  ImageBankFree
//
//  Created by yoshi on 2/23/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ProtectedContentDelegate.h"


@interface PrivatePasswordDialog : NSObject<UIAlertViewDelegate,UITextFieldDelegate> {
	id delegate;
	UIView *blackWall;
	BOOL isResuming;
	UIAlertView *passwordAlert;
    BOOL isForOfflineFolders;
    BOOL isShown;
}

-(id)initWithVC:(UIViewController*)vc;
-(void) showPrivatePasswordDialog;
-(void) dismissDialog;
-(BOOL) isAlertShown;

@property (readwrite,nonatomic,assign) id delegate;
@property (readwrite,nonatomic,assign) BOOL isResuming;
@property (readwrite,nonatomic,assign) BOOL isForOfflineFolders;

@end
