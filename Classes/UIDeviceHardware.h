//
//  UIDeviceHardware.h
//  ImageBank
//
//  Created by yoshi on 9/18/12.
//
//

#import <Foundation/Foundation.h>

@interface UIDeviceHardware : NSObject
+ (NSString *) platform;
+ (NSString *) platformString;
+ (BOOL) isSlowerDevice;
+(BOOL) isiPad2OrHigher;
@end
