//
//  StoreManager.h
//  MKSync
//
//  Created by Mugunth Kumar on 17-Oct-09.
//  Copyright 2009 MK Inc. All rights reserved.
//  mugunthkumar.com

#import <Foundation/Foundation.h>
#import <StoreKit/StoreKit.h>
#import "MKStoreObserver.h"

@protocol MKStoreKitDelegate <NSObject>
- (void)productPurchaseCancelled:(NSString*) productId;
- (void)productPurchaseFailed:(SKPaymentTransaction *)transaction;
-(void)productRestoreFailed;
@optional
- (void)productPurchased:(NSString *)productId;
@end

@interface MKStoreManager : NSObject<SKProductsRequestDelegate> {

	NSMutableArray *purchasableObjects;
	MKStoreObserver *storeObserver;	
	
}

@property (nonatomic, retain) NSMutableArray *purchasableObjects;
@property (nonatomic, retain) MKStoreObserver *storeObserver;

- (void) requestProductData;

- (void) buyFullVersionUpgrade:(SKProduct*) product; // expose product buying functions, do not expose
-(void) restoreFullVersionUpgrade;

//- (void) buyFeatureB; // your product ids. This will minimize changes when you change product ids later

// do not call this directly. This is like a private method
- (void) buyFeature:(SKProduct*) product;
-(void) restoreFeature;

- (void) failedTransaction: (SKPaymentTransaction *)transaction;
-(void) failedRestore;
-(void) provideContent: (NSString*) productIdentifier shouldSerialize: (BOOL) serialize;

+ (MKStoreManager*)sharedManager;

+ (BOOL) fullVersionUpgradePurchased;
//+ (BOOL) featureBPurchased;

//DELEGATES
+(id)delegate;	
+(void)setDelegate:(id)newDelegate;

@end
