//
//  UINavigationController+Rotation.m
//  ImageBank
//
//  Created by yoshi on 9/13/12.
//
//

#import "UINavigationController+Rotation.h"

@implementation UINavigationController (Rotation)
- (NSUInteger)supportedInterfaceOrientations {
    return [[self.viewControllers lastObject] supportedInterfaceOrientations];
}

-(BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    return [[self.viewControllers lastObject] shouldAutorotateToInterfaceOrientation:toInterfaceOrientation];
}
@end
