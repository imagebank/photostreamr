//
//  FileUploadingView.m
//  ImageBankFree
//
//  Created by yoshi on 4/10/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "PopUpNotificationView.h"
#import "Utilities.h"

@implementation PopUpNotificationView

@synthesize spinner, statusMsg, doneImage, failImage, progressView;

- (id)initWithFrame:(CGRect)frame {
	if ((self = [super initWithFrame:frame])) {
		NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"PopUpNotificationView" owner:self options:nil];
		UIView *mainView = [subviewArray objectAtIndex:0];
		[self addSubview:mainView];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(flipViewAccordingToStatusBarOrientation:) name:UIApplicationDidChangeStatusBarOrientationNotification object:nil];
	}
	return self;
}

-(void)slideInPopUpView:(UIInterfaceOrientation)orientation localizedTextKey:(NSString*)textKey {
    if (![NSThread isMainThread]) 
        [self performSelectorOnMainThread:@selector(slideInPopUpView) withObject:nil waitUntilDone:NO];
    
    CGRect uploadProgressFrame = self.frame;
    originalFrame = uploadProgressFrame;
    
	// set it out of frame
	UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    
    CGAffineTransform rotationTransform = CGAffineTransformIdentity;
    if ( UIDeviceOrientationIsLandscape(orientation) ) {
		uploadProgressFrame.origin.y = 0;
		uploadProgressFrame.origin.x = - window.frame.size.height;
		if ( orientation == UIDeviceOrientationLandscapeLeft ) {
			rotationTransform = CGAffineTransformRotate(rotationTransform, DegreesToRadians(90));
		} else {
            uploadProgressFrame.origin.x = window.frame.size.width + self.frame.size.height;
            uploadProgressFrame.origin.y = window.frame.size.height;
			rotationTransform = CGAffineTransformRotate(rotationTransform, DegreesToRadians(270));
		}
		self.transform = rotationTransform;
	} else {
        if ( orientation == UIDeviceOrientationPortraitUpsideDown ) {
            rotationTransform = CGAffineTransformRotate(rotationTransform, DegreesToRadians(180));
            self.transform = rotationTransform;
            uploadProgressFrame.origin.x = window.frame.size.width;
            uploadProgressFrame.origin.y = - window.frame.size.height;
        } else {
            uploadProgressFrame.origin.y = window.frame.size.height + self.frame.size.height;
        }
	}
    
	self.frame = uploadProgressFrame;
	self.statusMsg.text = NSLocalizedString(textKey,textKey);
	[window addSubview:self];
	
	// show view indicating image being uploaded
	// todo: animate the view into position
	[UIView beginAnimations:@"showUploadingView" context:NULL];
	//	[UIView setAnimationDelay:1];
	[UIView setAnimationCurve:UIViewAnimationCurveLinear];
	[UIView setAnimationDuration:0.5];
	CGRect uploadFrame = self.frame;
	if ( UIDeviceOrientationIsLandscape(orientation) ) {
		if ( orientation == UIDeviceOrientationLandscapeRight ) {
            uploadFrame.origin.x = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ? window.frame.size.width - 74 :window.frame.size.width - 64);
		} else {
            uploadFrame.origin.x = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ? 74 :64);
		}
	} else {
        if ( orientation == UIDeviceOrientationPortraitUpsideDown ) {
            uploadFrame.origin.y = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ? 74 :64);
        } else {
            uploadFrame.origin.y = window.frame.size.height - 75;
        }
	}
	self.frame = uploadFrame;
	[UIView commitAnimations];
	[self setHidden:NO];
	[[self spinner] startAnimating];
}

-(void) hideView {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
	[UIView beginAnimations:@"hideUploadingView" context:NULL];
	[UIView setAnimationDelay:0.5];
	[UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
	[UIView setAnimationDuration:1];
	[UIView setAnimationDelegate:self];
	[UIView setAnimationDidStopSelector:@selector(slideOutPopUpFromView)];
	CGRect uploadViewFrame = self.frame;
	UIWindow *window = [[UIApplication sharedApplication] keyWindow];
	if ( UIDeviceOrientationIsLandscape(orientation) ) {
		uploadViewFrame.origin.x = - self.frame.size.height;
		if ( orientation == UIDeviceOrientationLandscapeRight ) {
            uploadViewFrame.origin.x = window.frame.size.width + self.frame.size.height;
		}
	} else {
        if ( orientation == UIDeviceOrientationPortraitUpsideDown ) {
            uploadViewFrame.origin.y = - window.frame.size.height;
        } else {
            uploadViewFrame.origin.y = window.frame.size.height + self.frame.size.height;
        }
	}
	self.frame = uploadViewFrame;
	[UIView commitAnimations];
}

-(void)slideOutPopUpFromView {
	[self removeFromSuperview];
}

- (void)flipViewAccordingToStatusBarOrientation:(NSNotification *)notification {
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    CGRect uploadFrame = originalFrame;
    TTDINFO(@"orig frame: x=%f, y=%f",originalFrame.origin.x,originalFrame.origin.y);
    TTDINFO(@"self frame: x=%f, y=%f",self.frame.origin.x,self.frame.origin.y);
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    CGAffineTransform rotationTransform = CGAffineTransformIdentity;
    switch (orientation) { 
        case UIInterfaceOrientationPortraitUpsideDown:
            rotationTransform = CGAffineTransformRotate(rotationTransform, DegreesToRadians(180));
            uploadFrame.origin.x = window.frame.size.width;
            uploadFrame.origin.y = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ? 74 :64);
            break;
        case UIInterfaceOrientationLandscapeLeft:
            uploadFrame.origin.x = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ? window.frame.size.width - 74 :window.frame.size.width - 64);
            uploadFrame.origin.y = window.frame.size.height;
            
            TTDINFO(@"upload frame origin x = %f, y = %f",uploadFrame.origin.x,uploadFrame.origin.y);
            
			rotationTransform = CGAffineTransformRotate(rotationTransform, DegreesToRadians(270));
            TTDINFO(@"upload frame origin x = %f, y = %f",uploadFrame.origin.x,uploadFrame.origin.y);
            break;
        case UIInterfaceOrientationLandscapeRight:
            uploadFrame.origin.y = 0;
            rotationTransform = CGAffineTransformRotate(rotationTransform, DegreesToRadians(90));
            uploadFrame.origin.x = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ? 74 :64);
            break;

        default: // as UIInterfaceOrientationPortrait
            uploadFrame.origin.y = window.frame.size.height - 75;
            break;
    }
    self.frame = uploadFrame;
    self.transform = rotationTransform;
    
}

- (void)dealloc {
    [spinner dealloc];
    [statusMsg dealloc];
    [doneImage dealloc];
    [failImage dealloc];
  	[super dealloc];
}

@end
