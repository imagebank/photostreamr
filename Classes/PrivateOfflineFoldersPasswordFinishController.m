//
//  PrivateOfflineFoldersPasswordFinishController.m
//  ImageBankFree
//
//  Created by Yoshi Sugawara on 5/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "PrivateOfflineFoldersPasswordFinishController.h"

@interface PrivateOfflineFoldersPasswordFinishController ()

@end

@implementation PrivateOfflineFoldersPasswordFinishController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doDone:)] autorelease];
    [self.view setBackgroundColor:[UIColor colorWithRed:211/255.0f green:211/255.0f blue:211/255.0f alpha:1.0f]];

    self.navigationItem.hidesBackButton = YES;
    
    if ( scrollView ) {
        scrollView.contentSize = CGSizeMake(self.view.bounds.size.width, (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) ? self.view.bounds.size.height + 240.0 : self.view.bounds.size.height);
    }
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

-(void)doDone:(id)sender {
    [self dismissModalViewControllerAnimated:YES];
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    if ( scrollView && !(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)) { 
        scrollView.contentSize = CGSizeMake(self.view.bounds.size.height,self.view.bounds.size.width);
    }
}

@end
