//
//  FolderContentsDataSource.m
//  ImageBankFree
//
//  Created by Yoshi Sugawara on 9/25/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "FolderContentsDataSource.h"
#import "PrivateFolderPasswordChecker.h"
#import "File.h"
#import "FolderContentsModel.h"
#import "Utilities.h"

#ifdef FREE_UPGRADABLE_VERSION
#import "MKStoreManager.h"
#endif

@implementation FolderContentsDataSource

@synthesize folders, currentDirectory;

- (void)modelDidFinishLoad:(id<TTModel>)model {
    FolderContentsModel *fcm = (FolderContentsModel*) model;
    [self setFolders:[[[NSMutableArray alloc] init] autorelease]];
    [self refreshFolders];
    [self setPhotoSource:[fcm photosModel]];
}

- (NSInteger)columnCountForView:(UIView *)view {
    CGFloat width = TTScreenBounds().size.width;
    CGFloat thumbWidth = [Utilities thumbnailWidthForScreenWidth:width];
    return floorf(width / thumbWidth);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Private


///////////////////////////////////////////////////////////////////////////////////////////////////
- (BOOL)hasMoreToLoad {
    return _photoSource.maxPhotoIndex+1 < _photoSource.numberOfPhotos;
}

-(BOOL)showFoldersSection {
    return [self folders] && [[self folders] count] > 0;
}


///////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark UITableViewDataSource


///////////////////////////////////////////////////////////////////////////////////////////////////
- (NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section {
    
    if ( section == 0 && [self showFoldersSection] ) {
        // Folders section
        return self.folders.count;
    } else {
        // Images section
        NSInteger maxIndex = _photoSource.maxPhotoIndex;
        NSInteger columnCount = [self columnCountForView:tableView];
        if (maxIndex >= 0) {
            maxIndex += 1;
            NSInteger count =  ceil((maxIndex / columnCount) + (maxIndex % columnCount ? 1 : 0));
            if (self.hasMoreToLoad) {
                return count + 1;
                
            } else {
                return count;
            }
            
        } else {
            return 0;
        }
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if ( [self showFoldersSection] && [[self photoSource] numberOfPhotos] > 0) {
        return 2;
    } else {
        return 1;
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if ( section == 0 && [self showFoldersSection] ) {
        return NSLocalizedString(@"Folders", @"Folders");
    }
    return NSLocalizedString(@"Images",@"Images");
}

- (void)tableView:(UITableView*)tableView cell:(UITableViewCell*)cell willAppearAtIndexPath:(NSIndexPath*)indexPath {
    [super tableView:tableView cell:cell willAppearAtIndexPath:indexPath];
    if (indexPath.row == [self tableView:tableView numberOfRowsInSection:self.showFoldersSection ? 1 : 0]-1 && [cell isKindOfClass:[TTTableMoreButtonCell class]]) {
        TTTableMoreButton* moreLink = [(TTTableMoreButtonCell *)cell object];
        moreLink.isLoading = YES;
        [(TTTableMoreButtonCell *)cell setAnimating:YES];
        [(TTTableMoreButtonCell *)cell setUserInteractionEnabled:NO];
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        if ( ![self.model isLoading] ) {
            TTDINFO(@"more link loading!");
            [self.model load:TTURLRequestCachePolicyDefault more:YES];
        }
    } else if ( indexPath.section == 0 && [self showFoldersSection] ) {
        UIImage *image = [UIImage imageNamed:@"cell-gradient.png"];
        UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
        cell.backgroundView = imageView;
        [imageView release];
//        cell.backgroundColor = [UIColor whiteColor];
    }
}
///////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark TTTableViewDataSource


///////////////////////////////////////////////////////////////////////////////////////////////////
- (id<TTModel>)model {
    return _model;
}



///////////////////////////////////////////////////////////////////////////////////////////////////
- (id)tableView:(UITableView*)tableView objectForRowAtIndexPath:(NSIndexPath*)indexPath {
    if ( [self showFoldersSection] && indexPath.section == 0 ) {
        // Folders section
        File *file = (File*)[self.folders objectAtIndex:indexPath.row];
        TTTableTextItem *item = [file tableItemWithUIOrientation:UIInterfaceOrientationPortrait];
        
        // disable the url for the Shuffle row for the free version and if they haven't upgraded yet
#ifdef FREE_UPGRADABLE_VERSION
        if ( [file isKindOfClass:[RandomDirectoryFile class]] && ![MKStoreManager fullVersionUpgradePurchased] ) {
            [item setURL:@"tt://"];
        }
#endif
        
        // hack the userInfo to add an entry for the current directory
        NSMutableDictionary *newUserInfo = [NSMutableDictionary dictionaryWithDictionary:(NSDictionary*)item.userInfo];
        [newUserInfo setObject:self.currentDirectory forKey:@"parent_directory"];
        [item setUserInfo:newUserInfo];
        return item;
    } else {
        // Images section
        if (indexPath.row == [self tableView:tableView numberOfRowsInSection:self.showFoldersSection ? 1 : 0]-1 && 
            self.hasMoreToLoad) {
            NSString* text = TTLocalizedString(@"Load More Photos...", @"");
            NSString* caption = nil;
            if (_photoSource.numberOfPhotos == -1) {
                caption = [NSString stringWithFormat:TTLocalizedString(@"Showing %@ Photos", @""),
                           TTFormatInteger(_photoSource.maxPhotoIndex+1)];
                
            } else {
                caption = [NSString stringWithFormat:TTLocalizedString(@"Showing %@ of %@ Photos", @""),
                           TTFormatInteger(_photoSource.maxPhotoIndex+1),
                           TTFormatInteger(_photoSource.numberOfPhotos)];
            }
            
            return [TTTableMoreButton itemWithText:text subtitle:caption];
            
        } else {
            NSInteger columnCount = [self columnCountForView:tableView];
            return [_photoSource photoAtIndex:indexPath.row * columnCount];
        }
    }
}

-(void)refreshFolders {
    [self.folders removeAllObjects];
    int i = 0;	
	
	// private folders access
	BOOL canViewPrivateFolders = [[PrivateFolderPasswordChecker sharedInstance] allowAccess];
	
    FolderContentsModel *fcm = (FolderContentsModel*) self.model;
    FileListModel *fileListModel = [fcm fileListModel];
    
	for (File *f in [fileListModel dirs]) {
		TTDINFO(@"refresh file url = %@",f.url);
		TTDINFO(@"refresh file name = %@",f.name);
        
		if ( [f isKindOfClass:[DirectoryFile class]] ) {
			DirectoryFile *dir = (DirectoryFile*) f;
			if ( [dir isPrivate] && !canViewPrivateFolders ) {
				continue;
			}
            TTDINFO(@"adding folder!");
            [self.folders addObject:f];
		}
		if ( ![f.name isEqualToString:@"__imagebank_random_images"] ) {
			i++;
		}
		
	}
}

-(void)dealloc {
    [folders release];
    [currentDirectory release];
    [super dealloc];
}
@end
