//
//  HelpPopOverController.h
//  ImageBankFree
//
//  Created by Yoshi Sugawara on 2/5/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PopOverContentDelegate.h"

@interface HelpPopOverController : UITableViewController<UIAlertViewDelegate> {
    NSObject<PopOverContentDelegate> *delegate;
    BOOL isFromOfflineFoldersController;
}



@property (nonatomic, retain) NSObject<PopOverContentDelegate> *delegate;
@property (nonatomic, assign) BOOL isFromOfflineFoldersController;

-(IBAction)sortOrderChanged:(id) sender;

@end
