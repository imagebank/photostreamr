//
//  PrivateFolderPasswordChecker.m
//  ImageBankFree
//
//  Created by yoshi on 2/23/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "PrivateFolderPasswordChecker.h"
#import "Utilities.h"


@implementation PrivateFolderPasswordChecker

@synthesize hashedPFPassword, allowAccess, allowOfflineAccess, dateWentIntoBackground;

static PrivateFolderPasswordChecker *sharedInstance = nil;

// Get the shared instance and create it if necessary.
+ (PrivateFolderPasswordChecker*)sharedInstance {
    if (sharedInstance == nil) {
        sharedInstance = [[super allocWithZone:NULL] init];
    }
	
    return sharedInstance;
}

// We don't want to allocate a new instance, so return the current one.
+ (id)allocWithZone:(NSZone*)zone {
    return [[self sharedInstance] retain];
}

// Equally, we don't want to generate multiple copies of the singleton.
- (id)copyWithZone:(NSZone *)zone {
    return self;
}

// Once again - do nothing, as we don't have a retain counter for this object.
- (id)retain {
    return self;
}

-(BOOL)canAccessWithHashedPassword:(NSString*)hashedPassword {
	if ( self.hashedPFPassword == nil ) {
		[self setAllowAccess:YES];
		return self.allowAccess;
	}
	if ( [self.hashedPFPassword isEqualToString:hashedPassword] ) {
		[self setAllowAccess:YES];
		return self.allowAccess;
	}
	[self setAllowAccess:NO];
	return self.allowAccess;
}

-(BOOL)canAccessOfflineWithPassword:(NSString*)password {
    NSUserDefaults *settings = [NSUserDefaults standardUserDefaults];
    NSString *storedPassword = [settings objectForKey:@"offline_private_password"];
    if ( storedPassword == nil ) {
        [self setAllowOfflineAccess:YES];
        return self.allowOfflineAccess;
    }
    if ( [storedPassword isEqualToString:password] ) {
        [self setAllowOfflineAccess:YES];
        return self.allowOfflineAccess;
    }
    [self setAllowOfflineAccess:NO];
    return self.allowOfflineAccess;
}

- (BOOL)showButton {
	return hashedPFPassword != nil && ![hashedPFPassword isEqualToString:@""];
}

- (BOOL) shouldCheckAccess {
    if ( self.dateWentIntoBackground == nil ) {
        return YES;
    }
    NSTimeInterval secondsBetween = [[NSDate date] timeIntervalSinceDate:self.dateWentIntoBackground];
    if ( secondsBetween <= 2 ) {
        return NO;
    }
    return YES;
}


// Replace the retain counter so we can never release this object.
- (NSUInteger)retainCount {
    return NSUIntegerMax;
}

// This function is empty, as we don't want to let the user release this object.
- (oneway void)release {
	
}

//Do nothing, other than return the shared instance - as this is expected from autorelease.
- (id)autorelease {
    return self;
}

@end
