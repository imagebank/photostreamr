//
//  MovableViewWithInsetView.h
//  ImageBankFree
//
//  Created by Yoshi Sugawara on 4/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MovableViewWithInsetView : UIView {
    CGPoint currentPoint;
    IBOutlet UIView *insetView;
    IBOutlet UIImageView *closeButtonView;
    IBOutlet UIView *backgroundView;
}

@property (nonatomic,retain) IBOutlet UIView *insetView;
@property (nonatomic,retain) IBOutlet UIImageView *closeButtonView;
@property (nonatomic,retain) IBOutlet UIView *backgroundView;

@end
