//
//  FilesListDataSource.m
//  ImageBank
//
//  Created by yoshi on 5/13/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "FileListDataSource.h"
#import "FileListModel.h"
#import "File.h"
#import "PrivateFolderPasswordChecker.h"

@implementation FileListDataSource

@synthesize uiOrientation;

- (void)tableViewDidLoadModel:(UITableView *)tableView
{
    [super tableViewDidLoadModel:tableView];
		
    TTDINFO(@"Removing all objects in the table view.");
    [self refreshItems];
}

-(void)refreshItems {
	[self.items removeAllObjects];
	int i = 0;	
	[self setItems:[NSMutableArray array]];
	
	// private folders access
	BOOL canViewPrivateFolders = [[PrivateFolderPasswordChecker sharedInstance] allowAccess];
	
	for (File *f in [(FileListModel *) self.model files]) {
		TTDINFO(@"file url = %@",f.url);
		TTDINFO(@"file name = %@",f.name);

		if ( [f isKindOfClass:[DirectoryFile class]] ) {
			DirectoryFile *dir = (DirectoryFile*) f;
			if ( [dir isPrivate] && !canViewPrivateFolders ) {
				continue;
			}
		} else if ( [f isKindOfClass:[ImageFile class]] ) {
			ImageFile *imageFile = (ImageFile*) f;
			[imageFile setDirectory:[(FileListModel*)self.model directory]];
			f = imageFile;
		}
		
		[self.items addObject:[f tableItemWithUIOrientation:self.uiOrientation]];
		if ( ![f.name isEqualToString:@"__imagebank_random_images"] ) {
			i++;
		}
		
	}
}

// added so that the section header would not appear (ios5 change)
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return @"";
}

@end
