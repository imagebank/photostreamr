//
//  FolderContentsModel.h
//  ImageBankFree
//
//  Created by Yoshi Sugawara on 9/26/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <Three20/Three20.h>
#import "FileListModel.h"
#import "PhotoSource.h"

@interface FolderContentsModel : TTModel {
    FileListModel *fileListModel;
    PhotoSource *photosModel;
    BOOL isRootDirectory;
    
}

@property(nonatomic,retain) FileListModel *fileListModel;
@property(nonatomic,retain) PhotoSource *photosModel;
@property(nonatomic,assign) BOOL isRootDirectory;

-(id)initWithTitle:(NSString*)title directory:(DirectoryFile*)dir isOffline:(BOOL)isOffline;

@end
