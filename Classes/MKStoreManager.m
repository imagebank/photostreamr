//
//  MKStoreManager.m
//
//  Created by Mugunth Kumar on 15-Nov-09.
//  Copyright 2009 Mugunth Kumar. All rights reserved.
//  mugunthkumar.com
//

#import "MKStoreManager.h"


@implementation MKStoreManager

@synthesize purchasableObjects;
@synthesize storeObserver;

// all your features should be managed one and only by StoreManager
static NSString *fullVersionUpgradeId = @"full_version_upgrade";
//static NSString *featureBId = @"com.mycompany.myapp.myfeatureb";

BOOL fullVersionUpgradePurchased;
//BOOL featureBPurchased;

static __weak id<MKStoreKitDelegate> _delegate;
static MKStoreManager* _sharedStoreManager; // self

- (void)dealloc {
	
	[_sharedStoreManager release];
	[storeObserver release];
	[super dealloc];
}

+ (id)delegate {
	
    return _delegate;
}

+ (void)setDelegate:(id)newDelegate {
	
    _delegate = newDelegate;	
}

+ (BOOL) fullVersionUpgradePurchased {
	
	return fullVersionUpgradePurchased;
}

//+ (BOOL) featureBPurchased {
//	
//	return featureBPurchased;
//}

+ (MKStoreManager*)sharedManager
{
	@synchronized(self) {
		
        if (_sharedStoreManager == nil) {
			
            [[self alloc] init]; // assignment not done here
			_sharedStoreManager.purchasableObjects = [[NSMutableArray alloc] init];			
//			[_sharedStoreManager requestProductData];
			
			NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];	
			fullVersionUpgradePurchased = [userDefaults boolForKey:fullVersionUpgradeId]; 
			
			_sharedStoreManager.storeObserver = [[MKStoreObserver alloc] init];
			[[SKPaymentQueue defaultQueue] addTransactionObserver:_sharedStoreManager.storeObserver];
        }
    }
    return _sharedStoreManager;
}


#pragma mark Singleton Methods

+ (id)allocWithZone:(NSZone *)zone

{	
    @synchronized(self) {
		
        if (_sharedStoreManager == nil) {
			
            _sharedStoreManager = [super allocWithZone:zone];			
            return _sharedStoreManager;  // assignment and return on first allocation
        }
    }
	
    return nil; //on subsequent allocation attempts return nil	
}


- (id)copyWithZone:(NSZone *)zone
{
    return self;	
}

- (id)retain
{	
    return self;	
}

- (unsigned)retainCount
{
    return UINT_MAX;  //denotes an object that cannot be released
}

- (void)release
{
    //do nothing
}

- (id)autorelease
{
    return self;	
}


- (void) requestProductData
{
	SKProductsRequest *request= [[SKProductsRequest alloc] initWithProductIdentifiers: 
								 [NSSet setWithObjects: fullVersionUpgradeId, nil]];
	request.delegate = self;
	[request start];
}


- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response
{
	[purchasableObjects addObjectsFromArray:response.products];
	// populate UI
	for(int i=0;i<[purchasableObjects count];i++)
	{
		
		SKProduct *product = [purchasableObjects objectAtIndex:i];
		NSLog(@"Feature: %@, Cost: %f, ID: %@",[product localizedTitle],
			  [[product price] doubleValue], [product productIdentifier]);
	}
	
	[request autorelease];
}

//- (void) buyFeatureB
//{
//	[self buyFeature:featureBId];
//}

-(void) showUnableToPurchaseAlert {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Cannot purchase upgrade",
                                                                              @"Alert title when tapping Upgrade button but not able to make payments")
                                                    message:NSLocalizedString(@"Sorry, you are not authorized to purchase from the App Store",
                                                                              @"Sorry, you are not authorized to purchase from the App Store")
                                                   delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [alert show];
    [alert release];
}

- (void) buyFeature:(SKProduct*) product
{
	if ([SKPaymentQueue canMakePayments])
	{
		SKPayment *payment = [SKPayment paymentWithProduct:product];
		[[SKPaymentQueue defaultQueue] addPayment:payment];
	}
	else
	{
        [self showUnableToPurchaseAlert];
	}
}

-(void) restoreFeature
{
    if ([SKPaymentQueue canMakePayments])
	{
        [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
    }
    else
    {
        [self showUnableToPurchaseAlert];
    }
}

							 
- (void) buyFullVersionUpgrade:(SKProduct*) product
{
	[self buyFeature:product];
}

-(void) restoreFullVersionUpgrade
{
    [self restoreFeature];
}


- (void) failedTransaction: (SKPaymentTransaction *)transaction
{
	[_delegate productPurchaseFailed:transaction];
}

-(void) failedRestore {
    [_delegate productRestoreFailed];
}

-(void) provideContent: (NSString*) productIdentifier shouldSerialize: (BOOL) serialize
{
	NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
	if([productIdentifier isEqualToString:fullVersionUpgradeId])
	{
		fullVersionUpgradePurchased = YES;
		if(serialize)
		{
			if([_delegate respondsToSelector:@selector(productPurchased:)])
				[_delegate productPurchased:productIdentifier];

			[userDefaults setBool:fullVersionUpgradePurchased forKey:fullVersionUpgradeId];		
		}
	}

//	if([productIdentifier isEqualToString:featureBId])
//	{
//		featureBPurchased = YES;
//		if(serialize)
//		{
//			if([_delegate respondsToSelector:@selector(productPurchased:)])
//				[_delegate productPurchased:productIdentifier];
//			
//			[userDefaults setBool:featureBPurchased forKey:featureBId];		
//		}
//	}
}


@end
