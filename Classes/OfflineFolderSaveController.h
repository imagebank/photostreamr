//
//  OfflineFolderSaveController.h
//  ImageBankFree
//
//  Created by Yoshi Sugawara on 7/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FolderContentsModel.h"
#import "FolderContentsController.h"

@interface OfflineFolderSaveController : UITableViewController<UITextFieldDelegate,UIAlertViewDelegate> {
    NSMutableArray *existingFolders;
    FolderContentsModel *folderContentsModel;
    DirectoryFile *parentDirectory;
    BOOL isDownloadingAll;
    FolderContentsController *parentVC;
    UIAlertView *alertView;
    PrivatePasswordDialog *privatePasswordDialog;
    UIBarButtonItem *privateFoldersAccessButton;
    UIImageView *lockFlashView;
    DirectoryFile *lastSavedFolder;
    BOOL isSavingFromPhotoController;
}

@property (nonatomic,readwrite,retain) NSMutableArray *existingFolders;
@property (nonatomic,readwrite,retain) FolderContentsModel *folderContentsModel;
@property (nonatomic,readwrite,retain) DirectoryFile *parentDirectory;
@property (nonatomic,readwrite,assign) BOOL isDownloadingAll;
@property (nonatomic,readwrite,retain) FolderContentsController *parentVC;
@property (nonatomic,readwrite,retain) UIAlertView *alertView;
@property (readwrite,nonatomic,retain) UIBarButtonItem *privateFoldersAccessButton;
@property (readwrite,nonatomic,retain) DirectoryFile *lastSavedFolder;
@property (readwrite,nonatomic,assign) BOOL isSavingFromPhotoController;

@end
