//
//  AddServerController.m
//  PhotoStreamr
//
//  Created by yoshi on 6/13/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "AddServerController.h"
#import "Server.h"
#import "Constants.h"
#import "HTMLHelpController.h"
#import "Utilities.h"

@implementation AddServerController

@synthesize nameField, addressField, portField, pinField, managedObjectContext;


- (id)initWithNavigatorURL:(NSURL*)URL query:(NSDictionary*)query {
    addByPIN = NO;
    
    NSString *nibName = 
    (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) ? @"AddServerController-iPad" : @"AddServerController_NO_iAD";

    if ( [(NSString*)[query objectForKey:@"add_by"] isEqualToString:@"pin"] ) {
        addByPIN = YES;
        nibName = 
        (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) ? 
            @"AddServerControllerUsingPIN-iPad" :
            @"AddServerControllerUsingPIN";
    }
    
	if ( self = [self initWithNibName:nibName
							   bundle:nil] ) {
		self.title = NSLocalizedString(@"Add a Server",@"Add a Server View Title");
		[self setManagedObjectContext:[query objectForKey:@"managedObjectContext"]];
	}
	return self;
}

/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        // Custom initialization
    }
    return self;
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	nameField.delegate = self;
	addressField.delegate = self;
	portField.delegate = self;

    if ( addByPIN ) {
        UIBarButtonItem *remoteHelpButton = [[UIBarButtonItem alloc] 
                                    initWithTitle:NSLocalizedString(@"Help",@"Help")
                                    style:UIBarButtonItemStylePlain target:self action:@selector(showHelp:)];
        self.navigationItem.rightBarButtonItem = remoteHelpButton;
        self.navigationItem.rightBarButtonItem.enabled = YES;
    }
    
    [self.view setBackgroundColor:[UIColor colorWithRed:211/255.0f green:211/255.0f blue:211/255.0f alpha:1.0f]];
    self.navigationBarTintColor = [Utilities globalAppTintColor];
    
}

-(NSUInteger)supportedInterfaceOrientations {
	if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
		return UIInterfaceOrientationMaskAll;
	} else {
		return UIInterfaceOrientationMaskPortrait;
	}
}

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
	if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        return YES;
    }
    return UIInterfaceOrientationIsPortrait(toInterfaceOrientation);
}


-(void)showHelp:(id)sender {
    HTMLHelpController *helpController = [[[HTMLHelpController alloc] init] autorelease];
    [helpController setHtmlFilenamePrefix:@"help_remote"];
    [helpController setPresentedAsModal:YES];
    UINavigationController *navwrap = [[[UINavigationController alloc] initWithRootViewController:helpController] autorelease];
    [self presentModalViewController:navwrap
                            animated:YES];
}

-(void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
	[self.navigationController setToolbarHidden:YES];
}

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}

#pragma mark
#pragma mark UITextFieldDelegate 
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
	switch (textField.tag) {
		case kNameFieldTag:
			[addressField becomeFirstResponder];
			return YES;
			break;
		case kAddressFieldTag:
			[portField becomeFirstResponder];
			return YES;
			break;
		case kPortFieldTag:
			[self createServer:textField];
			return NO;
			break;
		default:
			[textField resignFirstResponder];
			break;
	}
	return NO;
}

#pragma mark
#pragma mark UI Actions
-(IBAction)createServer:(id)sender {
	NSString *name = nameField.text;
	NSString *address = addressField.text;
	NSString *port = portField.text;
    NSString *pin = pinField.text;
	if ( [name length] == 0 ) {
		[self showAlertWithTitle:NSLocalizedString(@"Missing Required Information",@"Error Alert Title")
							 msg:NSLocalizedString(@"Please enter the server name",@"Missing server name message")];
		[nameField becomeFirstResponder];
		return;
	}
	if ( !addByPIN && [address length] == 0 ) {
		[self showAlertWithTitle:NSLocalizedString(@"Missing Required Information",@"Error Alert Title")
							 msg:NSLocalizedString(@"Please enter the server address",@"Missing server address message")];
		[addressField becomeFirstResponder];
		return;
	}
	if ( !addByPIN && [port length] == 0 ) {
		[self showAlertWithTitle:NSLocalizedString(@"Missing Required Information",@"Error Alert Title")
							 msg:NSLocalizedString(@"Please enter the server port",@"Missing server port message")];
		[portField becomeFirstResponder];
		return;
	}
    if ( addByPIN && [pin length] == 0) {
		[self showAlertWithTitle:NSLocalizedString(@"Missing Required Information",@"Error Alert Title")
							 msg:NSLocalizedString(@"Please enter the remote server ID code",@"Missing PIN code message")];
        [pinField becomeFirstResponder];
        return;
    }
	Server *server = (Server *)[NSEntityDescription insertNewObjectForEntityForName:@"Server" inManagedObjectContext:self.managedObjectContext];
	[server setName:name];
	[server setUrl:[NSString stringWithFormat:@"%@:%@",address,port]];
    [server setPin:pin];
	NSError *error;	
	if (![self.managedObjectContext save:&error]) {
		// Handle the error.
		NSLog(@"Unresolved error %@, %@, %@", error, [error userInfo],[error localizedDescription]);
	}
	[self.navigationController popToRootViewControllerAnimated:YES];
}

-(void)showAlertWithTitle:(NSString*)title msg:(NSString*)msg {
	UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:title
														message:msg
													   delegate:self 
											  cancelButtonTitle:@"OK" 
											  otherButtonTitles:nil];
	[alertView show];
	[alertView release];
}


- (void)dealloc {
	[nameField release];
	[addressField release];
	[portField release];
    [super dealloc];
}


@end
