//
//  OfflineFoldersController.h
//  ImageBankFree
//
//  Created by Yoshi Sugawara on 5/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Three20/Three20.h>
#import <UIKit/UIKit.h>
#import "PopOverContentDelegate.h"
#import "IASKAppSettingsViewController.h"
#import "ProtectedContentDelegate.h"
#import "PrivatePasswordDialog.h"
#import "PPRevealSideViewController.h"

@interface OfflineFoldersController : UIViewController<PPRevealSideViewControllerDelegate, PopOverContentDelegate,IASKSettingsDelegate,ProtectedContentDelegate,UITableViewDelegate,UITableViewDataSource>{
    NSMutableArray *offlineFolders;
    NSManagedObjectContext *managedObjContext;
    // Keep a reference to this so the icon image can be modified elsewhere
    UIBarButtonItem *privateFoldersAccessButton;
    UIImageView *lockFlashView;
    PrivatePasswordDialog *privatePasswordDialog;
    UIBarButtonItem *changePasswordButton;
    IBOutlet UITableView *tableView;
    IBOutlet UIView *noFoldersView;
    IBOutlet UILabel *savedFoldersDescription;
    IBOutlet UILabel *savedFoldersStorageDescription;
    IBOutlet UIScrollView *scrollView;
    
    NSString *sortByField;
    BOOL sortAscending;
}

-(void)updatePrivateFoldersAccessButtonStatus;

@property(nonatomic,readwrite,retain) NSMutableArray *offlineFolders;
@property(nonatomic,readwrite,retain) NSManagedObjectContext *managedObjContext;
@property (readwrite,nonatomic,retain) UIBarButtonItem *privateFoldersAccessButton;
@property (readwrite,nonatomic,retain) UIBarButtonItem *changePasswordButton;
@property (readwrite,nonatomic,retain) UIView *noFoldersView;
@property (readwrite,nonatomic,retain) UILabel *savedFoldersDescription;
@property (readwrite,nonatomic,retain) UILabel *savedFoldersStorageDescription;
@property (readwrite,nonatomic,retain) UITableView *tableView;
@property (readwrite,nonatomic,retain) UIScrollView *scrollView;
@property (nonatomic,retain) NSString *sortByField;

@end
