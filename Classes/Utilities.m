//
//  Utilities.m
//  ImageBank
//
//  Created by yoshi on 6/6/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "Utilities.h"
#import "PrivateFolderPasswordChecker.h"
#import "File.h"

@implementation Utilities

CGFloat DegreesToRadians(CGFloat degrees) {return degrees * M_PI / 180;};

//generate md5 hash from string
+ (NSString *) returnMD5Hash:(NSString*)concat {
    const char *concat_str = [concat UTF8String];
    unsigned char result[CC_MD5_DIGEST_LENGTH];
    CC_MD5(concat_str, strlen(concat_str), result);
    NSMutableString *hash = [NSMutableString string];
    for (int i = 0; i < 16; i++)
        [hash appendFormat:@"%02X", result[i]];
    return [hash lowercaseString];
	
}

+(NSString *) hashedSecret:(NSString*)secret {
	return [self returnMD5Hash:[NSString stringWithFormat:@"%@%@",secret,@"jhr3j13"]];
}


+(BOOL) isUpgradedVersion {
	// change this to get the purchase identifier and a hash of the identifier for a little better security?
	NSString* upgraded = (NSString*) [[NSUserDefaults standardUserDefaults] stringForKey:@"upgraded"];
	return [upgraded isEqualToString:@"YES"];
}

// Encode a string to embed in an URL.
+(NSString*) encodeToPercentEscapeString:(NSString *)string {
	return (NSString *)
	CFURLCreateStringByAddingPercentEscapes(NULL,
											(CFStringRef) string,
											NULL,
											(CFStringRef) @"!*'();:@&=+$,/?%#[]",
											kCFStringEncodingUTF8);
}

+(NSString*)getLocalizedHtmlFilenameUsingPrefix :(NSString*)filePrefix {
	// get current language
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	NSArray *languages = [defaults objectForKey:@"AppleLanguages"];
	NSString *currentLanguage = [languages objectAtIndex:0];
	TTDINFO(@"Current language: %@", currentLanguage);
	NSString *filename = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%@_%@",filePrefix,currentLanguage] ofType:@"html"];
	if ( [[NSFileManager defaultManager] fileExistsAtPath:filename] )
		return filename;
	return [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%@",filePrefix] ofType:@"html"];
}

+(BOOL)isOfflineFolder:(NSString*)path {
    NSFileManager *fm = [NSFileManager defaultManager];
    BOOL isDir;
    NSString *modeljson = [path stringByAppendingPathComponent:@"model.json"];
    if ( [fm fileExistsAtPath:path isDirectory:&isDir] && isDir && [fm fileExistsAtPath:modeljson] ) {
        return YES;
    }
    return NO;
}

+(void)setOfflinePrivateFoldersPassword:(NSString*)newPw {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setValue:newPw forKey:@"offline_private_password"];
}

+(NSString*)getOfflinePrivateFoldersPassword {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    return [userDefaults stringForKey:@"offline_private_password"];
}

+(BOOL)isOfflinePrivateFoldersPasswordSetup {
    NSString *pw = [Utilities getOfflinePrivateFoldersPassword];
    if ( pw != nil && ![pw isEqualToString:@""] ) {
        return YES;
    }
    return NO;
}

+(BOOL)isOfflinePrivateFolder:(NSString*)path {
    NSFileManager *fm = [NSFileManager defaultManager];
    BOOL isDir;
    if ( [Utilities isOfflineFolder:path] && [fm fileExistsAtPath:path isDirectory:&isDir] && isDir ) {
        // check if private folder
        NSString *privateFolderFile = [path stringByAppendingPathComponent:@"private"];
        return [fm fileExistsAtPath:privateFolderFile];
    }
    return NO;
}

+(void)enableOfflinePrivateFolder:(NSString *)fullPath {
    NSString *privateFolderFile = [fullPath stringByAppendingPathComponent:@"private"];
    NSString *contents = @"YES";
    NSError *writeError = nil;
    [contents writeToFile:privateFolderFile atomically:YES encoding:NSUTF8StringEncoding error:&writeError];
}

+(void)disableOfflinePrivateFolder:(NSString*) fullPath {
    NSString *privateFolderFile = [fullPath stringByAppendingPathComponent:@"private"];
    NSFileManager *fm = [NSFileManager defaultManager];
    NSError *error = nil;
    if ( [fm fileExistsAtPath:privateFolderFile] ) {
        [fm removeItemAtPath:privateFolderFile error:&error];
    }
}

+(NSMutableArray*) offlineFolders {
    // read directories
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES); 
    NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
    NSFileManager *fm = [NSFileManager defaultManager];
    NSError *error = nil;
    NSMutableArray *contentWithFullPaths = [[[NSMutableArray alloc] init] autorelease];
    for (NSString *file in [fm contentsOfDirectoryAtPath:documentsDirectory error:&error]) {
        NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:file];
        BOOL isPrivate = [Utilities isOfflinePrivateFolder:fullPath];
        if ( isPrivate && ![[PrivateFolderPasswordChecker sharedInstance] allowOfflineAccess] ) {
            continue;
        }
        if ( [Utilities isOfflineFolder:fullPath] ) {
            DirectoryFile *dir = [[DirectoryFile alloc] initWithName:fullPath url:@"" imageCount:0 dirCount:0];
            [dir setIsPrivate:isPrivate];
            NSDictionary* fileAttribs = [[NSFileManager defaultManager] attributesOfItemAtPath:fullPath error:nil];
            [dir setModifiedDate:[fileAttribs fileModificationDate]];
            [contentWithFullPaths addObject:dir];
        }
    }
    return contentWithFullPaths;
}


+(NSString*)savedPasswordForServerUrl:(NSString*) serverUrl {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *savedServerPasswords = [defaults dictionaryForKey:@"SavedPasswords"];
    NSString *password = [savedServerPasswords objectForKey:serverUrl];
    TTDINFO(@"stored password for %@ = %@",serverUrl,password);
    return password;
}

+(void) storePassword:(NSString*)password forServerUrl:(NSString*)serverUrl {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *savedServerPasswords = [defaults dictionaryForKey:@"SavedPasswords"];
    NSMutableDictionary *newSavedPasswords = savedServerPasswords == nil ? [NSMutableDictionary dictionary] : [savedServerPasswords mutableCopy];
    TTDINFO(@"saving password = %@ for server: %@",password,serverUrl);
    [newSavedPasswords setObject:password forKey:serverUrl];
    [defaults setObject:newSavedPasswords forKey:@"SavedPasswords"];
    [defaults synchronize];
}

+(void) destroySavedPasswords {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[NSDictionary dictionary] forKey:@"SavedPasswords"];
    [defaults synchronize];
}


+(DirectoryFile*) lastSavedFolder {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *name = [defaults stringForKey:@"last_saved_name"];
    BOOL isPrivate = [defaults boolForKey:@"last_saved_is_private"];
    
    if ( [name length] == 0 ) {
        return nil;
    }
    
    DirectoryFile *dir = [[[DirectoryFile alloc] init] autorelease];
    [dir setName:name];
    [dir setIsPrivate:isPrivate];
    return dir;
}

+(void) storeLastSavedFolder:(NSString *)folderName isPrivate:(BOOL)isPrivate {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    TTDINFO(@"saving last saved folder: %@ , isPrivate = %@",folderName,isPrivate ? @"yes" : @"no");

    [defaults setObject:folderName forKey:@"last_saved_name"];
    [defaults setBool:isPrivate forKey:@"last_saved_is_private"];
    [defaults synchronize];
}

+(NSDictionary*) folderSortOptions {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *savedFolderOptions = [defaults dictionaryForKey:@"FolderOptions"];
    if ( savedFolderOptions == nil ) {
        return [NSDictionary dictionaryWithObjectsAndKeys:@"name",@"sort_by_field",@"yes",@"is_asc", nil];
    }
    return savedFolderOptions;
}

+(void) storeFolderSortOptionsWithField:(NSString*)field isAscending:(BOOL)isAsc {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *savedFolderOptions = [defaults dictionaryForKey:@"FolderOptions"];
    NSMutableDictionary *newSavedFolderOptions = savedFolderOptions == nil ? [NSMutableDictionary dictionary] : [savedFolderOptions mutableCopy];
    [newSavedFolderOptions setObject:field forKey:@"sort_by_field"];
    [newSavedFolderOptions setObject:(isAsc ? @"yes" : @"no") forKey:@"is_asc"];
    TTDINFO(@"storing sort field: %@ , is asc = %@",field,isAsc ? @"yes" : @"no");
    [defaults setObject:newSavedFolderOptions forKey:@"FolderOptions"];
    [defaults synchronize];    
}

+(CGFloat) thumbnailWidthForScreenWidth:(CGFloat)width {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    BOOL largeSize = [defaults boolForKey:@"pref_large_size"];
    if ( width == 320.0 ) {
        // Portrait iPhone: all models
        return largeSize ? 100.0 : 75;
    } else if ( width == 568.0 ) {
        // Landscape iPhone / iPod Touch with 4" Display
        return largeSize ? 108 : 76.0;
    } else if ( width == 480.0 ) {
        // Landscape iPhone 3.5" Display
        return largeSize ? 114.0 : 75;
    } else if ( width == 768.0 ) {
        // Portrait iPad
        return largeSize ? 187.0 : 123.0;
    } else if ( width == 1024.0 ) {
        // Landscape iPad
        return largeSize ? 200.0 : 123.0;
    } else {
        // unknown
        TTDINFO(@"Unsupported screen width %f, defaulting to 75.0",width);
        return 75.0;
    }
}

+(CGFloat) thumbnailSpacingForScreenWidth:(CGFloat)width {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    BOOL largeSize = [defaults boolForKey:@"pref_large_size"];
    if ( width == 320.0 ) {
        // Portrait iPhone: all models
        return largeSize ? 5.0 : 4.0;
    } else if ( width == 568.0 ) {
        // Landscape iPhone / iPod Touch with 4" Display
        return largeSize ? 4.0 : 5.0;
    } else if ( width == 480.0 ) {
        // Landscape iPhone 3.5" Display
        return 4.0;
    } else if ( width == 768.0 || width == 1024.0 ) {
        // Portrait or Landscape iPad
        return 4.0;
    } else {
        // unknown
        TTDINFO(@"Unsupported screen width %f, defaulting to 4.0",width);
        return 4.0;
    }
}

+(NSUInteger) thumbnailCountForScreenWidth:(CGFloat)width {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    BOOL largeSize = [defaults boolForKey:@"pref_large_size"];
    if ( width == 320.0 ) {
        // Portrait iPhone: all models
        return largeSize ? 3 : 4;
    } else if ( width == 568.0 ) {
        // Landscape iPhone / iPod Touch with 4" Display
        return 7;
    } else if ( width == 480.0 ) {
        // Landscape iPhone 3.5" Display
        return largeSize ? 4 : 6;
    } else if ( width == 768.0 ) {
        // Portrait iPad
        return largeSize ? 4 : 6;
    } else if ( width == 1024.0 ) {
        // Landscape iPad
        return largeSize ? 5 : 8;
    } else {
        // unknown
        TTDINFO(@"Unsupported screen width %f, defaulting to 4",width);
        return 4;
    }
}

+ (NSString *)contentTypeForImageData:(NSData *)data {
    uint8_t c;
    [data getBytes:&c length:1];
    
    switch (c) {
        case 0xFF:
            return @"image/jpeg";
        case 0x89:
            return @"image/png";
        case 0x47:
            return @"image/gif";
        case 0x49:
        case 0x4D:
            return @"image/tiff";
    }
    return nil;
}

+(UIColor*) globalAppTintColor {
//  return [UIColor colorWithRed:0.99f green:0.64f blue:0.18f alpha:1.0];
    return [UIColor redColor];
}

@end
