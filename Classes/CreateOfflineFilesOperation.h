//
//  CreateOfflineFilesOperation.h
//  ImageBankFree
//
//  Created by Yoshi Sugawara on 5/14/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PopUpNotificationView.h"
#import "FileListModel.h"
#import "PhotoSource.h"
#import "File.h"

@interface CreateOfflineFilesOperation : NSOperation {
    // In concurrent operations, we have to manage the operation's state
    BOOL executing_;
    BOOL finished_;

    FileListModel *model;
    NSArray *selectedImageFiles;
    
    NSMutableArray *fileData;
    NSString *pathToDocumentsDirectory;
    NSString *pathToTempDirectory;
    
    NSString *directoryName;
        
    // growl-like notification support
    PopUpNotificationView *popUpView;
    UIInterfaceOrientation orientation;

    BOOL saveToCameraRoll;
}

@property (nonatomic,readonly) NSError* error;
@property (nonatomic,readwrite,copy) FileListModel *model;
@property (nonatomic,readwrite,retain) NSArray *selectedImageFiles;
@property (nonatomic,readwrite,retain) NSMutableArray *fileData;
@property (nonatomic,readwrite,retain) 	PopUpNotificationView *popUpView;
@property (nonatomic,readwrite,assign)	UIInterfaceOrientation orientation;
@property (nonatomic,readwrite,retain) NSString *pathToDocumentsDirectory;
@property (nonatomic,readwrite,retain) NSString *pathToTempDirectory;
@property (nonatomic,readwrite,assign) BOOL saveToCameraRoll;
@property (nonatomic,readwrite,retain) NSString *directoryName;

-(id)initWithFileListModel:(FileListModel*)fileListModel withOrientation:(UIInterfaceOrientation)orient withDirName:(NSString*)dirName;
-(id)initWithFileListModel:(FileListModel *)fileListModel withPhotoSource:(PhotoSource*)photoSource withOrientation:(UIInterfaceOrientation)orient withDirName:(NSString*)dirName;
-(void)setup;
-(void)slideInPopUpView;
-(void) hideView;
-(void)updateStatusMessageSuccess;
-(void)updateStatusMessageFail;
-(void) generateFiles;
-(void)updateProgress:(NSNumber*)progress;

@end
