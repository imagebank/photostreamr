//
//  PrivateOfflineFoldersPasswordConfirmController.m
//  ImageBankFree
//
//  Created by Yoshi Sugawara on 5/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "PrivateOfflineFoldersPasswordConfirmController.h"
#import "Utilities.h"
#import "PrivateOfflineFoldersPasswordFinishController.h"

@implementation PrivateOfflineFoldersPasswordConfirmController

@synthesize password;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = NSLocalizedString(@"Private Folders",@"");
    [self.view setBackgroundColor:[UIColor colorWithRed:211/255.0f green:211/255.0f blue:211/255.0f alpha:1.0f]];
    
    if ( scrollView ) {
        scrollView.contentSize = CGSizeMake(self.view.bounds.size.width, (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) ? self.view.bounds.size.height + 200.0 : self.view.bounds.size.height);
    }
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    if ( scrollView && !(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)) { 
        scrollView.contentSize = CGSizeMake(self.view.bounds.size.height,self.view.bounds.size.width);
    }
}

-(IBAction)verifyAndSavePassword:(id)sender {
    if ( ![password isEqualToString:passwordConfirmField.text] ) {
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Password Confirm Failed", @"")
                                                            message:NSLocalizedString(@"Your passwords must match.",@"")
                                                           delegate:self
                                                  cancelButtonTitle:@"OK" 
                                                  otherButtonTitles:nil];
        [alertView show];
        [alertView release];
        [passwordConfirmField becomeFirstResponder];
        return;
    }
    [Utilities setOfflinePrivateFoldersPassword:password];
    PrivateOfflineFoldersPasswordFinishController *finishController = [[[PrivateOfflineFoldersPasswordFinishController alloc] initWithNibName:@"PrivateOfflineFoldersPasswordFinishController" bundle:nil] autorelease];
    [self.navigationController pushViewController:finishController animated:YES];
}


#pragma mark
#pragma mark UITextFieldDelegate 
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self verifyAndSavePassword:textField];
    return NO;
}

@end
