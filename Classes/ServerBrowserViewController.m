//
//  ServerBrowserViewController.m
//  ImageBank
//
//  Created by yoshi on 5/29/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "ServerBrowserViewController.h"
#import "Server.h"
#import "Utilities.h"

#ifdef FREE_UPGRADABLE_VERSION
#import "MKStoreManager.h"
#import "BuyUpgradeController.h"
#endif

#import <netinet/in.h>


@implementation ServerBrowserViewController

@synthesize managedObjectContext;

- (id)initWithNavigatorURL:(NSURL*)URL query:(NSDictionary*)query {
	if ( self = [super init] ) {
		self.title = NSLocalizedString(@"Add Server",@"Add server from local network or manually");
		self.managedObjectContext = [query objectForKey:@"managedObjectContext"];
        self.tableViewStyle = UITableViewStyleGrouped;
	}
	return self;
}

-(void) createModel {
	
	TTTableTextItem *addServerManuallyItem = [TTTableTextItem itemWithText:NSLocalizedString(@"Add a Server Manually",@"Add server manually") URL:@"tt://addservermanually"];
	addServerManuallyItem.userInfo = [NSDictionary dictionaryWithObjectsAndKeys:self.managedObjectContext,@"managedObjectContext",
                                      @"ip_address",@"add_by",nil];
    TTTableTextItem *addServerByPINItem = [TTTableTextItem itemWithText:NSLocalizedString(@"Add a Server By PIN",@"Add a Server By PIN") URL:@"tt://addservermanually"];
    
#ifdef FREE_UPGRADABLE_VERSION
    if ( ![MKStoreManager fullVersionUpgradePurchased] ) {
        addServerByPINItem = [TTTableTextItem itemWithText:NSLocalizedString(@"Add a Server By PIN",@"Add a Server By PIN") URL:@"tt://"];
    }      
#endif
    
    addServerByPINItem.userInfo = [NSDictionary dictionaryWithObjectsAndKeys:self.managedObjectContext,@"managedObjectContext",
                                   @"pin",@"add_by",nil];
    TTTableTextItem *remoteServerHelp = [TTTableTextItem itemWithText:NSLocalizedString(@"About Remote Servers",@"About Remote Servers")
                                                                    URL:@"tt://htmlhelp"];
    remoteServerHelp.userInfo = [NSDictionary dictionaryWithObjectsAndKeys:@"help_remote",@"htmlFilenamePrefix",nil];
	self.dataSource = [TTSectionedDataSource dataSourceWithArrays:
                       NSLocalizedString(@"Remote Servers",@"Remote Servers"),[NSArray arrayWithObjects:addServerByPINItem,remoteServerHelp,nil],
					   NSLocalizedString(@"Advanced",@"For advanced users"),[NSArray arrayWithObjects:addServerManuallyItem,nil],nil];
}

-(void)didSelectObject:(id)object atIndexPath:(NSIndexPath *)indexPath {
	TTDINFO(@"index path = %i",indexPath.row);
    if ( indexPath.section == 0 && indexPath.row == 0 ) {
#ifdef FREE_UPGRADABLE_VERSION
        // selected to add remote server
        if ( ![MKStoreManager fullVersionUpgradePurchased] ) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Requires Full Version"
                                                                                      ,@"Requires Full Version")
                                                            message:NSLocalizedString(@"Upgrade for Remote Server",@"Upgrade for Remote Server")
                                                           delegate:self cancelButtonTitle:NSLocalizedString(@"Not Now", @"Not Now") otherButtonTitles:NSLocalizedString(@"Upgrade",@"Upgrade to full version button"),nil];
            [alert show];
            [alert release];
        }
#endif
    }
}

-(void)viewDidLoad {
    // give the table view a custom tag to use section headers - see TTTableViewDelegateCategory, where section headers are supressed
    [self.tableView setTag:100];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.tableView setBackgroundColor:[UIColor colorWithRed:211/255.0f green:211/255.0f blue:211/255.0f alpha:1.0f]];
    
    self.navigationBarTintColor = [Utilities globalAppTintColor];
    
}

-(void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
	[self.navigationController setToolbarHidden:YES];
}

-(NSUInteger)supportedInterfaceOrientations {
	if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
		return UIInterfaceOrientationMaskAll;
	} else {
		return UIInterfaceOrientationMaskPortrait;
	}
}

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
	if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        return YES;
    }
    return UIInterfaceOrientationIsPortrait(toInterfaceOrientation);
}


#ifdef FREE_UPGRADABLE_VERSION
-(void)showUpgradeView:(id)sender {
	if ([SKPaymentQueue canMakePayments]) {
		BuyUpgradeController *upgradeController = [[BuyUpgradeController alloc] init];
//		[upgradeController setManagedObjContext:self.managedObjContext];
		[self.navigationController presentModalViewController:upgradeController animated:YES];
        //		[upgradeController release];
	} else {
		// TODO: show alert view
		NSLog(@"Cannot make payments!");
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Cannot purchase upgrade",
																				  @"Alert title when tapping Upgrade button but not able to make payments")
														message:NSLocalizedString(@"Sorry, you are not authorized to purchase from the App Store",
																				  @"Sorry, you are not authorized to purchase from the App Store")
													   delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];
		[alert release];
	}
}

#pragma mark
#pragma mark UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
	if ( buttonIndex == 0 ) {
		[alertView dismissWithClickedButtonIndex:0 animated:YES];
	} else {
		[alertView dismissWithClickedButtonIndex:0 animated:YES];
		[self showUpgradeView:alertView];
    }
}
#endif


-(void)dealloc {
	[managedObjectContext release];
	[super dealloc];
}

@end
