//
//  ServerBrowserViewController.h
//  ImageBank
//
//  Created by yoshi on 5/29/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Three20/Three20.h>


@interface ServerBrowserViewController : TTTableViewController 
  {
	NSManagedObjectContext *managedObjectContext;
	
}

@property (readwrite,nonatomic,retain) 	NSManagedObjectContext *managedObjectContext;

- (id)initWithNavigatorURL:(NSURL*)URL query:(NSDictionary*)query;

@end
