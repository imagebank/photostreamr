//
//  PhotoController.h
//  ImageBank
//
//  Created by yoshi on 5/14/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Three20/Three20.h>
#import "ProtectedContentDelegate.h"
#import "PrivatePasswordDialog.h"
#import "ImageMetaDataViewController.h"
#import "FolderContentsModel.h"

@class FolderContentsController;

@protocol PhotoControllerDelegate <NSObject>
-(void)setCurrentPhotoIndex:(NSInteger)index;
@end

@interface PhotoController : TTPhotoViewController<ProtectedContentDelegate,TTURLRequestDelegate,UIActionSheetDelegate,UINavigationControllerDelegate> {
	BOOL isFromPrivateFolder;
	NSManagedObjectContext *managedObjContext;
    PrivatePasswordDialog *privatePasswordDialog;
    ImageMetaDataViewController *metaVC;
    BOOL isOfflineFolder;
    FolderContentsModel *folderContentsModel;
    DirectoryFile *parentDirectory;
    FolderContentsController *parentVC;
    BOOL isSavingOffline;
}

-(void) preFetchNextImage;
-(void) preFetchNextImages:(NSInteger)count;
-(void) preFetchPreviousImage;
-(void) preFetchImageAtIndex:(NSInteger)index;

@property (readwrite,nonatomic) BOOL isFromPrivateFolder;
@property (readwrite,nonatomic,retain) NSManagedObjectContext *managedObjContext;
@property (readwrite,nonatomic,retain) ImageMetaDataViewController *metaVC;
@property (readwrite,nonatomic) BOOL isOfflineFolder;
@property (nonatomic,readwrite,copy) FolderContentsModel *folderContentsModel;
@property (nonatomic,readwrite,retain) DirectoryFile *parentDirectory;
@property (nonatomic,readwrite,retain) FolderContentsController *parentVC;

@property (nonatomic,assign) id<PhotoControllerDelegate> delegate;

@end