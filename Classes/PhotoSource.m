//
//  PhotoSource.m
//  ImageBank
//
//  Created by yoshi on 5/14/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "PhotoSource.h"
#import "File.h"
#import "FileListResponseProcessor.h"
#import "InternetConnectionStatusChecker.h"
#import "FileListModel.h"
#import "UIScreen+Retina.h"
#import "UIDeviceHardware.h"


#define kNumberOfPhotosToFetch 20

@implementation PhotoSource

@synthesize title = _title;
@synthesize totalNumberOfPhotos = _totalNumberOfPhotos;
@synthesize sortByField, sortAscending;


///////////////////////////////////////////////////////////////////////////////////////////////////
// NSObject

- (id)initWithTitle:(NSString*)title directory:(DirectoryFile*)dir photos:(NSArray*)photos {
	if (self = [super init]) {
		_title = [title copy];
		_photos = [[NSMutableArray alloc] init];
		_directory = [dir retain];
		
		int i = 0;
		for (ImageFile *file in photos) {
			if ( file.fileType == FileTypeImage  ) {
				Photo *photo = [[[Photo alloc] initWithURL:file.url smallURL:file.thumbnailUrl size:file.size] autorelease];
				photo.photoSource = self;
				photo.index = i;
				i++;
				[_photos addObject:photo];
			}
		}
		_totalNumberOfPhotos = i;
	}
	return self;
}

- (id)initWithTitle:(NSString*)titles
		  directory:(DirectoryFile*)dir
totalNumberOfPhotos:(NSInteger)totalNumberOfPhotos
forceReloadFromNetwork:(BOOL)reload {
	if (self = [super init]) {
		_title = [titles copy];
		_photos = [[NSMutableArray alloc] init];
		_totalNumberOfPhotos = totalNumberOfPhotos;
		_directory = [dir retain];
		_forceLoadingFromNetwork = reload;
	}
	return self;
}

- (id) initWithPhoto:(Photo*)photo {
    if ( self = [super init]) {
        _title = [photo getFilename];
        _photos = [[NSMutableArray alloc] init];
        [_photos addObject:photo];
        _totalNumberOfPhotos = 1;
    }
    return self;
}


-(void)setPhotosFromFileListModel:(FileListModel*)flm {
    for (ImageFile *file in [flm files]) {
        Photo *photo = [[[Photo alloc] initWithURL:file.url smallURL:file.thumbnailUrl size:file.size] autorelease];
        photo.photoSource = self;
        photo.index = _photos.count;
        photo.metadataUrl = file.metadataUrl;
        photo.filename = file.name;
//        photo.caption = file.name;
        if ( _photos.count < [self numberOfPhotos] )
            [_photos addObject:photo];
    }
    [self setLoadedTime:[NSDate date]];
    [self didFinishLoad];
}

#pragma mark
#pragma mark TTURLRequest Model
- (void)load:(TTURLRequestCachePolicy)cachePolicy more:(BOOL)more {
    TTDINFO(@"Start load of photo source...");
	NSInteger startIndex = [self maxPhotoIndex] + 1;
	NSString *url = nil;
	if ( _directory ) {
		NSInteger numberOfPhotosToFetch = [[[NSUserDefaults standardUserDefaults] objectForKey:@"pref_thumbnail_count"] intValue];
		if ( !numberOfPhotosToFetch )
			numberOfPhotosToFetch = kNumberOfPhotosToFetch;
		url = [NSString stringWithFormat:@"%@&i=1&start=%i&n=%i&sort_by=%@&sort_asc=%@",_directory.url,startIndex,numberOfPhotosToFetch,self.sortByField,(self.sortAscending ? @"true" : @"false")];
	} else {
		url = [NSString stringWithFormat:@"%@&json=1",_image.url];
	}
	TTURLRequest *request = [TTURLRequest requestWithURL:url delegate:self];
	request.cachePolicy = cachePolicy;
	if ( _forceLoadingFromNetwork )
		request.cachePolicy = TTURLRequestCachePolicyNetwork;
	request.response = self;
	request.httpMethod = @"GET";
    TTDINFO(@"Making request for photo source %@",url);
	[request send];
//	[self didFinishLoad];
}

- (NSError*)request:(TTURLRequest*)request processResponse:(NSHTTPURLResponse*)response data:(id)data {
    TTDINFO(@"Start process response of photo source...");
	if ( _photos.count >= [self numberOfPhotos] )
		return nil;
	FileListResponseProcessor *responseProcessor = [[[FileListResponseProcessor alloc] init] autorelease];
	[responseProcessor request:request processResponse:response data:data];
	for (ImageFile *file in responseProcessor.files) {
		if ( file.fileType == FileTypeImage  ) {
			Photo *photo = [[[Photo alloc] initWithURL:file.url smallURL:file.thumbnailUrl size:file.size] autorelease];
			photo.photoSource = self;
			photo.index = _photos.count;
            photo.metadataUrl = file.metadataUrl;
            photo.filename = file.name;
//            photo.caption = file.name;
			if ( _photos.count < [self numberOfPhotos] )
				[_photos addObject:photo];
		}
	}	
	return nil;
}

- (id)init {
	return [self initWithTitle:nil directory:nil photos:nil];
}

- (void)dealloc {
	TT_RELEASE_SAFELY(_photos);
//	TT_RELEASE_SAFELY(_title);
	TT_RELEASE_SAFELY(_directory);
    [sortByField release];
	[super dealloc];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// TTPhotoSource

- (NSInteger)numberOfPhotos {
    return _totalNumberOfPhotos;
}

- (NSInteger)maxPhotoIndex {
	return _photos.count-1;
}

- (id<TTPhoto>)photoAtIndex:(NSInteger)index {
	if (index < _photos.count) {
		id photo = [_photos objectAtIndex:index];
		if (photo == [NSNull null]) {
			return nil;
		} else {
			return photo;
		}
	} else {
		return nil;
	}
}

@end

///////////////////////////////////////////////////////////////////////////////////////////////////

@implementation Photo

@synthesize photoSource = _photoSource, size = _size, index = _index, caption = _caption, metadataUrl = _metadataUrl, isSelected = _isSelected, filename = _filename;

///////////////////////////////////////////////////////////////////////////////////////////////////
// NSObject

- (id)initWithURL:(NSString*)URL smallURL:(NSString*)smallURL size:(CGSize)size {
	return [self initWithURL:URL smallURL:smallURL size:size caption:nil metadataUrl:nil];
}

- (id)initWithURL:(NSString*)URL smallURL:(NSString*)smallURL size:(CGSize)size
caption:(NSString*)caption metadataUrl:(NSString*)metadataUrl {
	if (self = [super init]) {
		_photoSource = nil;
		_URL = [URL copy];
		_smallURL = [smallURL copy];
		_thumbURL = [smallURL copy];
		_size = size;
		_caption = [caption copy];
		_index = NSIntegerMax;
        _metadataUrl = [metadataUrl copy];
        [self setIsSelected:NO];
	}
	return self;
}

-(NSString*) getMetadataUrl {
    return _metadataUrl;
}

-(NSString*) getFilename {
    return _filename;
}

- (void)dealloc {
	TT_RELEASE_SAFELY(_URL);
	TT_RELEASE_SAFELY(_smallURL);
	TT_RELEASE_SAFELY(_thumbURL);
	TT_RELEASE_SAFELY(_caption);
	[super dealloc];
}


///////////////////////////////////////////////////////////////////////////////////////////////////
// TTPhoto

- (NSString*)URLForVersion:(TTPhotoVersion)version {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    BOOL isOfflineFolder = [_URL hasPrefix:@"documents://"];
	if (version == TTPhotoVersionLarge) {
        NSString *url = _URL;
        TTDINFO(@"url: %@",url);
        TTDINFO(@"hardware: %@ : %@ : %@",[UIDeviceHardware platform],[UIDeviceHardware platformString],[userDefaults boolForKey:@"pref_ipad_retina_images"] ? @"YES" : @"NO");
        if ( !isOfflineFolder ) {
            if ( ([[UIScreen mainScreen] isIPadRetina] ||
                  [[UIDeviceHardware platformString] isEqualToString:@"iPhone 5"] ||
                  [[UIDeviceHardware platformString] isEqualToString:@"iPhone 5S"] ||
                  [[UIDeviceHardware platformString] isEqualToString:@"Simulator"]) &&
                [userDefaults boolForKey:@"pref_ipad_retina_images"] )
            {
                url = [NSString stringWithFormat:@"%@&r_ipad=t",url];
                TTDINFO("specifying retina display ipad for large photo version: %@",url);
            }
            
            if ( [[InternetConnectionStatusChecker sharedInstance] useLowQualityImages] ) {            
                url = [NSString stringWithFormat:@"%@&q=lo",url];
                TTDINFO(@"getting lower quality image for large photo version: %@",url);
                return url;
            } else {
                TTDINFO("wifi available so using large: %@", url);
            }
        }

		return url;
	} else if (version == TTPhotoVersionMedium) {
		return _URL;
	} else if ( version == TTPhotoVersionSmall || version == TTPhotoVersionThumbnail ) {
        BOOL useLowQualityThumbnails = [userDefaults boolForKey:@"pref_lowq_thumbnails"];
        NSString *url = _smallURL;
        
        if ( [UIDeviceHardware isSlowerDevice] ) {
            url = [NSString stringWithFormat:@"%@&ld=t",url];
            TTDINFO(@"Using thumbs for slower device: %@",url);
        }
        
        if ( !isOfflineFolder ) {
            if ( useLowQualityThumbnails ) {
                url = [NSString stringWithFormat:@"%@&lq=t",url];
                TTDINFO("Using lower quality thumbnail: %@",url);
            }
            if ( [[UIScreen mainScreen] isIPadRetina] ||
                [UIDeviceHardware isiPad2OrHigher]) {
                url = [NSString stringWithFormat:@"%@&r_ipad=t",url];
                TTDINFO("specifying higher quality thumbnail for ipad 2 and higher: %@",url);
            }
        }
		return url;
	} else {
		return nil;
	}
}

@end