//
//  MovableViewWithInsetView.m
//  ImageBankFree
//
//  Created by Yoshi Sugawara on 4/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MovableViewWithInsetView.h"
#import <QuartzCore/QuartzCore.h>

@implementation MovableViewWithInsetView

@synthesize insetView, closeButtonView, backgroundView;

- (id)initWithCoder:(NSCoder *)coder {
	
	self = [super initWithCoder:coder];
	if (self) {
	}
	return self;
}

- (void)awakeFromNib {
    [self.backgroundView.layer setCornerRadius:30.0f];
    //        [self.layer setBorderColor:[UIColor blackColor].CGColor];
    //        [self.layer setBorderWidth:1.5f];
    [self.backgroundView.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.backgroundView.layer setShadowOpacity:0.8];
    [self.backgroundView.layer setShadowRadius:3.0];
    [self.backgroundView.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void) dealloc {
    [insetView release];
    [closeButtonView release];
    [backgroundView release];
    [super dealloc];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
	
	// We only support single touches, so anyObject retrieves just that touch from touches
	UITouch *touch = [touches anyObject];
	
    if ( [touch view] == closeButtonView ) {
        [UIView animateWithDuration:0.15 
                         animations:^{
                             self.transform = CGAffineTransformMakeScale(0.1, 0.1);
                         }
         completion:^(BOOL finished) {
             self.hidden = YES;
             self.transform = CGAffineTransformIdentity;
         }];
        return;
    }
    
    // Only move the view if its not a touch in the inset
    if ( [touch view] != insetView) {
        // Animate the first touch
        currentPoint = [[touches anyObject] locationInView:self];
        [self animateFirstTouchAtPoint:currentPoint];        
    }
    
    return;
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
	
	UITouch *touch = [touches anyObject];
	
    // Only move the view if its not a touch in the inset
	if ([touch view] != insetView) {
		CGPoint activePoint = [touch locationInView:self];
        // Determine new point based on where the touch is now located
        CGPoint newPoint = CGPointMake(self.center.x + (activePoint.x - currentPoint.x),
                                       self.center.y + (activePoint.y - currentPoint.y));
        //--------------------------------------------------------
        // Make sure we stay within the bounds of the parent view
        //--------------------------------------------------------
        float midPointX = CGRectGetMidX(self.bounds);
        // If too far right...
        if (newPoint.x > self.superview.bounds.size.width  - midPointX)
            newPoint.x = self.superview.bounds.size.width - midPointX;
        else if (newPoint.x < midPointX) 	// If too far left...
            newPoint.x = midPointX;
        
        float midPointY = CGRectGetMidY(self.bounds);
        // If too far down...
        if (newPoint.y > self.superview.bounds.size.height  - midPointY)
            newPoint.y = self.superview.bounds.size.height - midPointY;
        else if (newPoint.y < midPointY)	// If too far up...
            newPoint.y = midPointY;
        
        // Set new center location
        self.center = newPoint;	
		return;
	}
}

//- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
//	
//	UITouch *touch = [touches anyObject];
//
//	// check if the view is outside of the window
//}
//
//- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
//	
//	/*
//     To impose as little impact on the device as possible, simply set the placard view's center and transformation to the original values.
//     */
//	insetView.center = self.center;
//	insetView.transform = CGAffineTransformIdentity;
//}

/*
 Alternate behavior.
 The preceding implementation grows the placard in place then moves it to the new location and shrinks it at the same time.  An alternative is to move the placard for the total duration of the grow and shrink operations; this gives a smoother effect.
 
 */


- (void)animateFirstTouchAtPoint:(CGPoint)touchPoint {
	
#define GROW_ANIMATION_DURATION_SECONDS 0.15
#define SHRINK_ANIMATION_DURATION_SECONDS 0.15
	
	/*
	 Create two separate animations, the first for the grow, which uses a delegate method as before to start an animation for the shrink operation. The second animation here lasts for the total duration of the grow and shrink animations and is responsible for performing the move.
	 */
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration:GROW_ANIMATION_DURATION_SECONDS];
	[UIView setAnimationDelegate:self];
	[UIView setAnimationDidStopSelector:@selector(growAnimationDidStop:finished:context:)];
	CGAffineTransform transform = CGAffineTransformMakeScale(1.2, 1.2);
	self.transform = transform;
	[UIView commitAnimations];
	
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration:GROW_ANIMATION_DURATION_SECONDS + SHRINK_ANIMATION_DURATION_SECONDS];
    CGPoint newPoint = CGPointMake(self.center.x + (touchPoint.x - currentPoint.x),
                                   self.center.y + (touchPoint.y - currentPoint.y));
    //--------------------------------------------------------
    // Make sure we stay within the bounds of the parent view
    //--------------------------------------------------------
    float midPointX = CGRectGetMidX(self.bounds);
    // If too far right...
    if (newPoint.x > self.superview.bounds.size.width  - midPointX)
        newPoint.x = self.superview.bounds.size.width - midPointX;
    else if (newPoint.x < midPointX) 	// If too far left...
        newPoint.x = midPointX;
    
    float midPointY = CGRectGetMidY(self.bounds);
    // If too far down...
    if (newPoint.y > self.superview.bounds.size.height  - midPointY)
        newPoint.y = self.superview.bounds.size.height - midPointY;
    else if (newPoint.y < midPointY)	// If too far up...
        newPoint.y = midPointY;
	self.center = newPoint;
	[UIView commitAnimations];
}


- (void)growAnimationDidStop:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context {
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration:SHRINK_ANIMATION_DURATION_SECONDS];
	self.transform = CGAffineTransformIdentity;	
	[UIView commitAnimations];
}

- (void)animationDidStop:(CAAnimation *)theAnimation finished:(BOOL)flag {
	//Animation delegate method called when the animation's finished:
	// restore the transform and reenable user interaction
	self.transform = CGAffineTransformIdentity;
	self.userInteractionEnabled = YES;
}


@end
