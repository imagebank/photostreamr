//
//  TTThumbsTableViewCellCategory.m
//  ImageBankFree
//
//  Created by yoshi on 11/27/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "TTThumbsTableViewCellCategory.h"
#import "Utilities.h"

@implementation TTThumbsTableViewCell(Category)

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString*)identifier {
    if (self = [super initWithStyle:style reuseIdentifier:identifier]) {
        CGFloat width = TTScreenBounds().size.width;
        CGFloat thumbSize = [Utilities thumbnailWidthForScreenWidth:width];
        CGFloat thumbSpacing = [Utilities thumbnailSpacingForScreenWidth:width];
        _thumbViews = [[NSMutableArray alloc] init];
        _thumbSize = thumbSize;
        _thumbOrigin = CGPointMake(thumbSpacing, 0);
        self.accessoryType = UITableViewCellAccessoryNone;
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    return self;
}

+ (CGFloat)tableView:(UITableView*)tableView rowHeightForObject:(id)object {
    CGFloat width = TTScreenBounds().size.width;
    CGFloat thumbSpacing = [Utilities thumbnailSpacingForScreenWidth:width];
    return [Utilities thumbnailWidthForScreenWidth:width] + thumbSpacing;
}

- (void)layoutThumbViews {
    CGFloat width = TTScreenBounds().size.width;
    CGFloat thumbSpacing = [Utilities thumbnailSpacingForScreenWidth:width];
    CGFloat thumbSize = [Utilities thumbnailWidthForScreenWidth:width];
    _thumbSize = thumbSize;
    
    CGRect thumbFrame = CGRectMake(self.thumbOrigin.x, self.thumbOrigin.y,
                                   self.thumbSize, self.thumbSize);
    
    
    for (TTThumbView* thumbView in _thumbViews) {
        thumbView.frame = thumbFrame;
        // set the frame for the selection overlay view
        thumbView.selectionView.frame = CGRectMake(0, 0,
                                                   thumbFrame.size.width, thumbFrame.size.height);
        // set the frame for the checkbox
        CGPoint bottomRightPoint = CGPointMake(4.f, 4.f);
        CGSize checkmarkSize = CGSizeMake(28.f,28.f);
        CGRect checkmarkRect = CGRectMake(bottomRightPoint.x,bottomRightPoint.y,
                                          checkmarkSize.width,checkmarkSize.height);
        thumbView.checkmarkImageView.frame = CGRectMake(
                                                        thumbFrame.size.width - checkmarkRect.size.width - checkmarkRect.origin.x,
                                                        thumbFrame.size.height - checkmarkRect.size.height - checkmarkRect.origin.y,
                                                        checkmarkRect.size.width,
                                                        checkmarkRect.size.height
                                                        );
        
        thumbFrame.origin.x += thumbSpacing + self.thumbSize;
    }
}

@end
