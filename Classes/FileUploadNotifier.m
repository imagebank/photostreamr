//
//  FileUploadNotifier.m
//  ImageBankFree
//
//  Created by yoshi on 4/12/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "FileUploadNotifier.h"
#import "File.h"
#import "UploadOperation.h"

@implementation FileUploadNotifier

@synthesize running, queue, callingController;

static FileUploadNotifier *sharedInstance = nil;

#pragma mark
#pragma mark Singleton methods

// Get the shared instance and create it if necessary.
+ (FileUploadNotifier*)sharedInstance {
    if (sharedInstance == nil) {
        sharedInstance = [[super allocWithZone:NULL] init];
		[sharedInstance setRunning:NO];
		[sharedInstance setQueue:[[NSMutableArray alloc] init]];
    }	
    return sharedInstance;
}


// We don't want to allocate a new instance, so return the current one.
+ (id)allocWithZone:(NSZone*)zone {
    return [[self sharedInstance] retain];
}

// Equally, we don't want to generate multiple copies of the singleton.
- (id)copyWithZone:(NSZone *)zone {
    return self;
}

// Once again - do nothing, as we don't have a retain counter for this object.
- (id)retain {
    return self;
}

// Replace the retain counter so we can never release this object.
- (NSUInteger)retainCount {
    return NSUIntegerMax;
}

// This function is empty, as we don't want to let the user release this object.
- (oneway void)release {
	
}

//Do nothing, other than return the shared instance - as this is expected from autorelease.
- (id)autorelease {
    return self;
}

-(void)addUploadOperation:(NSData*)imageData 
			 forDirectory:(DirectoryFile*)dir 
		   withController:(FolderContentsController*)controller
			  orientation:(UIInterfaceOrientation)orientation {
	TTDINFO(@"Adding upload operation...");
	UploadOperation *uo = [[UploadOperation alloc] initWithData:imageData forDirectory:dir withOrientation:orientation];
	self.callingController = controller;
	if ( !self.running ) {
		TTDINFO(@"upload operation not running, running the one just added...");
		self.running = YES;
		[uo start];
	} else {
		TTDINFO(@"upload operation running, adding to queue and scheduling process run...");
		[self.queue addObject:uo];
		// try to process in 3 secs
		[self scheduleProcessQueue];
	}
}

-(void)scheduleProcessQueue {
	[NSTimer scheduledTimerWithTimeInterval:3
									 target:self
								   selector:@selector(processQueue:)
								   userInfo:nil
									repeats:NO];
}	

-(void)processQueue:(NSTimer*) timer {
	TTDINFO(@"Starting process queue...");
	if ( self.running ) {
		TTDINFO(@"Process already running, rescheduling...");
		[self scheduleProcessQueue];
		return;
	} else if ( [queue count] > 0 ) {
		TTDINFO(@"Processing queue...");
		UploadOperation *nextOperation = [(UploadOperation*) [self.queue objectAtIndex:0] retain];
		[self.queue removeObjectAtIndex:0];
		self.running = YES;
		[nextOperation start];
		if ( [self.queue count] > 0 ) {
			[self scheduleProcessQueue];
		}
	}
}

-(void)notifyComplete:(UploadOperation*)operation {
	TTDINFO(@"upload operation complete, not running anymore");
	self.running = NO;
	[operation release];
	if ( callingController != nil && callingController.isViewLoaded && callingController.view.window) {
		[callingController refreshAction:nil];
	}
}

-(void)dealloc {
	[queue release];
	[super dealloc];
}


@end
