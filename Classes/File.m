//
//  File.m
//  ImageBank
//
//  Created by yoshi on 5/12/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "File.h"

@implementation File

@synthesize name, url, fileType, modifiedDate;

-(id)initWithName:(NSString*)filename url:(NSString*)fileURL filetype:(FileType)ftype {
	if ( self = [super init] ) {
		[self setName:filename];
		[self setUrl:fileURL];
		[self setFileType:ftype];
	}
	return self;
}

-(TTTableTextItem *)tableItemWithUIOrientation:(UIInterfaceOrientation)orientation {
	TTTableTextItem *item = [TTTableCaptionItem itemWithText:self.name
													  URL:@"tt://filelist?"]; 
	item.userInfo = [NSDictionary dictionaryWithObject:self forKey:@"file"];
	return item;
}

-(void)dealloc {
	[name release];
	[url release];
	[super dealloc];
}

@end

/////////////

@implementation ImageFile

@synthesize size, files, filesIndex, thumbnailUrl, smallThumbnailUrl, directory, metadataUrl;

-(id)initWithName:(NSString *)fname 
			  url:(NSString *)fileURL
			 size:(CGSize)s
     thumbnailUrl:(NSString*)thumburl
smallThumbnailUrl:(NSString*)smallthumburl
      metadataUrl:(NSString*)metadataurl {
	if ( self = [super initWithName:fname url:fileURL filetype:FileTypeImage] ) {
		size = s;
		[self setThumbnailUrl:thumburl];
		[self setSmallThumbnailUrl:smallthumburl];
        [self setMetadataUrl:metadataurl];
	}
	return self;
}

-(TTTableTextItem *)tableItemWithUIOrientation:(UIInterfaceOrientation)orientation {
	TTTableImageItem *item = [TTTableImageItem	itemWithText:self.name
												   imageURL:self.smallThumbnailUrl
											   defaultImage:[UIImage imageNamed:@"tableitem-photo.png"]
														URL:@"tt://photos?"];
    item.imageStyle = [TTImageStyle styleWithImage:nil defaultImage:nil contentMode:UIViewContentModeScaleAspectFill
                                              size:CGSizeMake(30.0f, 30.0f)
                                              next:nil];

	
	item.userInfo = [NSDictionary dictionaryWithObjectsAndKeys:self,@"imagefile",files,@"files",nil];
	return item;	
}


-(void)dealloc {
	[files release];
	[filesIndex release];
	[thumbnailUrl release];
	[super dealloc];
}
@end

///////////////

@implementation DirectoryFile

@synthesize imageCount, dirCount,isPrivate, isiPhoto;

-(id)initWithName:(NSString*)dirname url:(NSString*)fileURL 
		imageCount:(NSInteger)imgCount dirCount:(NSInteger)dCount {
	if ( self = [super initWithName:dirname url:fileURL filetype:FileTypeDirectory] ) {
		self.dirCount = dCount;
		self.imageCount = imgCount;
	}
	return self;
}

-(TTTableTextItem *)tableItemWithUIOrientation:(UIInterfaceOrientation)orientation {
	NSMutableDictionary *query = [[[NSMutableDictionary alloc] init] autorelease];
	[query setObject:self forKey:@"file"];
	TTTableImageItem *item = [TTTableImageItem	itemWithText:self.name
														imageURL:@""
                                               defaultImage:self.isiPhoto ? 
                                                              [UIImage imageNamed:@"tableitem-iphoto"] : 
                                                              [UIImage imageNamed:@"tableitem-folder.png"]
															 URL:@"tt://folderview?"];
	item.userInfo = query;
	return item;
}


-(void)dealloc {
	[super dealloc];
}
@end

/////////////////

@implementation RandomDirectoryFile

-(TTTableTextItem *)tableItemWithUIOrientation:(UIInterfaceOrientation)orientation {
	NSMutableDictionary *query = [[[NSMutableDictionary alloc] init] autorelease];
	[query setObject:self forKey:@"file"];
	TTTableImageItem *item = [TTTableImageItem	itemWithText:NSLocalizedString(@"Shuffle",@"Random images folder title")
												   imageURL:@""
											   defaultImage:[UIImage imageNamed:@"tableitem-random.png"]
														URL:@"tt://folderview?"];
	item.userInfo = query;
	return item;
}

-(void)dealloc {
	[super dealloc];
}
@end

/////////////////

@implementation SearchResultsDirectoryFile
-(void)dealloc {
	[super dealloc];
}
@end
