//
//  InternetConnectionStatusChecker.h
//  ImageBankFree
//
//  Created by Yoshi Sugawara on 8/20/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Reachability.h"

@interface InternetConnectionStatusChecker : NSObject {
    Reachability* internetReachable;
    Reachability* hostReachable;
    BOOL wifiAvailable;
}

+(id)sharedInstance;
-(BOOL) useLowQualityImages;
@property(nonatomic,readwrite) BOOL wifiAvailable;

@end
