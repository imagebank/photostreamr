//
//  FolderContentsController.h
//  ImageBank
//
//  Created by yoshi on 6/13/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Three20/Three20.h>
#import "File.h"
#import "FileListModel.h"
#import "PrivatePasswordDialog.h"
#import "ProtectedContentDelegate.h"
#import "PopOverContentDelegate.h"
#import "IASKAppSettingsViewController.h"
#import "PPRevealSideViewController.h"
#import "FolderContentsSearchResultsController.h"
#import "PhotoController.h"

#ifdef FREE_UPGRADABLE_VERSION
#import <iAd/iAd.h>
#import <StoreKit/StoreKit.h>
#endif


typedef enum {
	DisplayModeFiles = 0,
	DisplayModeImages = 1
} DisplayMode;


@interface FolderContentsController : TTThumbsViewController<IASKSettingsDelegate,ProtectedContentDelegate,UIImagePickerControllerDelegate,
TTURLRequestDelegate,UIActionSheetDelegate,UINavigationControllerDelegate,
PopOverContentDelegate,PPRevealSideViewControllerDelegate,UISearchBarDelegate,PhotoControllerDelegate
>
{
	// the core data source
	FileListModel *fileListModel;
	// current directory that contains all the images in this controller
	DirectoryFile *currentDirectory;
    DirectoryFile *parentDirectory;
	DisplayMode displayMode;
	BOOL isAtRootDirectory;
	NSManagedObjectContext *managedObjContext;
	BOOL doReload;
	NSString *titleForView;
    PrivatePasswordDialog *privatePasswordDialog;
    UIImageView *lockFlashView;
    BOOL isOfflineFolder;
	   
	// Keep a reference to this so the icon image can be modified elsewhere
    UIBarButtonItem *privateFoldersAccessButton;
    UIBarButtonItem *offlineButton;
    UIBarButtonItem *downloadSelectedButton;
	   
    UIImagePickerController *picker;
    NSMutableArray *actionSheetButtons;
	   
    BOOL isSelectingPhotosForDownload;
    UIBarButtonItem *upgradeButton;

    NSString *sortByField;
    BOOL sortAscending;
    
    FolderContentsSearchResultsController *searchResultsController;
    UISearchBar *folderSearchBar;
    UISearchDisplayController *folderSearchDisplayController;
    
    NSIndexPath *indexPathAtBeginDragging;
    NSInteger lastCenterPhotoIndex;
    
}

-(id)initWithNavigatorURL:(NSURL *)URL query:(NSDictionary *)query;
-(void)setupTableView;
-(void)setPhotoSourceAccordingToDirectoryWithReloadSetting:(BOOL)forceReload;
-(NSArray*) toolbarItems:(BOOL)hidden;
-(void)maybeRefreshDataSourceIfPrivateAccessHasChanged;
-(void) popAllViewControllers;
-(void)lockFlashAnimationDone;
-(void)refreshAction:(id)sender;
-(void) uploadImageUsingPhotoLibrary;
-(void) uploadImageUsingCamera;
-(void) uploadImageUsingClipboard;

- (void)updateTableDelegateWithVarHeight;
- (void)updateTableDelegateWithFixedHeight;

-(NSArray*)toolbarItemsForSelectingPhotos;
-(void) updatePhotosSelectedStatus;
-(void) changeOfflineButtonToInProgress;
-(void)doneSelectingPhotos:(id)sender;

@property (readwrite,nonatomic,retain) FileListModel *fileListModel;
@property (readwrite,nonatomic,retain) DirectoryFile *currentDirectory;
@property (readwrite,nonatomic,retain) DirectoryFile *parentDirectory;
@property (readwrite,nonatomic) DisplayMode displayMode;
@property (readwrite,nonatomic) BOOL isAtRootDirectory;
@property (readwrite,nonatomic,retain) NSManagedObjectContext *managedObjContext;
@property (readwrite,nonatomic,retain) UIBarButtonItem *privateFoldersAccessButton;
@property (readwrite,nonatomic,retain) UIBarButtonItem *offlineButton;
@property (readwrite,nonatomic,retain) UIBarButtonItem *downloadSelectedButton;
@property (nonatomic,retain) NSString *sortByField;
@property (nonatomic,retain) UISearchBar *folderSearchBar;
@property (nonatomic,retain) FolderContentsSearchResultsController *searchResultsController;
@property (nonatomic,retain) UISearchDisplayController *folderSearchDisplayController;
@property (nonatomic,retain) NSIndexPath *indexPathAtBeginDragging;

@end
