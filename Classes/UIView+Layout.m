//
//  UIView+Layout.m
//  ImageBank
//
//  Created by yoshi on 3/29/13.
//
//

#import "UIView+Layout.h"

@implementation UIView (Layout)

- (void)setNeedsLayoutRecursively {
    for (UIView *view in self.subviews) {
        [view setNeedsLayoutRecursively];
    }
    [self setNeedsLayout];
}

@end
