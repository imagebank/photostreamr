//
//  UploadOperation.h
//  ImageBankFree
//
//  Created by yoshi on 4/12/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PopUpNotificationView.h"
#import "File.h"

@interface UploadOperation : NSObject {
	PopUpNotificationView *fileUploadingView;
	NSData *imageData;
	DirectoryFile *dir;
	UIInterfaceOrientation orientation;
}

-(id)initWithData:(NSData*)data forDirectory:(DirectoryFile*)d withOrientation:(UIInterfaceOrientation)orientation;
-(void)start;
-(void) hideView;

@property (nonatomic,readwrite,retain) 	PopUpNotificationView *fileUploadingView;
@property (nonatomic,readwrite,retain) 	NSData *imageData;
@property (nonatomic,readwrite,retain)	DirectoryFile *dir;
@property (nonatomic,readwrite,assign)	UIInterfaceOrientation orientation;

@end
