//
//  UIView+Layout.h
//  ImageBank
//
//  Created by yoshi on 3/29/13.
//
//

#import <UIKit/UIKit.h>

@interface UIView (Layout)
- (void)setNeedsLayoutRecursively;

@end
