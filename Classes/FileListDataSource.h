//
//  FilesListDataSource.h
//  ImageBank
//
//  Created by yoshi on 5/13/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "Three20/Three20.h"


@interface FileListDataSource : TTListDataSource {
	// Store the interface orientation because the table cell content will differ based on it
	UIInterfaceOrientation uiOrientation;
}

-(void)refreshItems;

@property(nonatomic,readwrite) UIInterfaceOrientation uiOrientation;

@end
