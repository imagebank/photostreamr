//
//  FolderContentsDataSource.h
//  ImageBankFree
//
//  Created by Yoshi Sugawara on 9/25/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <Three20/Three20.h>
#import "FolderContentsModel.h"

@interface FolderContentsDataSource : TTThumbsDataSource {
    DirectoryFile *currentDirectory;
    NSMutableArray *folders;
}

@property (nonatomic,retain) NSMutableArray *folders;
@property (nonatomic,retain) DirectoryFile *currentDirectory;

- (NSInteger)columnCountForView:(UIView *)view;
-(void)refreshFolders;

@end
