//
//  UIScreen+Retina.h
//  ImageBankFree
//
//  Created by Yoshi Sugawara on 4/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIScreen (Retina)

// Returns YES if this is a Retina display.
- (BOOL)isRetina;

// Retina display iPad (the new iPad or iPad 3rd generation)
-(BOOL) isIPadRetina;

@end
