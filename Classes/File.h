//
//  File.h
//  ImageBank
//
//  Created by yoshi on 5/12/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
	FileTypeDirectory = 0,
	FileTypeImage = 1,
	FileTypeOther = 2
} FileType;

@interface File : NSObject {
	NSString *name;
	NSString *url;
	FileType fileType;
    NSDate *modifiedDate;
}

-(id)initWithName:(NSString*)filename url:(NSString*)fileURL filetype:(FileType)ftype;
-(TTTableTextItem *)tableItemWithUIOrientation:(UIInterfaceOrientation)orientation;


@property (readwrite,nonatomic,retain) NSString *name;
@property (readwrite,nonatomic,retain) NSString *url;
@property (readwrite,nonatomic) FileType fileType;
@property (readwrite,nonatomic,retain) NSDate *modifiedDate;

@end


///////////////////

@interface DirectoryFile : File {
	NSInteger imageCount;
	NSInteger dirCount;
	BOOL isPrivate;
    BOOL isiPhoto;
}

@property(readwrite,nonatomic) NSInteger imageCount;
@property(readwrite,nonatomic) NSInteger dirCount;
@property(readwrite,nonatomic) BOOL isPrivate;
@property(readwrite,nonatomic) BOOL isiPhoto;

-(id)initWithName:(NSString*)name url:(NSString*)fileURL imageCount:(NSInteger)imgCount dirCount:(NSInteger)dCount;

@end

////////////////////


@interface ImageFile : File {
	// The image library this image is contained within
	NSArray *files;
	// The index in the files array that points to this image
	NSNumber *filesIndex;
	CGSize size;
	NSString *thumbnailUrl;
	NSString *smallThumbnailUrl;
	DirectoryFile *directory;
    NSString *metadataUrl;
}

-(id)initWithName:(NSString *)name 
			  url:(NSString *)fileURL 
			 size:(CGSize)s 
	 thumbnailUrl:(NSString*)thumburl
smallThumbnailUrl:(NSString*)smallthumburl
      metadataUrl:(NSString*)metadataurl;

@property (readwrite,nonatomic,retain) NSArray *files;
@property (readwrite,nonatomic,retain) NSNumber *filesIndex;
@property (readwrite,nonatomic) CGSize size;
@property (readwrite,nonatomic,retain) NSString *thumbnailUrl;
@property (readwrite,nonatomic,retain) NSString *smallThumbnailUrl;
@property (readwrite,nonatomic,retain) DirectoryFile *directory;
@property (readwrite,nonatomic,retain) NSString *metadataUrl;

@end

///////////////////


@interface RandomDirectoryFile : DirectoryFile {
}
@end

///////////////////

@interface SearchResultsDirectoryFile : DirectoryFile {
}

@end
