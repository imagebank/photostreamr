//
//  HTMLHelpController.h
//  ImageBankFree
//
//  Created by Yoshi Sugawara on 8/17/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HTMLHelpController : UIViewController {
    IBOutlet UIWebView *webView;
    
    NSString *htmlFilenamePrefix;
    BOOL presentedAsModal;
}

-(id)initWithNavigatorURL:(NSURL *)URL query:(NSDictionary *)query;

@property(nonatomic,retain) IBOutlet UIWebView *webView;
@property(nonatomic,retain) NSString *htmlFilenamePrefix;
@property(nonatomic,readwrite) BOOL presentedAsModal;

@end
