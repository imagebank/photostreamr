//
//  HTMLHelpController.m
//  ImageBankFree
//
//  Created by Yoshi Sugawara on 8/17/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "HTMLHelpController.h"
#import "Utilities.h"

@implementation HTMLHelpController

@synthesize webView, htmlFilenamePrefix, presentedAsModal;

-(void)dealloc {
    [htmlFilenamePrefix release];
    [super dealloc];
}

-(id)initWithNavigatorURL:(NSURL *)URL query:(NSDictionary *)query {
	if ( self = [super init] ) {
        [self setHtmlFilenamePrefix:[query objectForKey:@"htmlFilenamePrefix"]];
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.navigationController.navigationBar setTintColor:[Utilities globalAppTintColor]];
    if ( presentedAsModal ) {
        UIBarButtonItem *closeButton = [[UIBarButtonItem alloc] 
                                        initWithTitle:NSLocalizedString(@"Close",@"Close")
                                        style:UIBarButtonItemStylePlain target:self action:@selector(closeWindow:)];
        self.navigationItem.rightBarButtonItem = closeButton;
        self.navigationItem.rightBarButtonItem.enabled = YES;
    }
    
    // load html content
    NSURL *url = [NSURL fileURLWithPath:[Utilities getLocalizedHtmlFilenameUsingPrefix:self.htmlFilenamePrefix]];
    [self.webView loadRequest:[NSURLRequest requestWithURL:url]];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(void)closeWindow:(id)sender {
    [self dismissModalViewControllerAnimated:YES];
}


@end
