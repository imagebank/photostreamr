// Playground - noun: a place where people can play

import UIKit



enum Rank: Int {
    case Ace = 1
    case Two, Three, Four, Five, Six, Seven, Eight, Nine, Ten
    case Jack, Queen, King
    func description() -> String {
        switch self {
        case .Ace:
            return "ace"
        case .Jack:
            return "jack"
        case .Queen:
            return "queen"
        case .King:
            return "king"
        default:
            return String(self.toRaw())
        }
    }
}

enum Suit {
    case Spades, Hearts, Diamonds, Clubs
    func description() -> String {
        switch self {
        case .Spades:
            return "spades"
        case .Hearts:
            return "hearts"
        case .Diamonds:
            return "diamonds"
        case .Clubs:
            return "clubs"
        }
    }
    
    func color() -> String {
        switch self {
        case let x where x == Suit.Spades || x == Suit.Clubs:
            return "black"
        default:
            return "red"
        }
    }
}


struct Card {
    var rank: Rank
    var suit: Suit
    func description() -> String {
        return "The \(rank.description()) of \(suit.description())"
    }
}

func createDeck() -> Array<Card> {
    var cards: Array<Card> = []
    for rank in Rank.Ace.toRaw()..(Rank.King.toRaw() + 1) {
        cards += Card(rank: Rank.fromRaw(rank)!, suit: Suit.Spades)
        cards += Card(rank: Rank.fromRaw(rank)!, suit: Suit.Hearts)
        cards += Card(rank: Rank.fromRaw(rank)!, suit: Suit.Diamonds)
        cards += Card(rank: Rank.fromRaw(rank)!, suit: Suit.Clubs)
    }
    return cards
}

var deck = createDeck()
deck.count

for card in deck {
    card.description()
    println(card.description())
}



